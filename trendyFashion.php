<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ArticleOne.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $articlesFashion = getArticlesOne($conn, " WHERE type = 'Fashion' ");
$articlesFashion = getArticlesOne($conn, " WHERE type = 'Fashion' AND display = 'YES' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="Fashion | Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">
<link rel="canonical" href="https://tevy.asia/trendyFashion.php" />
<title>Fashion | Tevy</title>
<?php include 'css.php'; ?>




</head>
<body>
<?php include 'header-after-login.php'; ?>

<div class="background-div">
    <div class="cover-gap content min-height">
        <div class="test ">

            <h1 class="landing-h1"><?php echo _HEADER_FASHION ?></h1>	   

            <div class="big-white-div">

            <?php
            $conn = connDB();
            if($articlesFashion)
            {
                for($cnt = 0;$cnt < count($articlesFashion) ;$cnt++)
                {
                ?>


                    <!-- <a href='article.php?id=<?php //echo $articlesFashion[$cnt]->getSeoTitle();?>'> -->
                    <a href='article.php?id=<?php echo $articlesFashion[$cnt]->getArticleLink();?>'>

                        <div class="article-card article-card-overwrite">

                            <div class="article-bg-img-box">
                                <img src="uploads/<?php echo $articlesFashion[$cnt]->getTitleCover();?>" class="article-img1" alt="<?php echo $articlesFashion[$cnt]->getTitle();?>" title="<?php echo $articlesFashion[$cnt]->getTitle();?>">
                            </div>

                            <div class="box-caption box2">

                                <div class="wrap-a wrap100">
                                    <!-- <a href="beautyCare.php" class="peach-hover cate-a transition"> -->
                                    <a href='article.php?id=<?php echo $articlesFashion[$cnt]->getArticleLink();?>' class="peach-hover cate-a transition">
                                      <?php echo _HEADER_FASHION ?> <span class="grey-text">• <?php echo $articlesFashion[$cnt]->getDateCreated();?></span>
                                    </a>
                                </div>
								<a href='article.php?id=<?php echo $articlesFashion[$cnt]->getArticleLink();?>'>
                                    <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a">
                                        <?php echo $articlesFashion[$cnt]->getTitle();?>
                                    </div>
    
                                    <div class="text-content-div">
                                        <?php echo $articlesFashion[$cnt]->getParagraphOne();?>
                                    </div>
								</a>
                            </div>
                            
                        </div>
                    </a>
                    
                <?php
                }
                ?>
            <?php
            }
            $conn->close();
            ?>

            </div>


        </div>

    <!--tab 2 -->
    </div>

    <div class="clear"></div>

</div>

<?php include 'footer.php'; ?>

</body>
</html>