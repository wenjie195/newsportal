<div class="footer-red">
<p class="footer-p"><span class="flaticon-copyright-1 bigger-c white-text"></span> 2020 Tevy <?php echo _FOOTER_ALL_RIGHT ?>All Right Reserved</p>
</div>
<!-- Login Modal -->
<div id="login-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-login">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 darkpink-text login-h1"><?php echo _JS_LOGIN ?></h1>	   
   <div class="big-white-div">
		<div class="login-div">
         <form action="utilities/loginFunction.php" method="POST">
            <div class="login-input-div">
                <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
                <input class="aidex-input clean" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="username" name="username" required>
            </div>  
            <div class="login-input-div">
                <p class="input-top-text"><?php echo _JS_PASSWORD ?></p>
                <input class="aidex-input clean" type="password" placeholder="<?php echo _JS_PASSWORD ?>" id="password" name="password" required>
            </div>   

            <button class="clean-button clean login-btn pink-button" name="login"><?php echo _JS_LOGIN ?></button>
        </form>
          
      </div>                 
 </div>
        
                
	</div>

</div>



<script src="js/jquery-2.2.1.min.js" type="text/javascript"></script> 

<script>
document.onkeydown = function(e) {
    if(e.keyCode == 123) {
      return false;
     }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.keyCode == 'S'.charCodeAt(0)){
     return false;
    }
	 if(e.ctrlKey && e.keyCode == 'C'.charCodeAt(0)){
     return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){
     return false;
    }      
 }
</script>



  
<script>
function printFunction() {
    window.print();
}
</script>

<script src="js/bootstrap.js"></script>
<script src="js/ie-emulation-modes-warning.js"></script>
<script src="js/ie10-viewport-bug-workaround.js"></script>





<script src="js/main-login.js"></script>
<script src="js/headroom.js"></script>
<script>
(function() {
    var header = new Headroom(document.querySelector("#header"), {
        tolerance: 5,
        offset : 205,
        classes: {
          initial: "animated",
          pinned: "slideDown",
          unpinned: "slideUp"
        }
    });
    header.init();

    var bttHeadroom = new Headroom(document.getElementById("btt"), {
        tolerance : 0,
        offset : 500,
        classes : {
            initial : "slide",
            pinned : "slide--reset",
            unpinned : "slide--down"
        }
    });
    bttHeadroom.init();
}());
</script>

<script src="js/jquery-2.2.1.min.js" type="text/javascript"></script> 

<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
<script>

var loginmodal = document.getElementById("login-modal");
var deletemodal = document.getElementById("delete-modal");


var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var opendelete = document.getElementsByClassName("open-delete")[0];

var closelogin = document.getElementsByClassName("close-login")[0];
var closedelete = document.getElementsByClassName("close-delete")[0];
var closedelete1 = document.getElementsByClassName("close-delete")[1];


if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openlogin1){
openlogin1.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(opendelete){
opendelete.onclick = function() {
  deletemodal.style.display = "block";
}
}

if(closelogin){
  closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closedelete){
  closedelete.onclick = function() {
  deletemodal.style.display = "none";
}
}
if(closedelete1){
  closedelete1.onclick = function() {
  deletemodal.style.display = "none";
}
}
window.onclick = function(event) {

  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }  
  if (event.target == deletemodal) {
    deletemodal.style.display = "none";
  }    
}
</script>
<script>
function myFunction() {
  var x = document.getElementById("moreParagraph");
  var y = document.getElementById("addButton");
  if (x.style.display === "none") {
	x.style.display = "none";
  } else {
	x.style.display = "block";
	y.style.display = "none";
  }
}
</script>
  <!--this is for the posting article plugin in notepad any changes please refer to the script -->
  <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
  <!-- Just be careful that you give correct path to your tinymce.min.js file, above is the default example -->
  <script type="text/javascript">
  tinymce.init({
       selector: "textarea#post_article",
       theme: "modern",
       mode : "specific_textareas",
       editor_selector : "mceEditor",
       width:'auto',
       height: 300,
       //this are the layout controls of tinyMCE
       statusbar:false,
       branding: false,
       menubar:false,
       paste_as_text: true,
       autosave_ask_before_unload:true,
       plugins: [
              "autosave paste autolink link image lists charmap  preview hr anchor pagebreak spellchecker image",
              "searchreplace wordcount visualblocks visualchars  fullscreen  nonbreaking ",
              "save",
              "media"
      ],
      //content_css: "css/content.css",
      toolbar: "undo redo  | bold italic | link |  preview fullpage | restoredraft image media",
      style_formats: [
           {title: 'Bold text', inline: 'b'},
           {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
           {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
           {title: 'Example 1', inline: 'span', classes: 'example1'},
           {title: 'Example 2', inline: 'span', classes: 'example2'},
           {title: 'Table styles'},
           {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
       ],
   });
   //raw html content(fail to use modal might need some kind of database or popout message iframe)
   function content() {
     //Get the HTML contents of the currently active editor
     //console.debug(tinyMCE.activeEditor.getContent());
     //method1 getting the content of the active editor
     //console.log(tinyMCE.activeEditor.getContent());
     //method2 getting the content by id of a particular textarea
     tinymce.remove("post_article");
     console.log(tinyMCE.get('post_article').getContent());
  }
  //  var temp = tinymce.get('post_article').getContent();
  //  console.log(temp);

  </script>
