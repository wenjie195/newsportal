<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ArticleOne.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$articles = getArticlesOne($conn,"WHERE seo_title = ?",array("seo_title"),array($_GET['id']), "s");
$articlesTitle = $articles[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:image" content="https://chillitbuddy.com/images/facebook-preview.jpg" />-->
<meta property="og:title" content="<?php echo $articlesTitle->getTitle();?> | Tevy" />
<meta property="og:description" content="<?php echo $articlesTitle->getParagraphOne();?>" />
<meta name="description" content="<?php echo $articlesTitle->getParagraphOne();?>" />
<meta name="keywords" content="<?php echo $articlesTitle->getKeywordTwo();?>">
<!--<link rel="canonical" href="https://chillitbuddy.com/" />-->
<title><?php echo $articlesTitle->getTitle();?> | Tevy</title>

<?php include 'css.php'; ?>
</head>

<?php include 'header-after-login.php'; ?>

<?php 
        // Program to display URL of current page. 
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
        $link = "https"; 
        else
        $link = "http"; 

        // Here append the common URL characters. 
        $link .= "://"; 

        // Append the host(domain name, ip) to the URL. 
        $link .= $_SERVER['HTTP_HOST']; 

        // Append the requested resource location to the URL 
        $link .= $_SERVER['REQUEST_URI']; 

        // Print the link 
        // echo $link; 
        ?> 

        <?php
        $str1 = $link;
        $referrerUidLink = str_replace( 'http://localhost/newsportal/article001.php?id=', '', $str1);
        //seo title
        // echo $referrerUidLink;
?>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
$articlesDetails = getArticlesOne($conn,"WHERE seo_title = ? ", array("seo_title") ,array($_GET['id']),"s");
?>

  <?php
  if($articlesDetails)
  {
  for($cnt = 0;$cnt < count($articlesDetails) ;$cnt++)
  {
  ?>

<div class="background-div">
    <div class="cover-gap content min-height2">

  <div class="print-area" id="printarea">







    <div class="clear"></div>

    <h1 class="red-h1 slab darkpink-text"><?php echo $articlesTitle->getTitle();?></h1>

    <div class="left-contributor-div">
      <!--<p class="contributor-p">by <span class="pink-span"><?php echo $articlesDetails[$cnt]->getAuthorName();?></span>, Contributor</p>-->
      <!-- craft the category link here -->
      <p class="contributor-p"><a href="#" class="orange-p"><?php echo $articlesDetails[$cnt]->getType();?></a></p>
      <p class="blue-date"><?php echo $articlesDetails[$cnt]->getDateCreated();?></p>
    </div>
	<img src="uploads/<?php echo $articlesDetails[$cnt]->getTitleCover();;?>" class="width100 cover-photo">


    

  <div class="test">
  	<p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphOne();?></p>
    <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageOne();;?>" class="width100 article-photo">
    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphTwo();?></p>
    <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageTwo();;?>" class="width100 article-photo">
    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphThree();?></p>
    <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageThree();;?>" class="width100 article-photo">
    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphFour();?></p>
    <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageFour();;?>" class="width100 article-photo">
    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphFive();?></p>
    <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageFive();;?>" class="width100 article-photo">
    



  </div>

  </div>
  <div class="hide-print">
      <script async src="https://static.addtoany.com/menu/page.js"></script>
    <!-- AddToAny END -->
    <div class="clear"></div>
  <div class="width100 overflow same-padding">
              <!-- AddToAny BEGIN -->
        <div class="border-div"></div>
        <h3 class="darkpink-text share-h3">Share:</h3>
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
        <a class="a2a_button_copy_link"></a>
        <a class="a2a_button_facebook"></a>
        <a class="a2a_button_twitter"></a>
        <a class="a2a_button_linkedin"></a>
        <a class="a2a_button_blogger"></a>
        <a class="a2a_button_facebook_messenger"></a>
        <a class="a2a_button_whatsapp"></a>
        <a class="a2a_button_wechat"></a>
        <a class="a2a_button_line"></a>
        <a class="a2a_button_telegram"></a>
        <!--<a class="a2a_button_print"></a>-->
        </div> 
  </div>
  <div class="width100 overflow same-padding">
  	<div class="border-div"></div>
    <div id="disqus_thread"></div>
    <script>
    
    /**
    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
    /*
    var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */
    (function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = 'https://chillitbuddy-com-1.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
    })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript"></a></noscript>
     <script id="dsq-count-scr" src="//chillitbuddy-com-1.disqus.com/count.js" async></script>                           
                                
                                
                                
 <a href="https://bossinternational.asia/tevy/article001.php?id=<?php echo $articlesDetails[$cnt]->getSeoTitle();;?>#disqus_thread" class="live-hide"></a>
    
    </div>
    </div>
    
    </div>  
    </div>

  <?php
  }
  ?>
  <?php
  }
  ?>

  <div class="clear"></div>

  <?php
  }
  ?>



<?php include 'footer.php'; ?>

</body>
</html>