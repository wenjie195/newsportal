<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width; initial-scale = 1.0; 
maximum-scale=1.0; user-scalable=no" /> 

<meta property="og:image" content="https://chillitbuddy.com/images/balik-kampung-1.jpg" />
<meta property="og:title" content="Chill'it Buddy - The Journey" />
<meta property="og:description" content="Every year at this time of the season, mum would be calling me and I do not know how to answer her. Being in an advertising agency, Raya means even much more work for me. But not this year! This year I will be travelling back to my kampung. The place where I was brought up and the place I truly belong. Now, has been 7 years since I had been in Kuala Lumpur away from ibu and ayah. This is indeed the festive that I had been waiting for, to be back into ibu’s hugs and kisses." />
<meta name="author" content="Chill'it Buddy">
<meta name="keywords" content="chill it buddy, chill, it, buddy, cili padi,vidatech, chill'it buddy, chillitbuddy, chill it buddy, etc">


<title>Chill'it Buddy | The Journey</title>
<?php include 'css.php'; ?>




</head>
<?php include 'header.php'; ?>
<?php include 'ads1.php'; ?>


<div class="content article-div">
<div class="print-area" id="printarea">
<div class="top-article-row">
<div class="left-top-article">
<p class="orange-p"><a href="lifestyle.php" class="orange-p">Lifestyle</a></p></div>
<div class="right-top-article">

<!-- kindly plug in the php link for the article-->
<div class="fb-share-button print-hide" data-href="https://chillitbuddy.com/article001.php" data-layout="button_count" data-size="small" data-mobile-iframe="true">
<a class="fb-xfbml-parse-ignore"  href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fchillitbuddy.com%2Farticle001.php&amp;src=sdkpreparse" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fchillitbuddy.com%2Farticle001.php&amp;src=sdkpreparse', 'newwindow', 'width=300, height=250'); return false;" class="fb-share-a"><i class="flaticon-facebook-letter-logo fb-share"></i></a></div>


<a href="https://twitter.com/share" class="twitter-share-button print-hide" data-show-count="false" onclick="window.open('https://twitter.com/share', 'newwindow', 'width=300, height=250'); return false;"><i class="flaticon-twitter-logo-silhouette twitter-share"></i></a>
<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','images/bookmark2.png',1)" class="bookmark-a live-hide print-hide"><img src="images/bookmarka.png"  id="Image1" class="bookmark-img"></a></div></div>
<div class="clear"></div>
<h1 class="red-h1 slab">The Journey</h1>
<div class="left-contributor-div">
<p class="contributor-p">by <span class="pink-span">Siti Aida</span>, Contributor</p>
<p class="blue-date">24th of June 2017</p></div>
<div class="right-share-div print-hide">
<a href="" class="like-a live-hide"><i class="flaticon-like"></i><span class="like-amount">12k</span></a>
<a href="" class="comment-a live-hide"><i class="flaticon-blank-squared-bubble"></i><span class="comment-amount">20k</span></a>
<div class="dropdown dropdown-share">
<button class="btn btn-primary dropdown-toggle share-a" type="button" data-toggle="dropdown"><i class="flaticon-share-1"></i></button>


<div class="dropdown-menu dropdown-menu-share">

<div class="fb-share-button fb2" data-href="https://chillitbuddy.com/article001.php" data-layout="button_count" data-size="small" data-mobile-iframe="true">
<a class="fb-xfbml-parse-ignore share-dropdown-a fb-a"  href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fchillitbuddy.com%2Farticle001.php&amp;src=sdkpreparse" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fchillitbuddy.com%2Farticle001.php&amp;src=sdkpreparse', 'newwindow', 'width=300, height=250'); return false;" ><i class="flaticon-facebook fb-icon"></i> Facebook</a></div>
    
     <a href="https://twitter.com/share" class="twitter-share-button share-dropdown-a tw-a" data-show-count="false" onclick="window.open('https://twitter.com/share', 'newwindow', 'width=300, height=250'); return false;"><i class="flaticon-twitter tw-icon"></i> Twitter</a>
     <a href="#" class="share-dropdown-a print-a" onclick="printFunction()"><i class="flaticon-printing-tool print-icon"></i>  Print</a>
     <a data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url=https://www.chillitbuddy.com/article001.php&media=https://www.chillitbuddy.com/images/logo.png&description=Chill%20it%20Buddy!" data-pin-custom="true" class="share-dropdown-a pin-a" onclick="window.open('https://www.pinterest.com/pin/create/button/?url=https://www.chillitbuddy.com/article001.php&media=https://www.chillitbuddy.com/images/logo.png&description=Chill%20it%20Buddy!', 'newwindow', 'width=300, height=250'); return false;"><i class="flaticon-pinterest"></i> Pinterest</a>
   <a href="https://www.gmail.com/" class="share-dropdown-a print-a" target="_blank" ><i class="flaticon-gmail-logo gmail-icon"></i>  Gmail</a>
   <a href="https://plus.google.com/share?url=chillitbuddy.com/article001.php" onclick="window.open('https://plus.google.com/share?url=chillitbuddy.com/article001.php', 'newwindow', 'width=300, height=250'); return false;" class="share-dropdown-a"><i class="flaticon-google-plus-1 g-icon"></i> Google +</a>
   
</div>    
</div></div>
<div class="clear"></div>
<img src="images/balik-kampung-1.jpg" class="top-article-img">
<div class="test">
<p class="article-p">
“Aida, bila nak balik kampong ni?”
</p>

<p class="article-p">
“Mak, kerja la ni”
</p>
<p class="article-p">
Every year at this time of the season, mum would be calling me and I do not know how to answer her. Being in an advertising agency, Raya means even much more work for me. But not this year! This year I will be travelling back to my kampung. The place where I was brought up and the place I truly belong. Now, has been 7 years since I had been in Kuala Lumpur away from ibu and ayah. This is indeed the festive that I had been waiting for, to be back into ibu’s hugs and kisses.
</p>
<p class="article-p">My checklist for the trip back:</p>
<p class="article-p">-	One day earlier before travel, check vehicle to make sure in good conditions. <br>
-	Starting the journey pretty early to avoid heavy traffic, but still jam, sad right? <br>
-	Be aware of AES on the highway, keeping eye on crazy drivers. RM300, duit raya?<br>
-	Tune on the radio for Raya songs and sing along!!<br>
-	Remember to set GPS for the fastest route home. <br>
-	Refill petrol if needed.<br>
-	At R&R, have a break and let the kids play around. <br>
-	Must drop by Perak and stops by RTC Perak to buy a hamper for parents as a gift.
</p>

<p class="article-p">Finally, reach kampung and run towards ibu and ayah to give them my salam! Wishing all my friends and relatives safe journey back kampung tonight.</p>



<a href="https://www.facebook.com/ChillitBuddy/" target="_blank">
<img src="images/fb-ads.jpg" class="ads2"></a>

<div class="clear"></div>

<p class="share-p slab print-hide">Share</p>


<!-- AddToAny BEGIN -->
<!-- AddToAny BEGIN -->
<!-- AddToAny BEGIN -->
<!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style print-hide">

<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_google_plus"></a>
<a class="a2a_button_pinterest"></a>
<a class="a2a_button_linkedin"></a>
<a class="a2a_button_email"></a>
<a class="a2a_button_reddit"></a>
<a class="a2a_button_tumblr"></a>
<a class="a2a_button_google_gmail"></a>
<a class="a2a_button_blogger_post"></a>
<a class="a2a_button_copy_link"></a>
<a class="a2a_button_facebook_messenger"></a>
<a class="a2a_button_google_bookmarks"></a>
<a class="a2a_button_line"></a>
<a class="a2a_button_sina_weibo"></a>
<a class="a2a_button_skype"></a>
<a class="a2a_button_sms"></a>
<a class="a2a_button_wechat"></a>
<a class="a2a_button_yahoo_mail"></a>
</div>
<script async src="https://static.addtoany.com/menu/page.js"></script>
<div class="clear"></div>
<div class="pbottom">
<p class="share-p slab article-recommend print-hide">Some Recommended Articles</p>
<div class="clear"></div>

<div class="col-md-4 p4 mt5 print-hide">
<a href="article007.php" class="img-hover"><div class="column-image-div article007-pic"></div>
</a>
<a href="article007.php"><p class="title-p title-p-hover">Most Common Hari Raya Food
</p></a>
</div>

<div class="col-md-4 p4 mt5 print-hide">
<a href="article003.php" class="img-hover"><div class="column-image-div article003-pic"></div>
</a>
<a href="article003.php"><p class="title-p title-p-hover">Why we procrastinate?
</p></a>
</div>

<div class="col-md-4 p4 mt5 print-hide">
<a href="article002.php" class="img-hover"><div class="column-image-div article002-pic">
</div></a>
<a href="article002.php"><p class="title-p title-p-hover">Who are we?</p></a>
</div>

</div>
<div class="clear"></div>

<p class="share-p slab article-recommend print-hide">Discussion</p>
<div class="clear"></div>
<div id="disqus_thread"></div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://chillitbuddy-com-1.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript"></a></noscript>
 <script id="dsq-count-scr" src="//chillitbuddy-com-1.disqus.com/count.js" async></script>                           
                            
                            
                            
                             <a href="https://chillitbuddy.com/article001.php#disqus_thread" class="live-hide"></a>

</div></div>
  <div class="clear"></div>
</div>
</div><!-- end of content div--->





<?php include 'footer.php'; ?>

