<?php
class ArticlesOne {
    /* Member variables */
    var $id,$uid,$authorUid,$authorName,$title,$seoTitle,$articleLink,$keywordOne,$keywordTwo,$titleCover,$paragraphOne,$imageOne,$paragraphTwo,$imageTwo,$paragraphThree,$imageThree,
            $paragraphFour,$imageFour,$paragraphFive,$imageFive,$paragraphSix,$paragraphSeven,$paragraphEight,$paragraphNine,$paragraphTen,
                $imageSix,$imageSeven,$imageEight,$imageNine,$imageTen,$imgCoverSrc,$imgOneSrc,$imgTwoSrc,$imgThreeSrc,$imgFourSrc,$imgFiveSrc,$imgSixSrc,
                    $imgSevenSrc,$imgEightSrc,$imgNineSrc,$imgTenSrc,$headerTwo,$headerThree,$videoTwo,$videoThree,$vdSrcTwo,$vdSrcThree,$author,$type,$display,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getAuthorUid()
    {
        return $this->authorUid;
    }

    /**
     * @param mixed $authorUid
     */
    public function setAuthorUid($authorUid)
    {
        $this->authorUid = $authorUid;
    }

    /**
     * @return mixed
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * @param mixed $authorName
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * @param mixed $seoTitle
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    }

    /**
     * @return mixed
     */
    public function getArticleLink()
    {
        return $this->articleLink;
    }

    /**
     * @param mixed $articleLink
     */
    public function setArticleLink($articleLink)
    {
        $this->articleLink = $articleLink;
    }

    /**
     * @return mixed
     */
    public function getKeywordOne()
    {
        return $this->keywordOne;
    }

    /**
     * @param mixed $keywordOne
     */
    public function setKeywordOne($keywordOne)
    {
        $this->keywordOne = $keywordOne;
    }

    /**
     * @return mixed
     */
    public function getKeywordTwo()
    {
        return $this->keywordTwo;
    }

    /**
     * @param mixed $seoTitle
     */
    public function setKeywordTwo($keywordTwo)
    {
        $this->keywordTwo = $keywordTwo;
    }

    /**
     * @return mixed
     */
    public function getTitleCover()
    {
        return $this->titleCover;
    }

    /**
     * @param mixed $titleCover
     */
    public function setTitleCover($titleCover)
    {
        $this->titleCover = $titleCover;
    }

    /**
     * @return mixed
     */
    public function getParagraphOne()
    {
        return $this->paragraphOne;
    }

    /**
     * @param mixed $paragraphOne
     */
    public function setParagraphOne($paragraphOne)
    {
        $this->paragraphOne = $paragraphOne;
    }

    /**
     * @return mixed
     */
    public function getImageOne()
    {
        return $this->imageOne;
    }

    /**
     * @param mixed $imageOne
     */
    public function setImageOne($imageOne)
    {
        $this->imageOne = $imageOne;
    }

    /**
     * @return mixed
     */
    public function getParagraphTwo()
    {
        return $this->paragraphTwo;
    }

    /**
     * @param mixed $paragraphTwo
     */
    public function setParagraphTwo($paragraphTwo)
    {
        $this->paragraphTwo = $paragraphTwo;
    }

    /**
     * @return mixed
     */
    public function getImageTwo()
    {
        return $this->imageTwo;
    }

    /**
     * @param mixed $imageTwo
     */
    public function setImageTwo($imageTwo)
    {
        $this->imageTwo = $imageTwo;
    }

    /**
     * @return mixed
     */
    public function getParagraphThree()
    {
        return $this->paragraphThree;
    }

    /**
     * @param mixed $paragraphThree
     */
    public function setParagraphThree($paragraphThree)
    {
        $this->paragraphThree = $paragraphThree;
    }

    /**
     * @return mixed
     */
    public function getImageThree()
    {
        return $this->imageThree;
    }

    /**
     * @param mixed $imageThree
     */
    public function setImageThree($imageThree)
    {
        $this->imageThree = $imageThree;
    }

    /**
     * @return mixed
     */
    public function getParagraphFour()
    {
        return $this->paragraphFour;
    }

    /**
     * @param mixed $paragraphFour
     */
    public function setParagraphFour($paragraphFour)
    {
        $this->paragraphFour = $paragraphFour;
    }

    /**
     * @return mixed
     */
    public function getImageFour()
    {
        return $this->imageFour;
    }

    /**
     * @param mixed $imageFour
     */
    public function setImageFour($imageFour)
    {
        $this->imageFour = $imageFour;
    }

    /**
     * @return mixed
     */
    public function getParagraphFive()
    {
        return $this->paragraphFive;
    }

    /**
     * @param mixed $paragraphFive
     */
    public function setParagraphFive($paragraphFive)
    {
        $this->paragraphFive = $paragraphFive;
    }

    /**
     * @return mixed
     */
    public function getImageFive()
    {
        return $this->imageFive;
    }

    /**
     * @param mixed $imageFive
     */
    public function setImageFive($imageFive)
    {
        $this->imageFive = $imageFive;
    }

    /**
     * @return mixed
     */
    public function getParagraphSix()
    {
        return $this->paragraphSix;
    }

    /**
     * @param mixed $paragraphSix
     */
    public function setParagraphSix($paragraphSix)
    {
        $this->paragraphSix = $paragraphSix;
    }

    /**
     * @return mixed
     */
    public function getParagraphSeven()
    {
        return $this->paragraphSeven;
    }

    /**
     * @param mixed $paragraphSeven
     */
    public function setParagraphSeven($paragraphSeven)
    {
        $this->paragraphSeven = $paragraphSeven;
    }

    /**
     * @return mixed
     */
    public function getParagraphEight()
    {
        return $this->paragraphEight;
    }

    /**
     * @param mixed $paragraphEight
     */
    public function setParagraphEight($paragraphEight)
    {
        $this->paragraphEight = $paragraphEight;
    }

    /**
     * @return mixed
     */
    public function getParagraphNine()
    {
        return $this->paragraphNine;
    }

    /**
     * @param mixed $paragraphNine
     */
    public function setParagraphNine($paragraphNine)
    {
        $this->paragraphNine = $paragraphNine;
    }

    /**
     * @return mixed
     */
    public function getParagraphTen()
    {
        return $this->paragraphTen;
    }

    /**
     * @param mixed $paragraphTen
     */
    public function setParagraphTen($paragraphTen)
    {
        $this->paragraphTen = $paragraphTen;
    }

    /**
     * @return mixed
     */
    public function getImageSix()
    {
        return $this->imageSix;
    }

    /**
     * @param mixed $imageSix
     */
    public function setImageSix($imageSix)
    {
        $this->imageSix = $imageSix;
    }

    /**
     * @return mixed
     */
    public function getImageSeven()
    {
        return $this->imageSeven;
    }

    /**
     * @param mixed $imageSeven
     */
    public function setImageSeven($imageSeven)
    {
        $this->imageSeven = $imageSeven;
    }

    /**
     * @return mixed
     */
    public function getImageEight()
    {
        return $this->imageEight;
    }

    /**
     * @param mixed $imageSix
     */
    public function setImageEight($imageEight)
    {
        $this->imageEight = $imageEight;
    }

    /**
     * @return mixed
     */
    public function getImageNine()
    {
        return $this->imageNine;
    }

    /**
     * @param mixed $imageSix
     */
    public function setImageNine($imageNine)
    {
        $this->imageNine = $imageNine;
    }

    /**
     * @return mixed
     */
    public function getImageTen()
    {
        return $this->imageTen;
    }

    /**
     * @param mixed $imageTen
     */
    public function setImageTen($imageTen)
    {
        $this->imageTen = $imageTen;
    }

    /**
     * @return mixed
     */
    public function getImgCoverSrc()
    {
        return $this->imgCoverSrc;
    }

    /**
     * @param mixed $imgCoverSrc
     */
    public function setImgCoverSrc($imgCoverSrc)
    {
        $this->imgCoverSrc = $imgCoverSrc;
    }

    /**
     * @return mixed
     */
    public function getImgOneSrc()
    {
        return $this->imgOneSrc;
    }

    /**
     * @param mixed $imgOneSrc
     */
    public function setImgOneSrc($imgOneSrc)
    {
        $this->imgOneSrc = $imgOneSrc;
    }

    /**
     * @return mixed
     */
    public function getImgTwoSrc()
    {
        return $this->imgTwoSrc;
    }

    /**
     * @param mixed $imgTwoSrc
     */
    public function setImgTwoSrc($imgTwoSrc)
    {
        $this->imgTwoSrc = $imgTwoSrc;
    }

    /**
     * @return mixed
     */
    public function getImgThreeSrc()
    {
        return $this->imgThreeSrc;
    }

    /**
     * @param mixed $imgThreeSrc
     */
    public function setImgThreeSrc($imgThreeSrc)
    {
        $this->imgThreeSrc = $imgThreeSrc;
    }

    /**
     * @return mixed
     */
    public function getImgFourSrc()
    {
        return $this->imgFourSrc;
    }

    /**
     * @param mixed $imgFourSrc
     */
    public function setImgFourSrc($imgFourSrc)
    {
        $this->imgFourSrc = $imgFourSrc;
    }

    /**
     * @return mixed
     */
    public function getImgFiveSrc()
    {
        return $this->imgFiveSrc;
    }

    /**
     * @param mixed $imgFiveSrc
     */
    public function setImgFiveSrc($imgFiveSrc)
    {
        $this->imgFiveSrc = $imgFiveSrc;
    }

    /**
     * @return mixed
     */
    public function getImgSixSrc()
    {
        return $this->imgSixSrc;
    }

    /**
     * @param mixed $imgSixSrc
     */
    public function setImgSixSrc($imgSixSrc)
    {
        $this->imgSixSrc = $imgSixSrc;
    }

    /**
     * @return mixed
     */
    public function getImgSevenSrc()
    {
        return $this->imgSevenSrc;
    }

    /**
     * @param mixed $imgSevenSrc
     */
    public function setImgSevenSrc($imgSevenSrc)
    {
        $this->imgSevenSrc = $imgSevenSrc;
    }

    /**
     * @return mixed
     */
    public function getImgEightSrc()
    {
        return $this->imgEightSrc;
    }

    /**
     * @param mixed $imgEightSrc
     */
    public function setImgEightSrc($imgEightSrc)
    {
        $this->imgEightSrc = $imgEightSrc;
    }

    /**
     * @return mixed
     */
    public function getImgNineSrc()
    {
        return $this->imgNineSrc;
    }

    /**
     * @param mixed $imgNineSrc
     */
    public function setImgNineSrc($imgNineSrc)
    {
        $this->imgNineSrc = $imgNineSrc;
    }

    /**
     * @return mixed
     */
    public function getImgTenSrc()
    {
        return $this->imgTenSrc;
    }

    /**
     * @param mixed $imgTenSrc
     */
    public function setImgTenSrc($imgTenSrc)
    {
        $this->imgTenSrc = $imgTenSrc;
    }

    /**
     * @return mixed
     */
    public function getHeaderTwo()
    {
        return $this->headerTwo;
    }

    /**
     * @param mixed $headerTwo
     */
    public function setHeaderTwo($headerTwo)
    {
        $this->headerTwo = $headerTwo;
    }

    /**
     * @return mixed
     */
    public function getHeaderThree()
    {
        return $this->headerThree;
    }

    /**
     * @param mixed $headerThree
     */
    public function setHeaderThree($headerThree)
    {
        $this->headerThree = $headerThree;
    }

    /**
     * @return mixed
     */
    public function getVideoTwo()
    {
        return $this->videoTwo;
    }

    /**
     * @param mixed $videoTwo
     */
    public function setVideoTwo($videoTwo)
    {
        $this->videoTwo = $videoTwo;
    }

    /**
     * @return mixed
     */
    public function getVideoThree()
    {
        return $this->videoThree;
    }

    /**
     * @param mixed $videoThree
     */
    public function setVideoThree($videoThree)
    {
        $this->videoThree = $videoThree;
    }

    /**
     * @return mixed
     */
    public function getVdSrcTwo()
    {
        return $this->vdSrcTwo;
    }

    /**
     * @param mixed $vdSrcTwo
     */
    public function setVdSrcTwo($vdSrcTwo)
    {
        $this->vdSrcTwo = $vdSrcTwo;
    }

        /**
     * @return mixed
     */
    public function getVdSrcThree()
    {
        return $this->vdSrcThree;
    }

    /**
     * @param mixed $vdSrcThree
     */
    public function setVdSrcThree($vdSrcThree)
    {
        $this->vdSrcThree = $vdSrcThree;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getArticlesOne($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","author_uid","author_name","title","seo_title","article_link","keyword_one","keyword_two","title_cover","paragraph_one","image_one","paragraph_two","image_two",
                                "paragraph_three","image_three","paragraph_four","image_four","paragraph_five","image_five","paragraph_six","paragraph_seven","paragraph_eight",
                                    "paragraph_nine","paragraph_ten","image_six","image_seven","image_eight","image_nine","image_ten","img_cover_source",
                                    "img_one_source","img_two_source","img_three_source","img_four_source","img_five_source","img_six_source","img_seven_source","img_eight_source",
                                        "img_nine_source","img_ten_source","header_two","header_three","video_two","video_three","vd_src_two","vd_src_three","author","type","display","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"articles_one");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$authorUid,$authorName,$title,$seoTitle,$articleLink,$keywordOne,$keywordTwo,$titleCover,$paragraphOne,$imageOne,$paragraphTwo,$imageTwo,$paragraphThree,$imageThree,
                                $paragraphFour,$imageFour,$paragraphFive,$imageFive,$paragraphSix,$paragraphSeven,$paragraphEight,$paragraphNine,$paragraphTen,
                                    $imageSix,$imageSeven,$imageEight,$imageNine,$imageTen,$imgCoverSrc,$imgOneSrc,$imgTwoSrc,$imgThreeSrc,$imgFourSrc,$imgFiveSrc,$imgSixSrc,
                                        $imgSevenSrc,$imgEightSrc,$imgNineSrc,$imgTenSrc,$headerTwo,$headerThree,$videoTwo,$videoThree,$vdSrcTwo,$vdSrcThree,$author,$type,$display,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new ArticlesOne;
            $class->setId($id);
            $class->setUid($uid);

            $class->setAuthorUid($authorUid);
            $class->setAuthorName($authorName);
            $class->setTitle($title);

            $class->setSeoTitle($seoTitle);

            $class->setArticleLink($articleLink);

            $class->setKeywordOne($keywordOne);
            $class->setKeywordTwo($keywordTwo);

            $class->setTitleCover($titleCover);

            $class->setParagraphOne($paragraphOne);
            $class->setImageOne($imageOne);
            $class->setParagraphTwo($paragraphTwo);
            $class->setImageTwo($imageTwo);

            $class->setParagraphThree($paragraphThree);
            $class->setImageThree($imageThree);
            $class->setParagraphFour($paragraphFour);
            $class->setImageFour($imageFour);
            $class->setParagraphFive($paragraphFive);
            $class->setImageFive($imageFive);

            $class->setParagraphSix($paragraphSix);
            $class->setParagraphSeven($paragraphSeven);
            $class->setParagraphEight($paragraphEight);

            $class->setParagraphNine($paragraphNine);
            $class->setParagraphTen($paragraphTen);
            $class->setImageSix($imageSix);
            $class->setImageSeven($imageSeven);
            $class->setImageEight($imageEight);
            $class->setImageNine($imageNine);
            $class->setImageTen($imageTen);

            $class->setImgCoverSrc($imgCoverSrc);

            $class->setImgOneSrc($imgOneSrc);
            $class->setImgTwoSrc($imgTwoSrc);
            $class->setImgThreeSrc($imgThreeSrc);
            $class->setImgFourSrc($imgFourSrc);
            $class->setImgFiveSrc($imgFiveSrc);
            $class->setImgSixSrc($imgSixSrc);
            $class->setImgSevenSrc($imgSevenSrc);
            $class->setImgEightSrc($imgEightSrc);
            $class->setImgNineSrc($imgNineSrc);
            $class->setImgTenSrc($imgTenSrc);

            $class->setHeaderTwo($headerTwo);
            $class->setHeaderThree($headerThree);
            $class->setVideoTwo($videoTwo);
            $class->setVideoThree($videoThree);
            $class->setVdSrcTwo($vdSrcTwo);
            $class->setVdSrcThree($vdSrcThree);

            $class->setAuthor($author);

            $class->setType($type);
            $class->setDisplay($display);

            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
