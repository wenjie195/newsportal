<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width; initial-scale = 1.0; 
maximum-scale=1.0; user-scalable=no" /> 

<meta property="og:image" content="https://chillitbuddy.com/images/facebook-preview.jpg" />
<meta property="og:title" content="Chill'it Buddy" />
<meta property="og:description" content="Chill'it Buddy - We are the awesome Chili Family! We are a social news and content website that gathers and share the latest, trendiest, spiciest news from all around the world." />
<meta name="author" content="Chill'it Buddy">
<meta name="keywords" content="chill it buddy, chill, it, buddy, cili padi,vidatech, chill'it buddy, chillitbuddy, chill it buddy, etc">
<link rel="canonical" href="https://chillitbuddy.com/" />
<title>Chill'it Buddy</title>
<?php include 'css.php'; ?>




</head>



 <div class="content wrapper">
    <div class="test wrapper">
      <h1 class="profile-title"><b>Write a New Article</b></h1>
      <form action="" method="post" id="profile">
        <div class="form-group">
          <h4  class="title-color"><b class="family">Title</b><h4>
          <input type="text" class="form-control form-control-border border-fix" placeholder="" title="post_article" />
        </div>
      <div class="form-group publish-border">
        <h4 class="title-color"><b>Upload Feature Photo</b></h4>
        <input id="upload" type="file"></input>
        <a href="" id="upload_photo_article" class="btn btn-default upload-article-image"><span style="color:black;">Browse</span></a>
      </div>
      <div class="form-group publish-border">
          <h4 class="title-color"><b class="family">Body Text</b></h4>
          <textarea id="post_article" name="area" class="form-control mceEditor" rows="2"></textarea>
      </div>
          <div class="form-group">
            <h4 class="title-color"><b class="family">Category</b></h4>
             <select class="form-control border-fix font-fix">
        <option selected class="color-fix">Hot News</option>
        <option selected class="color-fix">Trendy</option>
        <option selected class="color-fix">Food</option>
        <option selected class="color-fix">Lifestyle</option>
        <option selected class="color-fix">Sports</option>
        <option selected class="color-fix">Entertainment</option>
        <option selected class="color-fix">Science & Tech</option>
        <option selected class="color-fix">Art & Design</option>
        <option selected class="color-fix">Advertising & Marketing</option>
        <option selected class="color-fix">Humor</option>
      </select>
          </div>
          <div class="mobile publish-border">
            <div class="pull-right">
           <a href="#" onclick="content()" class="pencil-a" title="html_embed"><i class="flaticon-edit-1 pencil-write" aria-hidden="true"></i></a>
            <button type="button" class="btn btn-default draft"><b class="savedraft">Save as draft</b></button>
            <!-- <a href="#" onclick="content()">get raw content</a> -->
            <button type="submit" class="btn btn-default save">Publish</button>
          </div>
        </div>
      </form>
  </div>

</div>
  <div class="clear"></div>


<?php include 'footer.php'; ?>
