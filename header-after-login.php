<?php
if(isset($_SESSION['uid']))
{
?>

<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php"><img src="img/logo.png" class="logo-img" alt="Tevy" title="Tevy"></a>
            </div>



            <div class="right-menu-div float-right before-header-div">

                <a href="index.php" class="white-text menu-margin-right menu-item opacity-hover">
                    <?php echo _HEADER_HOME ?>
                </a>             
            	<a href="beautyCare.php" class="white-text menu-margin-right menu-item opacity-hover">
                    <?php echo _HEADER_BEAUTY ?>
                </a>  
            	<a href="trendyFashion.php" class="white-text menu-margin-right menu-item opacity-hover">
                	<?php echo _HEADER_FASHION ?>
                </a>             
            	<a href="socialNews.php" class="white-text menu-margin-right menu-item opacity-hover">
                	<?php echo _HEADER_SOCIAL ?>
                </a>  
                <div class="dropdown  white-text menu-item">
                 <?php echo _HEADER_ARTICLE ?>   
               
                            	<img src="img/dropdown.png" class="dropdown-img" alt="<?php echo _HEADER_ARTICLE ?>" title="<?php echo _HEADER_ARTICLE ?>">

                	<div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p pink-hover"><a href="userUploadArticles.php"  class="menu-padding dropdown-a menu-a pink-hover"><?php echo _HEADER_UPLOAD_ARTICLE ?></a></p>
                        <p class="dropdown-p pink-hover"><a href="viewArticles.php"  class="menu-padding dropdown-a menu-a pink-hover"><?php echo _HEADER_EDIT_ARTICLE ?></a></p>
                	</div>
                </div>            


                <div class="dropdown  white-text menu-item">
                    <?php echo _HEADER_LANGUAGE ?>
                    <img src="img/dropdown.png" class="dropdown-img" alt="<?php echo _HEADER_LANGUAGE ?>" title="<?php echo _HEADER_LANGUAGE ?>">
                    <div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p pink-hover"><a href="<?php $link ?>?lang=en" class="menu-padding dropdown-a menu-a pink-hover">English</a></p>
                        <p class="dropdown-p pink-hover"><a href="<?php $link ?>?lang=ch" class="menu-padding dropdown-a menu-a pink-hover">中文</a></p>
                    </div>
                </div>  


            	<!-- <a href="logout.php"  class="white-text menu-margin-right menu-item opacity-hover">
                	Logout
                </a>                  -->
                <a href="logout.php"  class="white-text menu-margin-right menu-item opacity-hover">
                    <?php echo _MAINJS_ALL_LOGOUT ?>
                </a>   
          
                           	<div id="dl-menu" class="dl-menuwrapper before-dl">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                  <li><a href="userUploadArticles.php"><?php echo _HEADER_UPLOAD_ARTICLE ?></a></li>
                                  <li><a href="viewArticles.php"><?php echo _HEADER_EDIT_ARTICLE ?></a></li>                                
                                  <li><a href="index.php"><?php echo _HEADER_HOME ?></a></li>
                                  <li><a href="beautyCare.php"><?php echo _HEADER_BEAUTY ?></a></li>
                                  <li><a href="trendyFashion.php"><?php echo _HEADER_FASHION ?></a></li>  
                                  <li><a href="socialNews.php"><?php echo _HEADER_SOCIAL ?></a></li>  
                                  <li><a href="<?php $link ?>?lang=en">Switch to English</a></li>  
                                  <li><a href="<?php $link ?>?lang=ch">转换成中文</a></li>                                                               
								  <li><a  href="logout.php"><?php echo _MAINJS_ALL_LOGOUT ?></a></li>
                                </ul>
							</div><!-- /dl-menuwrapper -->  
                                       	
            </div>
        </div>

</header>

<?php
}
else
{
?>

<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <a href="index.php"><img src="img/logo.png" class="logo-img" alt="Tevy" title="Tevy"></a>
            </div>



            <div class="right-menu-div float-right before-header-div">
            	<!-- <a href="index.php" class="white-text menu-margin-right menu-item opacity-hover">
                	Home
                </a>             
            	<a href="beautyCare.php" class="white-text menu-margin-right menu-item opacity-hover">
                	Beauty
                </a>   -->
                <a href="index.php" class="white-text menu-margin-right menu-item opacity-hover">
                    <?php echo _HEADER_HOME ?>
                </a>             
            	<a href="beautyCare.php" class="white-text menu-margin-right menu-item opacity-hover">
                    <?php echo _HEADER_BEAUTY ?>
                </a> 
            	<a href="trendyFashion.php" class="white-text menu-margin-right menu-item opacity-hover">
                	<?php echo _HEADER_FASHION ?>
                </a>             
            	<a href="socialNews.php" class="white-text menu-margin-right menu-item opacity-hover">
                	<?php echo _HEADER_SOCIAL ?>
                </a>   
                
                
                <div class="dropdown  white-text menu-item">
                    <?php echo _HEADER_LANGUAGE ?>
                    <img src="img/dropdown.png" class="dropdown-img" alt="<?php echo _HEADER_LANGUAGE ?>" title="<?php echo _HEADER_LANGUAGE ?>">
                    <div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en" class="menu-padding dropdown-a menu-a pink-hover">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch" class="menu-padding dropdown-a menu-a pink-hover">中文</a></p>
                    </div>
                </div>  


            	<!-- <a  class="white-text menu-margin-right menu-item opacity-hover open-login">
                	Login
                </a>                  -->

                <a  class="white-text menu-margin-right menu-item opacity-hover open-login">
                    <?php echo _MAINJS_INDEX_LOGIN ?>
                </a>     
          
                           	<div id="dl-menu" class="dl-menuwrapper before-dl">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                  <li><a href="index.php"><?php echo _HEADER_HOME ?></a></li>
                                  <li><a href="beautyCare.php"><?php echo _HEADER_BEAUTY ?></a></li>
                                  <li><a href="trendyFashion.php"><?php echo _HEADER_FASHION ?></a></li>  
                                  <li><a href="socialNews.php"><?php echo _HEADER_SOCIAL ?></a></li>                              
                                  <li><a href="<?php $link ?>?lang=en">Switch to English</a></li>  
                                  <li><a href="<?php $link ?>?lang=ch">转换成中文</a></li> 								  
                                  <li><a class="open-login"><?php echo _MAINJS_INDEX_LOGIN ?></a></li>
                                </ul>
							</div><!-- /dl-menuwrapper -->  
                                       	
            </div>
        </div>

</header>

<?php
}
?>