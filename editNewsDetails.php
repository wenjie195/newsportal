<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ArticleOne.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="Edit Article | Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">
<link rel="canonical" href="https://tevy.asia/editNewsDetails.php" />

<title>Edit Article | Tevy</title>
<?php include 'css.php'; ?>
</head>

<body>
<?php include 'header-after-login.php'; ?>

<div class="background-div">
    <div class="cover-gap content min-height2">
        <div class="big-white-div same-padding">

        <?php
        if(isset($_POST['news_uid']))
        {
        $conn = connDB();
        $articlesDetails = getArticlesOne($conn,"WHERE uid = ? ", array("uid") ,array($_POST['news_uid']),"s");
        ?>

       <form action="utilities/updateArticlesFunction.php" method="POST" enctype="multipart/form-data">
        	<h1 class="landing-h1 margin-left-0"><?php echo _EDIT_ARTICLE ?></h1>    

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_TITLE ?></p>            
                <!-- <input class="aidex-input clean" type="text" value="<?php //echo $articlesDetails[0]->getTitle(); ?>" id="update_title" name="update_title" required> -->

                <textarea class="one-line-textarea aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_TITLE ?>" id="update_title" name="update_title"><?php echo $articlesDetails[0]->getTitle(); ?></textarea>

            </div>  

            <div class="input-div">
                <p class="input-top-text darkpink-text"><?php echo _UPLOAD_ARTICLE_LINK ?></p>
                <!-- <input class="aidex-input clean" type="text" value="<?php //echo $articlesDetails[0]->getArticleLink(); ?>" id="update_article_link" name="update_article_link" required> -->
                <textarea class="three-line-textarea aidex-input clean" type="text" placeholder="how-to-stay-healthy" id="update_article_link" name="update_article_link"><?php echo $articlesDetails[0]->getArticleLink(); ?></textarea>
            </div>  

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_GOOGLE_KEYWORD ?></p>
                <!-- <input class="aidex-input clean" type="text" value="<?php //echo $articlesDetails[0]->getKeywordTwo(); ?>" id="update_keyword_two" name="update_keyword_two" required> -->
                <textarea class="three-line-textarea aidex-input clean" type="text" placeholder="beautiful, skin care, health" id="update_keyword_two" name="update_keyword_two"><?php echo $articlesDetails[0]->getKeywordTwo(); ?></textarea>
            </div>  

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_AUTHOR ?></p>
                <input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_AUTHOR ?>" value="<?php echo $articlesDetails[0]->getAuthor(); ?>" id="update_author" name="update_author">
            </div>  

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_CATEGORY ?> : <?php echo $articlesDetails[0]->getType(); ?></p>

                <select class="clean aidex-input" type="text" id="update_type" name="update_type" required>

                    <?php
                        if($articlesDetails[0]->getType() == 'Beauty')
                        {
                        ?>
                            <option selected value="Beauty"  name='Beauty'><?php echo _HEADER_BEAUTY ?></option>
                            <option value="Fashion"  name='Fashion'><?php echo _HEADER_FASHION ?></option>
                            <option value="Social"  name='Social'><?php echo _HEADER_SOCIAL ?></option>
                        <?php
                        }
                        else if($articlesDetails[0]->getType() == 'Fashion')
                        {
                        ?>
                            <option selected value="Fashion"  name='Fashion'><?php echo _HEADER_FASHION ?></option>
                            <option value="Beauty"  name='Beauty'><?php echo _HEADER_BEAUTY ?></option>
                            <option value="Social"  name='Social'><?php echo _HEADER_SOCIAL ?></option>
                        <?php
                        }
                        else if($articlesDetails[0]->getType() == 'Social')
                        {
                        ?>
                            <option selected value="Social"  name='Social'><?php echo _HEADER_SOCIAL ?></option>
                            <option value="Beauty"  name='Beauty'><?php echo _HEADER_BEAUTY ?></option>
                            <option value="Fashion"  name='Fashion'><?php echo _HEADER_FASHION ?></option>
                        <?php
                        }
                    ?>

                </select>                
            </div> 

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO ?></p>
                <input id="file-upload" type="file" name="update_cover_photo" id="update_cover_photo" accept="image/*">      

                <img src="uploads/<?php echo $articlesDetails[0]->getTitleCover();;?>" class="width100 article-photo">

            </div>             

            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>        
             	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgCoverSrc(); ?>" id="update_cover_photo_source" name="update_cover_photo_source" > 
			</div>
            
            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 1</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_PARAGRAPH ?> 1" value="<?php //echo $articlesDetails[0]->getParagraphOne(); ?>" id="update_paragraph_one" name="update_paragraph_one" required> -->
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 1"  id="update_paragraph_one" name="update_paragraph_one"><?php echo $articlesDetails[0]->getParagraphOne(); ?></textarea>
            </div>  

            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 1 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="update_image_one" id="update_image_one" accept="image/*">                   
                <?php $image1 = $articlesDetails[0]->getImageOne();
                if($image1 !=  '')
                {
                ?>
                    <img src="uploads/<?php echo $articlesDetails[0]->getImageOne();?>" class="width100 article-photo">
                <?php
                }
                else
                {
                ?>
                    No Image
                <?php
                }
                ?>
            </div> 

            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>  
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgOneSrc(); ?>" id="update_image_one_source" name="update_image_one_source">
            </div>
            
            
            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 2</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_PARAGRAPH ?> 2" value="<?php //echo $articlesDetails[0]->getParagraphTwo(); ?>" id="update_paragraph_two" name="update_paragraph_two"> -->
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 2" id="update_paragraph_two" name="update_paragraph_two"><?php echo $articlesDetails[0]->getParagraphTwo(); ?></textarea>
            </div>  
            <div class="para-photo delete-para-div">
                <p class="input-top-text"><?php echo _EDIT_ARTICLE_DELETE_PARAGRAPH ?></p>
                <select class="clean aidex-input" type="text" id="delete_paragraph_two" name="delete_paragraph_two">
                    <option value=""  name=''><?php echo _EDIT_ARTICLE_NO ?></option>
                    <option value="Delete"  name='Delete'><?php echo _EDIT_ARTICLE_YES ?></option>
                </select>  
			</div>
            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 2 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="update_image_two" id="update_image_two" accept="image/*" />            
                <?php $image2 = $articlesDetails[0]->getImageTwo();
                if($image2 !=  'Delete')
                {
                ?>
                    <img src="uploads/<?php echo $articlesDetails[0]->getImageTwo();?>" class="width100 article-photo">
                <?php
                }
                else
                {
                ?>
                    No Image
                <?php
                }
                ?>
            </div> 
            <div class="para-photo delete-para-div">
            	<p class="input-top-text"><?php echo _EDIT_ARTICLE_DELETE_PHOTO ?></p>
            <select class="clean aidex-input" type="text" id="delete_img_two" name="delete_img_two">
                <option value=""  name=''><?php echo _EDIT_ARTICLE_NO ?></option>
                <option value="Delete"  name='Delete'><?php echo _EDIT_ARTICLE_YES ?></option>
                <!-- <option value="Remain"  name='Remain'>Remain</option> -->
            </select>   
			</div>
            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>  
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgTwoSrc(); ?>" id="update_image_two_source" name="update_image_two_source">
            </div>
            <div class="para-photo delete-para-div">
            	<p class="input-top-text"><?php echo _EDIT_ARTICLE_DELETE_PHOTO_SOURCE ?></p>            
                <select class="clean aidex-input" type="text" id="delete_img_src_two" name="delete_img_src_two">
                    <option value=""  name=''><?php echo _EDIT_ARTICLE_NO ?></option>
                    <option value="Delete"  name='Delete'><?php echo _EDIT_ARTICLE_YES ?></option>
                </select>   
            </div>
            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 3</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_PARAGRAPH ?> 3" value="<?php //echo $articlesDetails[0]->getParagraphThree(); ?>" id="update_paragraph_three" name="update_paragraph_three" > -->
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 3" id="update_paragraph_three" name="update_paragraph_three" ><?php echo $articlesDetails[0]->getParagraphThree(); ?></textarea>
            </div>  
            <div class="para-photo delete-para-div">
                <p class="input-top-text"><?php echo _EDIT_ARTICLE_DELETE_PARAGRAPH ?></p> 
                <select class="clean aidex-input" type="text" id="delete_paragraph_three" name="delete_paragraph_three">
                    <option value=""  name=''><?php echo _EDIT_ARTICLE_NO ?></option>
                    <option value="Delete"  name='Delete'><?php echo _EDIT_ARTICLE_YES ?></option>
                </select>  
			</div>
            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 3 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="update_image_three" id="update_image_three" accept="image/*" />        
                
                <?php $image3 = $articlesDetails[0]->getImageThree();
                if($image3 !=  'Delete')
                {
                ?>
                    <img src="uploads/<?php echo $articlesDetails[0]->getImageThree();?>" class="width100 article-photo">
                <?php
                }
                else
                {
                ?>
                    No Image
                <?php
                }
                ?>
            </div> 
            <div class="para-photo delete-para-div">
            	<p class="input-top-text"><?php echo _EDIT_ARTICLE_DELETE_PHOTO ?></p>
                <select class="clean aidex-input" type="text" id="delete_img_three" name="delete_img_three">
                    <option value=""  name=''><?php echo _EDIT_ARTICLE_NO ?></option>
                    <option value="Delete"  name='Delete'><?php echo _EDIT_ARTICLE_YES ?></option>
                    <!-- <option value="Remain"  name='Remain'>Remain</option> -->
                </select>   
			</div>
            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>  
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgThreeSrc(); ?>" id="update_image_three_source" name="update_image_three_source">
            </div>
            <div class="para-photo delete-para-div">
            	<p class="input-top-text"><?php echo _EDIT_ARTICLE_DELETE_PHOTO_SOURCE ?></p>             
                <select class="clean aidex-input" type="text" id="delete_img_src_three" name="delete_img_src_three">
                    <option value=""  name=''><?php echo _EDIT_ARTICLE_NO ?></option>
                    <option value="Delete"  name='Delete'><?php echo _EDIT_ARTICLE_YES ?></option>
                </select>  
            </div>
            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 4</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_PARAGRAPH ?> 4" value="<?php //echo $articlesDetails[0]->getParagraphFour(); ?>" id="update_paragraph_four" name="update_paragraph_four" > -->
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 4" id="update_paragraph_four" name="update_paragraph_four" ><?php echo $articlesDetails[0]->getParagraphFour(); ?></textarea>
            </div>  
 

            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 4 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="update_image_four" id="update_image_four" accept="image/*" />         
                
                <?php $image4 = $articlesDetails[0]->getImageFour();
                if($image4 !=  '')
                {
                ?>
                    <img src="uploads/<?php echo $articlesDetails[0]->getImageFour();?>" class="width100 article-photo">
                <?php
                }
                else
                {
                ?>
                    No Image
                <?php
                }
                ?>
            </div> 

            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>  
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgFourSrc(); ?>" id="update_image_four_source" name="update_image_four_source">
            </div>
            
            
            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 5</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_PARAGRAPH ?> 5" value="<?php //echo $articlesDetails[0]->getParagraphFive(); ?>" id="update_paragraph_five" name="update_paragraph_five" > -->
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 5" id="update_paragraph_five" name="update_paragraph_five" ><?php echo $articlesDetails[0]->getParagraphFive(); ?></textarea>
            </div>  
 

            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 5 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="update_image_five" id="update_image_five" accept="image/*" />        
                
                <?php $image5 = $articlesDetails[0]->getImageFive();
                if($image5 !=  '')
                {
                ?>
                    <img src="uploads/<?php echo $articlesDetails[0]->getImageFive();?>" class="width100 article-photo">
                <?php
                }
                else
                {
                ?>
                    No Image
                <?php
                }
                ?>
            </div> 

            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>  
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgFiveSrc(); ?>" id="update_image_five_source" name="update_image_five_source">
            </div>

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 6</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_PARAGRAPH ?> 6" value="<?php //echo $articlesDetails[0]->getParagraphSix(); ?>" id="update_paragraph_six" name="update_paragraph_six" > -->
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 6" id="update_paragraph_six" name="update_paragraph_six" ><?php echo $articlesDetails[0]->getParagraphSix(); ?></textarea>
            </div>  
 
            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 6 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="update_image_six" id="update_image_six" accept="image/*" />        
                <?php $image6 = $articlesDetails[0]->getImageSix();
                if($image6 !=  '')
                {
                ?>
                    <img src="uploads/<?php echo $articlesDetails[0]->getImageSix();?>" class="width100 article-photo">
                <?php
                }
                else
                {
                ?>
                    No Image
                <?php
                }
                ?>
            </div> 

            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>  
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgSixSrc(); ?>" id="update_image_six_source" name="update_image_six_source">
            </div>

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 7</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_PARAGRAPH ?> 7" value="<?php //echo $articlesDetails[0]->getParagraphSeven(); ?>" id="update_paragraph_seven" name="update_paragraph_seven" > -->
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 7" id="update_paragraph_seven" name="update_paragraph_seven" ><?php echo $articlesDetails[0]->getParagraphSeven(); ?></textarea>
            </div>  
 
            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 7 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="update_image_seven" id="update_image_seven" accept="image/*" />        
                <?php $image7 = $articlesDetails[0]->getImageSeven();
                if($image7 !=  '')
                {
                ?>
                    <img src="uploads/<?php echo $articlesDetails[0]->getImageSeven();?>" class="width100 article-photo">
                <?php
                }
                else
                {
                ?>
                    No Image
                <?php
                }
                ?>
            </div> 

            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>  
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgSevenSrc(); ?>" id="update_image_seven_source" name="update_image_seven_source">
            </div>

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 8</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_PARAGRAPH ?> 8" value="<?php //echo $articlesDetails[0]->getParagraphEight(); ?>" id="update_paragraph_eight" name="update_paragraph_eight" > -->
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 8" id="update_paragraph_eight" name="update_paragraph_eight" ><?php echo $articlesDetails[0]->getParagraphEight(); ?></textarea>
            </div>  
 
            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 8 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="update_image_eight" id="update_image_eight" accept="image/*" />        
                <?php $image8 = $articlesDetails[0]->getImageEight();
                if($image8 !=  '')
                {
                ?>
                    <img src="uploads/<?php echo $articlesDetails[0]->getImageEight();?>" class="width100 article-photo">
                <?php
                }
                else
                {
                ?>
                    No Image
                <?php
                }
                ?>
            </div> 

            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>  
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgEightSrc(); ?>" id="update_image_eight_source" name="update_image_eight_source">
            </div>

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 9</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_PARAGRAPH ?> 9" value="<?php //echo $articlesDetails[0]->getParagraphNine(); ?>" id="update_paragraph_nine" name="update_paragraph_nine" > -->
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 9" id="update_paragraph_nine" name="update_paragraph_nine" ><?php echo $articlesDetails[0]->getParagraphNine(); ?></textarea>
            </div>  
 
            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 9 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="update_image_nine" id="update_image_nine" accept="image/*" />        
                <?php $image9 = $articlesDetails[0]->getImageNine();
                if($image9 !=  '')
                {
                ?>
                    <img src="uploads/<?php echo $articlesDetails[0]->getImageNine();?>" class="width100 article-photo">
                <?php
                }
                else
                {
                ?>
                    No Image
                <?php
                }
                ?>
            </div> 

            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>  
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgNineSrc(); ?>" id="update_image_nine_source" name="update_image_nine_source">
            </div>

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 10</p>
                <!-- <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_PARAGRAPH ?> 10" value="<?php //echo $articlesDetails[0]->getParagraphTen(); ?>" id="update_paragraph_ten" name="update_paragraph_ten" > -->
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 10" id="update_paragraph_ten" name="update_paragraph_ten" ><?php echo $articlesDetails[0]->getParagraphTen(); ?></textarea>
            </div>  
 
            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 10 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="update_image_ten" id="update_image_ten" accept="image/*" />        
                <?php $image10 = $articlesDetails[0]->getImageTen();
                if($image10 !=  '')
                {
                ?>
                    <img src="uploads/<?php echo $articlesDetails[0]->getImageTen();?>" class="width100 article-photo">
                <?php
                }
                else
                {
                ?>
                    No Image
                <?php
                }
                ?>
            </div> 

            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>  
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" value="<?php echo $articlesDetails[0]->getImgTenSrc(); ?>" id="update_image_ten_source" name="update_image_ten_source">
            </div>
            

                <input type="hidden" value="<?php echo $articlesDetails[0]->getUid(); ?>" id="article_uid" name="article_uid">

				 <button class="clean-button clean login-btn pink-button" name="submit"><?php echo _EDIT_ARTICLE_SUBMIT ?></button>
            </div>

        </form>

        <?php
        }
        ?>

    </div>
</div>

<?php include 'footer.php'; ?>

</body>
</html>                 