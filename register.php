<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
$conn = connDB();

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="Register | Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">
<link rel="canonical" href="https://tevy.asia/" />
<title>Register | Tevy</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>
    <div class="cover-gap content min-height2 ow-content-gap">
        <div class="big-white-div same-padding">
		<h1 class="landing-h1 margin-left-0"><?php echo _MAINJS_INDEX_REGISTER ?></h1>   
        
        <form action="utilities/newUserRegisterFunction.php" method="POST">
            <div class="login-input-div">
                <p class="input-top-text"><?php echo _MAINJS_INDEX_USERNAME ?></p>
                <input class="aidex-input clean" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" id="register_username" name="register_username" required>
            </div>

            <div class="login-input-div">
            <p class="input-top-text"><?php echo _MAINJS_INDEX_PASSWORD ?></p>
            <input class="aidex-input clean" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>" id="register_password" name="register_password" required>
            </div>

            <div class="login-input-div">
            <p class="input-top-text"><?php echo _MAINJS_INDEX_RETYPE_PASSWORD ?></p>
            <input class="aidex-input clean" type="password" placeholder="<?php echo _MAINJS_INDEX_RETYPE_PASSWORD ?>" id="register_retype_password" name="register_retype_password" required>
            </div>

            <div class="login-input-div">
                <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
                <input class="aidex-input clean" type="<?php echo _JS_EMAIL ?>" placeholder="Type Your Email" id="register_email" name="register_email" required>
            </div>

            <div class="login-input-div">
                <p class="input-top-text"><?php echo _MAINJS_CONTACT_NUMBER ?></p>
                <input class="aidex-input clean" type="text" placeholder="<?php echo _MAINJS_CONTACT_NUMBER ?>" id="register_phone" name="register_phone" required>
            </div>

            <div class="clear"></div>

            <button class="clean-button clean login-btn pink-button" name="register"><?php echo _MAINJS_CREATE_ACCOUNT ?></button>

            </div>
        </form>

    </div>
</div>
<?php include 'footer.php'; ?>

</body>
</html>