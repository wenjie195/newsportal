<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ArticleOne.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$articleRows = getArticlesOne($conn," WHERE author_uid = ? AND display = 'YES' ORDER BY date_created DESC ",array("author_uid"),array($uid),"s");
// $articleDetails = $articleRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="View All Articles | Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">
<link rel="canonical" href="https://tevy.asia/viewArticles.php" />
<title>View All Articles | Tevy</title>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header-after-login.php'; ?>


    <div class="cover-gap content min-height2 ow-content-gap">
        <div class="big-white-div same-padding">
		<h1 class="landing-h1 margin-left-0"><?php echo _VIEW_ARTICLES_ALL ?></h1>             
	<div class="scroll-div">
        <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _VIEW_ARTICLES_DATE ?></th>
                        <th>ID</th>
                        <th><?php echo _VIEW_ARTICLES_AUTHOR ?></th>
                        <th><?php echo _UPLOAD_ARTICLE_TITLE ?></th>
                        <th><?php echo _VIEW_ARTICLES_EDIT ?></th>
                        <th><?php echo _VIEW_ARTICLES_SHOW_HIDE ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = connDB();
                    if($articleRows)
                    {
                        for($cnt = 0;$cnt < count($articleRows) ;$cnt++)
                        {
                        ?>
                        <tr>
                            <td><?php echo $articleRows[$cnt]->getDateCreated();;?></td>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $articleRows[$cnt]->getAuthorName();;?></td>
                            <td><?php echo $articleRows[$cnt]->getTitle();;?></td>                       
                         
                            <td>
                                <form action="editNewsDetails.php" method="POST">
                                <!-- <form action="#" method="POST"> -->
                                    <button class="clean edit-btn" type="submit" name="news_uid" value="<?php echo $articleRows[$cnt]->getUid();?>">
                                    <?php echo _VIEW_ARTICLES_EDIT ?>
                                    </button>
                                </form>
                            </td>
                            <td>
                                <!-- <form action="utilities/hideArticleFunction.php" method="POST">
                                    <input class="aidex-input clean" type="hidden" value="<?php //echo $articleRows[$cnt]->getUid();?>" id="article_uid" name="article_uid">
                                    <button class="clean edit-btn red-background-ow delete-btn"  name="Submit">Delete</button>
                                </form> -->

                                <?php $display = $articleRows[$cnt]->getDisplay();
                                if($display == 'YES')
                                {
                                ?>

                                    <form action="utilities/articleHideFunction.php" method="POST">
                                        <input class="aidex-input clean" type="hidden" value="<?php echo $articleRows[$cnt]->getUid();?>" id="article_uid" name="article_uid">
                                        <div class="clean edit-btn red-background-ow delete-btn open-delete"  name="Submit"><?php echo _VIEW_ARTICLES_HIDE ?></div>
                                        <!-- Use this replace the button on top -->
                                       <!-- <div class="clean edit-btn red-background-ow delete-btn open-delete"  name="Submit"><?php echo _VIEW_ARTICLES_HIDE ?></div>
                                        
                                        <!-- Delete Modal -->
                                    <div id="delete-modal" class="modal-css">
                                        <div class="modal-content-css login-modal-content">
                                            <span class="close-css close-delete">&times;</span>
                                            <div class="clear"></div>
                                       <h1 class="title-h1 darkpink-text login-h1">Delete the Article?</h1>	   
                                       <div class="big-white-div">
                                            <div class="login-div confirm-div">
                                    
                                                <div class="clean-button clean no-btn grey-button close-delete text-center">Cancel</div>
                                                
                                                <button class="clean-button clean yes-btn pink-button" name="Submit">Confirm</button>
                                    
                                              
                                          </div>                 
                                     </div>
                                            
                                                    
                                        </div>
                                    
                                    </div>
                                    </form>

                                <?php
                                }
                                if($display == 'HIDE')
                                {
                                ?>

                                    <form action="utilities/articleShowFunction.php" method="POST">
                                        <input class="aidex-input clean" type="hidden" value="<?php echo $articleRows[$cnt]->getUid();?>" id="article_uid" name="article_uid">
                                        <button class="clean edit-btn red-background-ow delete-btn green-bg-ow"  name="Submit"><?php echo _VIEW_ARTICLES_SHOW ?></button>
                                    </form>

                                <?php
                                }
                                ?>
                            </td>   

                            <!-- <td>
                                <div class="clean edit-btn red-background-ow delete-btn open-delete"  >
                                    Delete
                                </div>             
                            </td>     -->

                                <!-- <form> 
                                    <button class="clean edit-btn red-background-ow delete-btn"  >
                                        Hide
                                    </button>
                                </form>  
                                    Hide it or Show it
                                <form> 
                                    <button class="clean edit-btn red-background-ow delete-btn green-bg-ow"  >
                                        Show
                                    </button>
                                </form> -->

                        </tr>
                        <?php
                        }
                        ?>
                    <?php
                    }
                    $conn->close();
                    ?>
                </tbody>
            </table>
		</div>


    </div>
</div>

<?php include 'footer.php'; ?>

</body>
</html>                 