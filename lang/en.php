<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "Attention");
define("_MAINJS_ENTER_USERNAME", "Please enter your username");
define("_MAINJS_ENTER_EMAIL", "Please enter your email address");
define("_MAINJS_ENTER_ICNO", "Please enter your ID number");
define("_MAINJS_SELECT_COUNTRY", "Please choose your country");
define("_MAINJS_ENTER_PHONENO", "Please enter your phone number");
//apply in all
define("_MAINJS_ALL_LOGOUT", "Logout");
//index
define("_MAINJS_INDEX_LOGIN", "Login");
define("_MAINJS_INDEX_USERNAME", "Username");
define("_MAINJS_INDEX_PASSWORD", "Password");
define("_MAINJS_INDEX_LATEST_ART", "Latest Articles");
define("_MAINJS_INDEX_RETYPE_PASSWORD", "Retype Password");
define("_MAINJS_CONTACT_NUMBER", "Contact Number");
define("_MAINJS_CREATE_ACCOUNT", "Create Account");
define("_MAINJS_INDEX_REGISTER", "Register");
//header
define("_HEADER_LANGUAGE", "Language/语言");
define("_HEADER_LOGOUT", "Logout");
define("_HEADER_PROFILE", "Profile");
define("_HEADER_EDIT_PROFILE", "Edit Profile");
define("_HEADER_CHANGE_EMAIL", "Change Email");
define("_HEADER_CHANGE_PHONE_NO", "Change Phone No.");
define("_HEADER_CHANGE_PASSWORD", "Change Password");
define("_HEADER_SIGN_UP", "Sign Up");
define("_HEADER_LOGIN", "Login");
define("_HEADER_MESSAGE", "Message");
define("_HEADER_HOME", "Home");
define("_HEADER_BEAUTY", "Beauty");
define("_HEADER_FASHION", "Fashion");
define("_HEADER_SOCIAL", "Social");
define("_HEADER_ARTICLE", "Article");
define("_HEADER_UPLOAD_ARTICLE", "Upload Article");
define("_HEADER_EDIT_ARTICLE", "Edit Article");
//JS
define("_JS_LOGIN", "Login");
define("_JS_USERNAME", "Username");
define("_JS_PASSWORD", "Password");
define("_JS_FULLNAME", "Fullname");
define("_JS_NEW_PASSWORD", "New Password");
define("_JS_CURRENT_PASSWORD", "Current Password");
define("_JS_RETYPE_PASSWORD", "Retype Password");
define("_JS_REMEMBER_ME", "Remember Me");
define("_JS_FORGOT_PASSWORD", "Forgot Password?");
define("_JS_FORGOT_TITLE", "Forgot Password");
define("_JS_EMAIL", "Email");
define("_JS_SIGNUP", "Sign Up");
define("_JS_FIRSTNAME", "First Name");
define("_JS_LASTNAME", "Last Name");
define("_JS_GENDER", "Gender");
define("_JS_MALE", "Male");
define("_JS_FEMALE", "Female");
define("_JS_BIRTHDAY", "Birthday");
define("_JS_COUNTRY", "Country");
define("_JS_CLOSE", "Close");
define("_JS_ERROR", "Error");
//VIEW MESSAGE
define("_VIEWMESSAGE_VIEW_ALL_MESSAGE", "View All Message");
define("_VIEWMESSAGE_NO", "NO.");
define("_VIEWMESSAGE_SENT", "SENT");
define("_VIEWMESSAGE_REPLY", "REPLY");
define("_VIEWMESSAGE_DATE", "DATE");
define("_VIEWMESSAGE_MESSAGE_STATUS", "MESSAGE STATUS");
define("_VIEWMESSAGE_READ", "READ");
define("_VIEWMESSAGE_NEW_MESSAGE", "New Message");
define("_VIEWMESSAGE_CHOOSE_YOUR_FILE", "Choose Your File");
//Profile
define("_PROFILE_PERSONAL_DETAILS", "Personal Details");
define("_PROFILE_CHOOSE_COUNTRY", "Choose Country");
//Top Up History
define("_TOPUP_HISTORY_DATE", "Date");
//ViewMessage
define("_VIEWMESSAGE_SENT2", "Send");
define("_VIEWMESSAGE_UR_MESSAGE", "Your Message Here");
define("_VIEWMESSAGE_UPLOAD", "Upload Image");
define("_VIEWMESSAGE_JUST_UPLOAD", "Upload");
define("_VIEWMESSAGE_SENT3", "Send");
//Article
define("_ARTICLE_SHARE", "Share");
define("_ARTICLE_RECOMMENDED", "Recommended");
define("_ARTICLE_SOURCE", "Source");
//Upload Article
define("_UPLOAD_ARTICLE_NEW", "New Article");
define("_UPLOAD_ARTICLE_TITLE", "Title");
define("_UPLOAD_ARTICLE_LINK", "Article Link (English only, no spacing or any symbol like coma, can use - to separate the word, cannot repeated with other article link)");
define("_UPLOAD_ARTICLE_EXAMPLE", "Example");
define("_UPLOAD_ARTICLE_GOOGLE_KEYWORD", "Google Search Keyword (Use coma to separate the keyword)");
define("_UPLOAD_ARTICLE_CATEGORY", "Category");
define("_UPLOAD_ARTICLE_CHOOSE_A_CATEGORY", "Choose a Category");
define("_UPLOAD_ARTICLE_COVER_PHOTO", "Cover Photo");
define("_UPLOAD_ARTICLE_PARAGRAPH", "Paragraph");
define("_UPLOAD_ARTICLE_IMAGE", "Image");
define("_UPLOAD_ARTICLE_SUBMIT", "Submit");
define("_UPLOAD_ARTICLE_AUTHOR", "Author");
define("_UPLOAD_ARTICLE_ADD_MORE_PARAGRAPH", "Add More Paragraph");
define("_UPLOAD_ARTICLE_COVER_PHOTO_SOURCE", "Source of Photo");
define("_UPLOAD_ARTICLE_VIDEO_SOURCE", "Source of Video");
define("_UPLOAD_ARTICLE_FILE_SOURCE", "Source of File");
define("_UPLOAD_ARTICLE_FILE_TYPE", "File Type (Photo/Video)");
define("_UPLOAD_ARTICLE_PHOTO", "Photo");
define("_UPLOAD_ARTICLE_VIDEO", "Video");
define("_UPLOAD_ARTICLE_FILE", "File");
define("_UPLOAD_ARTICLE_SUBHEADER", "Subheader");
//View Article
define("_VIEW_ARTICLES_ALL", "All Articles");
define("_VIEW_ARTICLES_DATE", "Date");
define("_VIEW_ARTICLES_AUTHOR", "Author");
define("_VIEW_ARTICLES_EDIT", "Edit");
define("_VIEW_ARTICLES_DELETE", "Delete");
define("_VIEW_ARTICLES_SHOW_HIDE", "Delete");
define("_VIEW_ARTICLES_SHOW", "Undo");
define("_VIEW_ARTICLES_HIDE", "Delete");
//Edit Article
define("_EDIT_ARTICLE", "Edit Article");
define("_EDIT_ARTICLE_SUBMIT", "Submit");
define("_EDIT_ARTICLE_CHOOSE_DELETE", "Select Delete to Delete the Paragraph");
define("_EDIT_ARTICLE_DELETE", "Delete");
define("_EDIT_ARTICLE_DELETE_PARAGRAPH", "Delete This Paragraph?");
define("_EDIT_ARTICLE_NO", "No");
define("_EDIT_ARTICLE_YES", "Yes (Delete)");
define("_EDIT_ARTICLE_DELETE_PHOTO", "Delete This Photo?");
define("_EDIT_ARTICLE_DELETE_PHOTO_SOURCE", "Delete This Source of Photo?");
//Footer
define("_FOOTER_ALL_RIGHT", "All Right Reserved");
define("_FOOTER_CREATE_ACCOUNT", "Create Account");