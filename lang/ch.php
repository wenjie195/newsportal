<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "注意");
define("_MAINJS_ENTER_USERNAME", "请输入您的用户名");
define("_MAINJS_ENTER_EMAIL", "请输入您的电子邮件地址");
define("_MAINJS_ENTER_ICNO", "请输入您的身份证号码");
define("_MAINJS_SELECT_COUNTRY", "请选择你的国家");
define("_MAINJS_ENTER_PHONENO", "请输入您的电话号码");
//apply in all
define("_MAINJS_ALL_LOGOUT", "登出");
//index
define("_MAINJS_INDEX_LOGIN", "登录");
define("_MAINJS_INDEX_USERNAME", "用户名");
define("_MAINJS_INDEX_PASSWORD", "密码");
define("_MAINJS_INDEX_LATEST_ART", "最新资讯");
define("_MAINJS_INDEX_RETYPE_PASSWORD", "再次输入密码");
define("_MAINJS_CONTACT_NUMBER", "联络号码");
define("_MAINJS_CREATE_ACCOUNT", "创建账号");
define("_MAINJS_INDEX_REGISTER", "注册");
//header
define("_HEADER_LANGUAGE", "Language/语言");
define("_HEADER_LOGOUT", "登出");
define("_HEADER_PROFILE", "个人中心");
define("_HEADER_EDIT_PROFILE", "修改个人资料");
define("_HEADER_CHANGE_EMAIL", "更改电邮地址");
define("_HEADER_CHANGE_PHONE_NO", "更改手机号码");
define("_HEADER_CHANGE_PASSWORD", "更改密码");
define("_HEADER_SIGN_UP", "注册");
define("_HEADER_LOGIN", "登录");
define("_HEADER_MESSAGE", "信息");
define("_HEADER_HOME", "主页");
define("_HEADER_BEAUTY", "美容");
define("_HEADER_FASHION", "时尚");
define("_HEADER_SOCIAL", "社交");
define("_HEADER_ARTICLE", "文章");
define("_HEADER_UPLOAD_ARTICLE", "上载文章");
define("_HEADER_EDIT_ARTICLE", "修改文章");
//JS
define("_JS_LOGIN", "登入");
define("_JS_USERNAME", "用户名");
define("_JS_PASSWORD", "密码");
define("_JS_FULLNAME", "全名");
define("_JS_NEW_PASSWORD", "新密码");
define("_JS_CURRENT_PASSWORD", "现在的密码");
define("_JS_RETYPE_PASSWORD", "再次输入密码");
define("_JS_REMEMBER_ME", "记住我");
define("_JS_FORGOT_PASSWORD", "忘记密码");
define("_JS_FORGOT_TITLE", "忘记密码");
define("_JS_EMAIL", "邮箱地址");
define("_JS_SIGNUP", "注册");
define("_JS_FIRSTNAME", "名字");
define("_JS_LASTNAME", "姓氏");
define("_JS_GENDER", "性别");
define("_JS_MALE", "男");
define("_JS_FEMALE", "女");
define("_JS_BIRTHDAY", "出生日期");
define("_JS_COUNTRY", "国家");
define("_JS_CLOSE", "关闭");
define("_JS_ERROR", "错误");
//VIEW MESSAGE
define("_VIEWMESSAGE_VIEW_ALL_MESSAGE", "浏览所有信息");
define("_VIEWMESSAGE_NO", "序");
define("_VIEWMESSAGE_SENT", "已发出");
define("_VIEWMESSAGE_REPLY", "回复");
define("_VIEWMESSAGE_DATE", "日期");
define("_VIEWMESSAGE_MESSAGE_STATUS", "状态");
define("_VIEWMESSAGE_READ", "已读");
define("_VIEWMESSAGE_NEW_MESSAGE", "新信息");
define("_VIEWMESSAGE_CHOOSE_YOUR_FILE", "选文件上传");
//Profile
define("_PROFILE_PERSONAL_DETAILS", "个人资料");
define("_PROFILE_CHOOSE_COUNTRY", "选择国家");
//Top Up History
define("_TOPUP_HISTORY_DATE", "日期");
//ViewMessage
define("_VIEWMESSAGE_SENT2", "发出");
define("_VIEWMESSAGE_UR_MESSAGE", "输入你的信息");
define("_VIEWMESSAGE_UPLOAD", "上载照片");
define("_VIEWMESSAGE_JUST_UPLOAD", "上载");
define("_VIEWMESSAGE_SENT3", "上载");
//Article
define("_ARTICLE_SHARE", "分享");
define("_ARTICLE_RECOMMENDED", "你可能喜欢");
define("_ARTICLE_SOURCE", "出处");
//Upload Article
define("_UPLOAD_ARTICLE_NEW", "新文章");
define("_UPLOAD_ARTICLE_TITLE", "标题");
define("_UPLOAD_ARTICLE_LINK", "文章链接 （只能使用英文字母,不可以有空格和任何符号包括逗号,可用-代替空格,不可与其他文章重复链接）");
define("_UPLOAD_ARTICLE_EXAMPLE", "例如");
define("_UPLOAD_ARTICLE_GOOGLE_KEYWORD", "谷歌搜索关键词 (请在每个关键词后面加上英文输入法逗号,)");
define("_UPLOAD_ARTICLE_CATEGORY", "分类");
define("_UPLOAD_ARTICLE_CHOOSE_A_CATEGORY", "选择类别");
define("_UPLOAD_ARTICLE_COVER_PHOTO", "封面图");
define("_UPLOAD_ARTICLE_PARAGRAPH", "文章段落");
define("_UPLOAD_ARTICLE_IMAGE", "照片");
define("_UPLOAD_ARTICLE_SUBMIT", "上载");
define("_UPLOAD_ARTICLE_AUTHOR", "作者");
define("_UPLOAD_ARTICLE_ADD_MORE_PARAGRAPH", "增加段落");
define("_UPLOAD_ARTICLE_COVER_PHOTO_SOURCE", "照片出处");
define("_UPLOAD_ARTICLE_VIDEO_SOURCE", "影片出处");
define("_UPLOAD_ARTICLE_FILE_SOURCE", "文件出处");
define("_UPLOAD_ARTICLE_FILE_TYPE", "文件格式 (照片/影片)");
define("_UPLOAD_ARTICLE_PHOTO", "照片");
define("_UPLOAD_ARTICLE_VIDEO", "影片");
define("_UPLOAD_ARTICLE_FILE", "文件");
define("_UPLOAD_ARTICLE_SUBHEADER", "副标题");
//View Article
define("_VIEW_ARTICLES_ALL", "全部文章");
define("_VIEW_ARTICLES_DATE", "日期");
define("_VIEW_ARTICLES_AUTHOR", "作者");
define("_VIEW_ARTICLES_EDIT", "修改");
define("_VIEW_ARTICLES_DELETE", "删除");
define("_VIEW_ARTICLES_SHOW_HIDE", "删除");
define("_VIEW_ARTICLES_SHOW", "撤回删除");
define("_VIEW_ARTICLES_HIDE", "删除");
//Edit Article
define("_EDIT_ARTICLE", "修改文章");
define("_EDIT_ARTICLE_SUBMIT", "提交");
define("_EDIT_ARTICLE_CHOOSE_DELETE", "选择删除将会删除/隐藏此段落");
define("_EDIT_ARTICLE_DELETE", "删除");
define("_EDIT_ARTICLE_DELETE_PARAGRAPH", "删除此段落？");
define("_EDIT_ARTICLE_NO", "否");
define("_EDIT_ARTICLE_YES", "是（删除）");
define("_EDIT_ARTICLE_DELETE_PHOTO", "删除此照片？");
define("_EDIT_ARTICLE_DELETE_PHOTO_SOURCE", "删除此照片出处？");
//Footer
define("_FOOTER_ALL_RIGHT", "版权所有");
define("_FOOTER_CREATE_ACCOUNT", "创建账号");