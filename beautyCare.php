<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ArticleOne.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $articlesBeauty = getArticlesOne($conn, " WHERE type = 'Beauty' ");
$articlesBeauty = getArticlesOne($conn, " WHERE type = 'Beauty' AND display = 'YES' ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="Beauty | Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">
<link rel="canonical" href="https://tevy.asia/beautyCare.php" />
<title>Beauty | News</title>
<?php include 'css.php'; ?>



</head>
<body>
<?php include 'header-after-login.php'; ?>

<div class="background-div">
    <div class="cover-gap content min-height">
        <div class="test ">

            <h1 class="landing-h1"><?php echo _HEADER_BEAUTY ?></h1>	   

            <div class="big-white-div overflow">

            <?php
            $conn = connDB();
            if($articlesBeauty)
            {
                for($cnt = 0;$cnt < count($articlesBeauty) ;$cnt++)
                {
                ?>


                    <!-- <a href='article001.php?id=<?php //echo $articlesBeauty[$cnt]->getSeoTitle();?>'> -->
                    <!-- <a href='article.php?id=<?php //echo $articlesBeauty[$cnt]->getSeoTitle();?>'> -->
                    <a href='article.php?id=<?php echo $articlesBeauty[$cnt]->getArticleLink();?>'>

                        <div class="article-card article-card-overwrite">

                            <div class="article-bg-img-box">
                                <img src="uploads/<?php echo $articlesBeauty[$cnt]->getTitleCover();?>" class="article-img1" alt="<?php echo $articlesBeauty[$cnt]->getTitle();?>" title="<?php echo $articlesBeauty[$cnt]->getTitle();?>">
                            </div>

                            <div class="box-caption box2">

                                <div class="wrap-a wrap100">
                                    <!-- <a href="beautyCare.php" class="peach-hover cate-a transition"> -->
                                    <a href='article.php?id=<?php echo $articlesBeauty[$cnt]->getArticleLink();?>' class="peach-hover cate-a transition">
                                        <?php echo _HEADER_BEAUTY ?> <span class="grey-text">• <?php echo $articlesBeauty[$cnt]->getDateCreated();?></span>
                                    </a>
                                </div>
								<a href='article.php?id=<?php echo $articlesBeauty[$cnt]->getArticleLink();?>'>
                                    <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a">
                                        <?php echo $articlesBeauty[$cnt]->getTitle();?>
                                    </div>
    
                                    <div class="text-content-div">
                                        <?php echo $articlesBeauty[$cnt]->getParagraphOne();?>
                                    </div>
								</a>
                            </div>
                            
                        </div>
                    </a>
                    
                <?php
                }
                ?>
            <?php
            }
            $conn->close();
            ?>

            </div>



        </div>

    <!--tab 2 -->
    </div>

    <div class="clear"></div>

</div>

<?php include 'footer.php'; ?>

</body>
</html>