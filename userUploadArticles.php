<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid  = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="Upload Article | Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">

<link rel="canonical" href="https://tevy.asia/userUploadArticles.php" />
<title>Upload Article | Tevy</title>
<?php include 'css.php'; ?>
</head>

<body>
<?php include 'header-after-login.php'; ?>

<div class="background-div">
    <div class="cover-gap content min-height2">
        <div class="big-white-div same-padding">
             

        <form action="utilities/addNewArticlesFunction.php" method="POST" enctype="multipart/form-data">
        	<h1 class="landing-h1 margin-left-0"><?php echo _UPLOAD_ARTICLE_NEW ?></h1>    

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_TITLE ?></p>
                <textarea class="one-line-textarea aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_TITLE ?>" id="title" name="title" required></textarea>
            </div>  

            <div class="input-div">
                <!-- <p class="input-top-text">Article Link</p> -->
                <p class="input-top-text darkpink-text"><?php echo _UPLOAD_ARTICLE_LINK ?></p>
               <textarea class="one-line-textarea aidex-input clean" type="text" placeholder="how-to-stay-healthy" id="article_link" name="article_link" required></textarea>
            </div>   

            <!-- <div class="input-div">
                <p class="input-top-text"><?php //echo _UPLOAD_ARTICLE_LINK ?></p>
                <input class="aidex-input clean" type="text" placeholder="<?php //echo _UPLOAD_ARTICLE_EXAMPLE ?>: beautiful-lady" id="keyword_one" name="keyword_one" required>
            </div>   -->

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_GOOGLE_KEYWORD ?></p>
                <textarea class="three-line-textarea aidex-input clean" type="text" placeholder="beautiful, skin care, health," id="keyword_two" name="keyword_two" required></textarea>
            </div>  

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_AUTHOR ?></p>
                <input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_AUTHOR ?>" id="author_name" name="author_name" required>
            </div>  

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_CATEGORY ?></p>
                <select class="clean aidex-input" type="text" id="type" name="type" required>
                    <option value="" name=" "><?php echo _UPLOAD_ARTICLE_CHOOSE_A_CATEGORY ?></option>
                    <option value="Beauty" name="Beauty"><?php echo _HEADER_BEAUTY ?></option>
                    <option value="Fashion" name="Fashion"><?php echo _HEADER_FASHION ?></option>
                    <option value="Social" name="Social"><?php echo _HEADER_SOCIAL ?></option>
                </select>                
            </div> 

            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO ?></p>
                <input id="file-upload" type="file" name="cover_photo" id="cover_photo" accept="image/*" required>    
            </div>   
            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>        
             	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" id="cover_photo_source" name="cover_photo_source" > 
			</div>
            
            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 1</p>
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 1" id="paragraph_one" name="paragraph_one" required></textarea>
            </div>  

            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 1 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" />       
            </div> 
            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p>  
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" id="image_one_source" name="image_one_source">
            </div>


            <div class="input-div">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_SUBHEADER ?> 2</p> 
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_SUBHEADER ?> 2" id="header_two" name="header_two"> 
            </div>               
            <div class="para-photo delete-para-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 2</p>
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 2" id="paragraph_two" name="paragraph_two"></textarea>
            </div>  
            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 2 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="image_two" id="image_two" accept="image/*" />                    
            </div> 
            <div class="para-photo delete-para-div">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p> 
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" id="image_two_source" name="image_two_source"> 
            </div>

            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 2 <?php echo _UPLOAD_ARTICLE_VIDEO ?></p>
                <input id="file-upload" type="file" name="video_two" id="video_two"/>    
            </div> 

            <div class="para-photo delete-para-div">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_VIDEO_SOURCE ?></p> 
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_VIDEO_SOURCE ?>" id="vd_src_two" name="vd_src_two"> 
            </div>   
            
            
            <div class="input-div">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_SUBHEADER ?> 3</p> 
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_SUBHEADER ?> 3" id="header_three" name="header_three"> 
            </div>                
            <div class="para-photo delete-para-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 3</p>
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 3" id="paragraph_three" name="paragraph_three"></textarea>
            </div>  
            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 3 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="image_three" id="image_three" accept="image/*" />             
            </div> 
            <div class="para-photo delete-para-div">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p> 
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" id="image_three_source" name="image_three_source"> 
            </div>            

            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 3 <?php echo _UPLOAD_ARTICLE_VIDEO ?></p>
                <input id="file-upload" type="file" name="video_three" id="video_three"/>    
            </div> 

            <div class="para-photo delete-para-div">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_VIDEO_SOURCE ?></p> 
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_VIDEO_SOURCE ?>" id="vd_src_three" name="vd_src_three"> 
            </div>   
            
            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 4</p>
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 4" id="paragraph_four" name="paragraph_four"></textarea>
            </div>  
            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 4 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="image_four" id="image_four" accept="image/*" />                
            </div> 
            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p> 
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" id="image_four_source" name="image_four_source"> 
            </div> 
            
            <div class="input-div">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 5</p>
                <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 5" id="paragraph_five" name="paragraph_five"></textarea>
            </div>  
            <div class="para-photo">
                <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 5 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                <input id="file-upload" type="file" name="image_five" id="image_five" accept="image/*" />                    
            </div> 
            <div class="para-photo">
            	<p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p> 
            	<input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>" id="image_five_source" name="image_five_source"> 
            </div> 
            
			<div onclick="myFunction()" id="addButton" class="add-button"><?php echo _UPLOAD_ARTICLE_ADD_MORE_PARAGRAPH ?></div>
            <div id="moreParagraph" class="more-paragraph">
    
                <div class="input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 6</p>
                    <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 6" id="paragraph_six" name="paragraph_six"></textarea>
                </div>  
                <div class="para-photo">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 6 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                    <input id="file-upload" type="file" name="image_six" id="image_six" accept="image/*" />             
                </div>
                <div class="para-photo">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p> 
                    <input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>"  id="image_six_source" name="image_six_source"> 
                </div> 

                <div class="input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 7</p>
                    <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 7" id="paragraph_seven" name="paragraph_seven"></textarea>
                </div>  
                <div class="para-photo">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 7 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                    <input id="file-upload" type="file" name="image_seven" id="image_seven" accept="image/*" />            
                </div>
                <div class="para-photo">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p> 
                    <input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>"  id="image_seven_source" name="image_seven_source"> 
                </div>

                <div class="input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 8</p>
                    <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 8" id="paragraph_eight" name="paragraph_eight"></textarea>
                </div>  
                <div class="para-photo">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 8 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                    <input id="file-upload" type="file" name="image_eight" id="image_eight" accept="image/*" /> 
                </div>
                <div class="para-photo">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p> 
                    <input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>"  id="image_eight_source" name="image_eight_source"> 
                </div>

                <div class="input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 9</p>
                    <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 9" id="paragraph_nine" name="paragraph_nine"></textarea>
                </div>  
                <div class="para-photo">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 9 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                    <input id="file-upload" type="file" name="image_nine" id="image_nine" accept="image/*" />                  
                </div>
                <div class="para-photo">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p> 
                    <input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>"  id="image_nine_source" name="image_nine_source"> 
                </div>

                <div class="input-div">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 10</p>
                    <textarea class="aidex-input clean tevy-textarea" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 10" id="paragraph_ten" name="paragraph_ten"></textarea>
                </div>  
                <div class="para-photo">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_PARAGRAPH ?> 10 <?php echo _UPLOAD_ARTICLE_IMAGE ?></p>
                    <input id="file-upload" type="file" name="image_ten" id="image_ten" accept="image/*" />           
                </div>
                <div class="para-photo">
                    <p class="input-top-text"><?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?></p> 
                    <input class="aidex-input clean" type="text" placeholder="<?php echo _UPLOAD_ARTICLE_COVER_PHOTO_SOURCE ?>"  id="image_ten_source" name="image_ten_source"> 
                </div>

            </div>


            <button class="clean-button clean login-btn pink-button" name="submit"><?php echo _UPLOAD_ARTICLE_SUBMIT ?></button>
           

        </form>

    </div>
</div>
<?php include 'footer.php'; ?>

</body>
</html>                 