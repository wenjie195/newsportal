-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2020 at 05:17 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_news`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles_one`
--

CREATE TABLE `articles_one` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `title_cover` varchar(255) DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `paragraph_four` text DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `paragraph_five` text DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `paragraph_six` varchar(255) DEFAULT NULL,
  `paragraph_seven` varchar(255) DEFAULT NULL,
  `paragraph_eight` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles_one`
--

INSERT INTO `articles_one` (`id`, `uid`, `author_uid`, `author_name`, `title`, `seo_title`, `keyword_one`, `keyword_two`, `title_cover`, `paragraph_one`, `image_one`, `paragraph_two`, `image_two`, `paragraph_three`, `image_three`, `paragraph_four`, `image_four`, `paragraph_five`, `image_five`, `paragraph_six`, `paragraph_seven`, `paragraph_eight`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, 'd4e888283a7932f57159465ece977006', '1ae56476f927e4c262036260d408c522', 'user1', 'Test Articles 1', 'TestArticles1-Test 1', 'Test 1', 'Article 1', 'image-02.jpg', 'Lionel Andrés Messi Cuccittini[note 1] (Spanish pronunciation: [ljoˈnel anˈdɾez ˈmesi] (About this soundlisten);[A] born 24 June 1987) is an Argentine professional footballer who plays as a forward and captains both Spanish club Barcelona and the Argentina national team. Often considered the best player in the world and widely regarded as one of the greatest players of all time, Messi has won a record six Ballon d&#039;Or awards,[note 2] and a record six European Golden Shoes. He has spent his entire professional career with Barcelona, where he has won a club-record 34 trophies, including ten La Liga titles, four UEFA Champions League titles and six Copas del Rey. A prolific goalscorer and a creative playmaker, Messi holds the records for most goals in La Liga (438), a La Liga and European league season (50), most hat-tricks in La Liga (36) and the UEFA Champions League (8), and most assists in La Liga (181) and the Copa América (12). He has scored over 700 senior career goals for club and country.', 'image-02.jpg', 'Lionel Andrés Messi Cuccittini[note 1] (Spanish pronunciation: [ljoˈnel anˈdɾez ˈmesi] (About this soundlisten);[A] born 24 June 1987) is an Argentine professional footballer who plays as a forward and captains both Spanish club Barcelona and the Argentina national team. Often considered the best player in the world and widely regarded as one of the greatest players of all time, Messi has won a record six Ballon d&#039;Or awards,[note 2] and a record six European Golden Shoes. He has spent his entire professional career with Barcelona, where he has won a club-record 34 trophies, including ten La Liga titles, four UEFA Champions League titles and six Copas del Rey. A prolific goalscorer and a creative playmaker, Messi holds the records for most goals in La Liga (438), a La Liga and European league season (50), most hat-tricks in La Liga (36) and the UEFA Champions League (8), and most assists in La Liga (181) and the Copa América (12). He has scored over 700 senior career goals for club and country.  Born and raised in central Argentina, Messi relocated to Spain to join Barcelona at age 13, for whom he made his competitive debut aged 17 in October 2004. He established himself as an integral player for the club within the next three years, and in his first uninterrupted season in 2008–09 he helped Barcelona achieve the first treble in Spanish football; that year, aged 22, Messi won his first Ballon d&#039;Or. Three successful seasons followed, with Messi winning three consecutive Ballons d&#039;Or, including an unprecedented fourth. During the 2011–12 season, he set the La Liga and European records for most goals scored in a single season, while establishing himself as Barcelona&#039;s all-time top scorer. The following two seasons, Messi finished second for the Ballon d&#039;Or behind Cristiano Ronaldo (his perceived career rival), before regaining his best form during the 2014–15 campaign, becoming the all-time top scorer in La Liga and leading Barcelona to a historic second treble, after which he was awarded a fifth Ballon d&#039;Or in 2015. Messi assumed the captaincy of Barcelona in 2018, and in 2019 he secured a record sixth Ballon d&#039;Or.', 'image-03.jpg', 'Lionel Andrés Messi Cuccittini[note 1] (Spanish pronunciation: [ljoˈnel anˈdɾez ˈmesi] (About this soundlisten);[A] born 24 June 1987) is an Argentine professional footballer who plays as a forward and captains both Spanish club Barcelona and the Argentina national team. Often considered the best player in the world and widely regarded as one of the greatest players of all time, Messi has won a record six Ballon d&#039;Or awards,[note 2] and a record six European Golden Shoes. He has spent his entire professional career with Barcelona, where he has won a club-record 34 trophies, including ten La Liga titles, four UEFA Champions League titles and six Copas del Rey. A prolific goalscorer and a creative playmaker, Messi holds the records for most goals in La Liga (438), a La Liga and European league season (50), most hat-tricks in La Liga (36) and the UEFA Champions League (8), and most assists in La Liga (181) and the Copa América (12). He has scored over 700 senior career goals for club and country.  Born and raised in central Argentina, Messi relocated to Spain to join Barcelona at age 13, for whom he made his competitive debut aged 17 in October 2004. He established himself as an integral player for the club within the next three years, and in his first uninterrupted season in 2008–09 he helped Barcelona achieve the first treble in Spanish football; that year, aged 22, Messi won his first Ballon d&#039;Or. Three successful seasons followed, with Messi winning three consecutive Ballons d&#039;Or, including an unprecedented fourth. During the 2011–12 season, he set the La Liga and European records for most goals scored in a single season, while establishing himself as Barcelona&#039;s all-time top scorer. The following two seasons, Messi finished second for the Ballon d&#039;Or behind Cristiano Ronaldo (his perceived career rival), before regaining his best form during the 2014–15 campaign, becoming the all-time top scorer in La Liga and leading Barcelona to a historic second treble, after which he was awarded a fifth Ballon d&#039;Or in 2015. Messi assumed the captaincy of Barcelona in 2018, and in 2019 he secured a record sixth Ballon d&#039;Or.  An Argentine international, Messi is his country&#039;s all-time leading goalscorer. At youth level, he won the 2005 FIFA World Youth Championship, finishing the tournament with both the Golden Ball and Golden Shoe, and an Olympic gold medal at the 2008 Summer Olympics. His style of play as a diminutive, left-footed dribbler drew comparisons with his compatriot Diego Maradona, who described Messi as his successor. After his senior debut in August 2005, Messi became the youngest Argentine to play and score in a FIFA World Cup during the 2006 edition, and reached the final of the 2007 Copa América, where he was named young player of the tournament. As the squad&#039;s captain from August 2011, he led Argentina to three consecutive finals: the 2014 FIFA World Cup, for which he won the Golden Ball, and the 2015 and 2016 Copas América. After announcing his international retirement in 2016, he reversed his decision and led his country to qualification for the 2018 FIFA World Cup, and a third-place finish at the 2019 Copa América.', 'image-04.png', 'Born and raised in central Argentina, Messi relocated to Spain to join Barcelona at age 13, for whom he made his competitive debut aged 17 in October 2004. He established himself as an integral player for the club within the next three years, and in his first uninterrupted season in 2008–09 he helped Barcelona achieve the first treble in Spanish football; that year, aged 22, Messi won his first Ballon d&#039;Or. Three successful seasons followed, with Messi winning three consecutive Ballons d&#039;Or, including an unprecedented fourth. During the 2011–12 season, he set the La Liga and European records for most goals scored in a single season, while establishing himself as Barcelona&#039;s all-time top scorer. The following two seasons, Messi finished second for the Ballon d&#039;Or behind Cristiano Ronaldo (his perceived career rival), before regaining his best form during the 2014–15 campaign, becoming the all-time top scorer in La Liga and leading Barcelona to a historic second treble, after which he was awarded a fifth Ballon d&#039;Or in 2015. Messi assumed the captaincy of Barcelona in 2018, and in 2019 he secured a record sixth Ballon d&#039;Or.', 'image-05.jpg', 'An Argentine international, Messi is his country&#039;s all-time leading goalscorer. At youth level, he won the 2005 FIFA World Youth Championship, finishing the tournament with both the Golden Ball and Golden Shoe, and an Olympic gold medal at the 2008 Summer Olympics. His style of play as a diminutive, left-footed dribbler drew comparisons with his compatriot Diego Maradona, who described Messi as his successor. After his senior debut in August 2005, Messi became the youngest Argentine to play and score in a FIFA World Cup during the 2006 edition, and reached the final of the 2007 Copa América, where he was named young player of the tournament. As the squad&#039;s captain from August 2011, he led Argentina to three consecutive finals: the 2014 FIFA World Cup, for which he won the Golden Ball, and the 2015 and 2016 Copas América. After announcing his international retirement in 2016, he reversed his decision and led his country to qualification for the 2018 FIFA World Cup, and a third-place finish at the 2019 Copa América.  One of the most famous athletes in the world, Messi has been sponsored by sportswear company Adidas since 2006 and has established himself as their leading brand endorser. According to France Football, he was the world&#039;s highest-paid footballer for five years out of six between 2009 and 2014, and was ranked the world&#039;s highest-paid athlete by Forbes in 2019. Messi was among Time&#039;s 100 most influential people in the world in 2011 and 2012. In 2020, he was awarded the Laureus World Sportsman of the Year.', 'image-06.jpg', NULL, NULL, NULL, 'Beauty', 'YES', '2020-03-31 09:25:50', '2020-04-01 02:46:02'),
(2, 'c4d3bcf74c9e86b4eadeb56b58f3de61', '1ae56476f927e4c262036260d408c522', 'user1', 'Test Article 2', 'TestArticle2-Test 2', 'Test 2', 'Article 2', NULL, '利昂内尔·安德烈斯·“莱奥”·梅西·庫西提尼（西班牙語：Lionel Andrés &quot;Leo&quot; Messi Cuccittini，西班牙語發音：[ljoˈnel anˈdɾes ˈmesi]  聆聽，1987年6月24日－），简称梅西，生于阿根廷圣菲省罗萨里奥，擁有加泰罗尼亚及意大利血統[2]，現時為西班牙甲組足球聯賽豪門巴塞罗那的隊長，場上主要擔任翼鋒，也可以擔任前鋒並回撤為進攻中場，目前他共獲得10次聯賽冠軍、4次歐冠冠軍、6座歐洲金靴獎、6次歐聯神射手、6座金球獎及6次FIFA國際世界足球先生。', '3.png', '利昂内尔·安德烈斯·“莱奥”·梅西·庫西提尼（西班牙語：Lionel Andrés &quot;Leo&quot; Messi Cuccittini，西班牙語發音：[ljoˈnel anˈdɾes ˈmesi]  聆聽，1987年6月24日－），简称梅西，生于阿根廷圣菲省罗萨里奥，擁有加泰罗尼亚及意大利血統[2]，現時為西班牙甲組足球聯賽豪門巴塞罗那的隊長，場上主要擔任翼鋒，也可以擔任前鋒並回撤為進攻中場，目前他共獲得10次聯賽冠軍、4次歐冠冠軍、6座歐洲金靴獎、6次歐聯神射手、6座金球獎及6次FIFA國際世界足球先生。  美斯年少時已加入巴塞隆拿青訓營拉瑪西亞，出道至今職業生涯一直效力巴塞罗那，並隨隊橫掃多項賽事冠軍，包括四次歐洲聯賽冠軍盃冠軍和十屆西班牙甲組聯賽冠軍，獲得輝煌的球會成就，同時打破諸多紀錄。美斯和C朗被媒體、名宿和球迷廣泛認定為世界球壇有史以来最伟大的球员之一[3][4][5][6][7][8]，美斯亦被球王馬勒當拿視為“完美接班人”[9]。  2009年，美斯以主力射手身份協助巴塞罗那連獲西甲聯賽、西班牙國王盃、歐洲聯賽冠軍盃三項冠軍，成為西班牙足球史上第一支三冠王球隊。接著參加下半年的三項專屬於冠軍之間的賽事，再度榮獲西班牙超級盃、歐洲超級盃、以及世界冠軍球會盃三大錦標，協助巴塞隆拿完成世界足壇絕無僅有的一次「六冠王」偉業。2011年，梅西協助巴塞罗那再奪得五項冠軍，成為該年的「五冠王」。', '4.png', '利昂内尔·安德烈斯·“莱奥”·梅西·庫西提尼（西班牙語：Lionel Andrés &quot;Leo&quot; Messi Cuccittini，西班牙語發音：[ljoˈnel anˈdɾes ˈmesi]  聆聽，1987年6月24日－），简称梅西，生于阿根廷圣菲省罗萨里奥，擁有加泰罗尼亚及意大利血統[2]，現時為西班牙甲組足球聯賽豪門巴塞罗那的隊長，場上主要擔任翼鋒，也可以擔任前鋒並回撤為進攻中場，目前他共獲得10次聯賽冠軍、4次歐冠冠軍、6座歐洲金靴獎、6次歐聯神射手、6座金球獎及6次FIFA國際世界足球先生。  美斯年少時已加入巴塞隆拿青訓營拉瑪西亞，出道至今職業生涯一直效力巴塞罗那，並隨隊橫掃多項賽事冠軍，包括四次歐洲聯賽冠軍盃冠軍和十屆西班牙甲組聯賽冠軍，獲得輝煌的球會成就，同時打破諸多紀錄。美斯和C朗被媒體、名宿和球迷廣泛認定為世界球壇有史以来最伟大的球员之一[3][4][5][6][7][8]，美斯亦被球王馬勒當拿視為“完美接班人”[9]。  2009年，美斯以主力射手身份協助巴塞罗那連獲西甲聯賽、西班牙國王盃、歐洲聯賽冠軍盃三項冠軍，成為西班牙足球史上第一支三冠王球隊。接著參加下半年的三項專屬於冠軍之間的賽事，再度榮獲西班牙超級盃、歐洲超級盃、以及世界冠軍球會盃三大錦標，協助巴塞隆拿完成世界足壇絕無僅有的一次「六冠王」偉業。2011年，梅西協助巴塞罗那再奪得五項冠軍，成為該年的「五冠王」。  2010年、2011年、2012年，梅西連續取得首三屆國際足協金球獎，總計梅西創紀錄地連續四年獲此殊榮。  2014年，美斯與内马尔和路爾斯·艾拔圖·蘇亞雷斯組成「MSN組合」。 2015年，梅西協助巴塞罗那再奪得五項冠軍，成為該年的「五冠王」，並再獲國際足協金球獎，成為歷史上首位奪下五屆金球獎和世界足球先生（不論合併前後）的球員[5][10][11]。2014-2015球季，「MSN組合」總共攻入122球，公認為史上最強大恐怖的鋒線組合。協助巴塞隆納成為史上第一支兩奪五冠王的球隊。  2011年，美斯以24歲之齡成為巴塞罗那足球會歷史上，在正式比賽中入球數最多的球員[6][12]。至今，美斯已在巴塞罗那攻入602球[13]，保有歐洲聯賽為單一球會入球的紀錄。他亦是西甲兼歐洲單一聯賽歷史上入球（419）最多的球員[14]，2019年美斯成為唯一突破西甲兼歐洲單一聯賽400進球的球員[15]，而且西甲助攻量（162）也是史上之最。美斯目前為巴萨和阿根廷上場的816場比賽中，不但進了668球，連助攻次數也有271次，没有球員能達到這個助攻數量，證明梅西也是大師級中场。', '5.jpg', '國家隊方面，27歲的美斯以隊長身份帶領阿根廷隊在2014年世界盃過關斬將，連續四場成為全場最佳球員，阿根廷隊決賽戰至加時僅敗於德國隊，屈居亞軍。梅西賽後獲頒世界盃金球獎[16]。  2016年美洲國家盃決賽裏，智利互射十二碼擊敗阿根廷，梅西不幸踢失，2014年世界杯和2015年美洲杯以及2016年美洲國家盃連續三次與國家隊冠軍擦身而過的沉痛打擊讓梅西賽後宣布退出國家隊，引發阿根廷舉國上下挽留，新教練親赴巴塞隆納游說，總統更親自致電慰留。2016年8月中旬，梅西宣佈回歸國家隊并将参与2018年世界盃的比赛。只不過在這屆世盃中，於16強僅以3-4敗於該屆世界盃冠軍的法國。  2019年，加泰羅尼亞政府為表揚他對宣揚當地文化、社會、慈善、青少年發展、醫療的卓越貢獻，授予最高榮譽之一的聖佐迪十字勳章。2019年，梅西獲得生涯第六次國際足總最佳男子球員獎、金球獎與歐洲金靴獎，成為歷史上唯一一位奪下六屆的球員。 [7][8][17][18]。  2019年，根據《福布斯》公佈的全球運動員收入排行榜，美斯以年收入1.27億美元，排名第1名[19]。 2020年劳伦斯世界体育奖在德国柏林举行。梅西、汉密尔顿共同当选年度最佳男运动员，历史第一位奪得該獎項的足球運動員。', '6.jpg', '2019年，加泰羅尼亞政府為表揚他對宣揚當地文化、社會、慈善、青少年發展、醫療的卓越貢獻，授予最高榮譽之一的聖佐迪十字勳章。2019年，梅西獲得生涯第六次國際足總最佳男子球員獎、金球獎與歐洲金靴獎，成為歷史上唯一一位奪下六屆的球員。 [7][8][17][18]。  2019年，根據《福布斯》公佈的全球運動員收入排行榜，美斯以年收入1.27億美元，排名第1名[19]。 2020年劳伦斯世界体育奖在德国柏林举行。梅西、汉密尔顿共同当选年度最佳男运动员，历史第一位奪得該獎項的足球運動員。', '1.png', NULL, NULL, NULL, 'Fashion', 'YES', '2020-03-31 09:28:14', '2020-04-01 02:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) NOT NULL,
  `ch_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `ch_name`, `en_name`, `date_created`, `date_updated`) VALUES
(1, '阿布哈茲', 'Abkhazia', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(2, '阿富汗', 'Afghanistan', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(3, '奥兰', 'Åland', '2020-02-28 02:51:32', '2020-02-28 02:53:16'),
(4, '阿尔巴尼亚', 'Albania', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(5, '阿尔及利亚', 'Algeria', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(6, '美属萨摩亚', 'American Samoa', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(7, '安道尔', 'Andorra', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(8, '安哥拉', 'Angola', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(9, '安圭拉', 'Anguilla', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(10, '安地卡及巴布達', 'Antigua and Barbuda', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(11, '阿根廷', 'Argentina', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(12, '亞美尼亞', 'Armenia', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(13, '阿尔扎赫', 'Artsakh', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(14, '阿鲁巴', 'Aruba', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(15, '澳大利亚', 'Australia', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(16, '奥地利', 'Austria', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(17, '阿塞拜疆', 'Azerbaijan', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(18, '巴哈马', 'Bahamas', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(19, '巴林', 'Bahrain', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(20, '孟加拉国 孟加拉国', 'Bangladesh', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(21, '巴巴多斯', 'Barbados', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(22, '白俄羅斯', 'Belarus', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(23, '比利時', 'Belgium', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(24, '伯利兹', 'Belize', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(25, '贝宁', 'Benin', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(26, '百慕大', 'Bermuda', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(27, '不丹', 'Bhutan', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(28, '玻利维亚', 'Bolivia', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(29, '波斯尼亚和黑塞哥维那', 'Bosnia and Herzegovina', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(30, '博茨瓦纳', 'Botswana', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(31, '巴西', 'Brazil', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(32, '文莱', 'Brunei', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(33, '保加利亚', 'Bulgaria', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(34, '布吉納法索', 'Burkina Faso', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(35, '布隆迪', 'Burundi', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(36, '柬埔寨', 'Cambodia', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(37, '喀麦隆', 'Cameroon', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(38, '加拿大', 'Canada', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(39, '佛得角', 'Cape Verde', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(40, '开曼群岛', 'Cayman Islands', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(41, '中非共和国', 'Central African Republic', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(42, '乍得', 'Chad', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(43, '智利', 'Chile', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(44, '中国', 'China', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(45, '圣诞岛', 'Christmas Island', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(46, '科科斯（基林）', 'Cocos (Keeling) Islands', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(47, '哥伦比亚', 'Colombia', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(48, '科摩罗', 'Comoros', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(49, '刚果（布）', 'Congo (Brazzaville)', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(50, '刚果（金）', 'Congo (Kinshasa)', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(51, '庫克群島', 'Cook Islands', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(52, '哥斯达黎加', 'Costa Rica', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(53, '科特迪瓦', 'Côte d\'Ivoire\r\n', '2020-02-28 02:51:37', '2020-02-28 03:38:22'),
(54, '克罗地亚', 'Croatia', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(55, '古巴', 'Cuba', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(56, '库拉索', 'Curaçao\r\n', '2020-02-28 02:51:37', '2020-02-28 03:37:19'),
(57, '賽普勒斯', 'Cyprus', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(58, '捷克', 'Czech Republic', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(59, '丹麥', 'Denmark', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(60, '吉布提', 'Djibouti', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(61, '多米尼克', 'Dominica', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(62, '多米尼加', 'Dominican Republic', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(63, '顿涅茨克', 'Donetsk', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(64, '厄瓜多尔', 'Ecuador', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(65, '埃及', 'Egypt', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(66, '薩爾瓦多', 'El Salvador', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(67, '赤道几内亚', 'Equatorial Guinea', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(68, '厄立特里亚', 'Eritrea', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(69, '爱沙尼亚', 'Estonia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(70, '斯威士兰', 'Eswatini', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(71, '衣索比亞', 'Ethiopia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(72, '福克蘭群島', 'Falkland Islands', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(73, '法罗群岛', 'Faroe Islands', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(74, '斐济', 'Fiji', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(75, '芬兰', 'Finland', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(76, '法國', 'France', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(77, '法屬玻里尼西亞', 'French Polynesia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(78, '加彭', 'Gabon', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(79, '冈比亚', 'Gambia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(80, '格鲁吉亚', 'Georgia', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(81, '德國', 'Germany', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(82, '加纳', 'Ghana', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(83, '直布罗陀', 'Gibraltar', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(84, '希臘', 'Greece', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(85, '格陵兰', 'Greenland', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(86, '格瑞那達', 'Grenada', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(87, '關島', 'Guam', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(88, '危地马拉', 'Guatemala', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(89, '根西', 'Guernsey', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(90, '几内亚', 'Guinea', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(91, '几内亚比绍', 'Guinea-Bissau', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(92, '圭亚那', 'Guyana', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(93, '海地', 'Haiti', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(94, '洪都拉斯', 'Honduras', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(95, '香港', 'Hong Kong', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(96, '匈牙利', 'Hungary', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(97, '冰島', 'Iceland', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(98, '印度', 'India', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(99, '印尼', 'Indonesia', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(100, '伊朗', 'Iran', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(101, '伊拉克', 'Iraq', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(102, '爱尔兰', 'Ireland', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(103, '马恩岛', 'Isle of Man', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(104, '以色列', 'Israel', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(105, '意大利', 'Italy', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(106, '牙买加', 'Jamaica', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(107, '日本', 'Japan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(108, '澤西', 'Jersey', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(109, '约旦', 'Jordan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(110, '哈萨克斯坦', 'Kazakhstan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(111, '肯尼亚', 'Kenya', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(112, '基里巴斯', 'Kiribati', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(113, '科索沃', 'Kosovo', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(114, '科威特', 'Kuwait', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(115, '吉尔吉斯斯坦', 'Kyrgyzstan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(116, '老挝', 'Laos', '2020-02-28 02:51:44', '2020-02-28 02:51:44'),
(117, '拉脫維亞', 'Latvia', '2020-02-28 02:51:44', '2020-02-28 02:51:44'),
(118, '黎巴嫩', 'Lebanon', '2020-02-28 02:51:45', '2020-02-28 02:51:45'),
(119, '賴索托', 'Lesotho', '2020-02-28 02:51:45', '2020-02-28 02:51:45'),
(120, '利比里亚', 'Liberia', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(121, '利比亞', 'Libya', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(122, '列支敦斯登', 'Liechtenstein', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(123, '立陶宛', 'Lithuania', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(124, '卢甘斯克', 'Luhansk', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(125, '卢森堡', 'Luxembourg', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(126, '澳門', 'Macau', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(127, '马达加斯加', 'Madagascar', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(128, '马拉维', 'Malawi', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(129, '马来西亚', 'Malaysia', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(130, '馬爾地夫', 'Maldives', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(131, '马里', 'Mali', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(132, '馬爾他', 'Malta', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(133, '马绍尔群岛', 'Marshall Islands', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(134, '毛里塔尼亚', 'Mauritania', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(135, '模里西斯', 'Mauritius', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(136, '墨西哥', 'Mexico', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(137, '密克羅尼西亞聯邦', 'Micronesia', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(138, '摩尔多瓦', 'Moldova', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(139, '摩納哥', 'Monaco', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(140, '蒙古國', 'Mongolia', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(141, '蒙特內哥羅', 'Montenegro', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(142, '蒙特塞拉特', 'Montserrat', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(143, '摩洛哥', 'Morocco', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(144, '莫桑比克', 'Mozambique', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(145, '緬甸', 'Myanmar', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(146, '纳米比亚', 'Namibia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(147, '瑙鲁', 'Nauru', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(148, '尼泊尔', 'Nepal', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(149, '荷蘭', 'Netherlands', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(150, '新喀里多尼亞', 'New Caledonia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(151, '新西蘭', 'New Zealand', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(152, '尼加拉瓜', 'Nicaragua', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(153, '尼日尔', 'Niger', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(154, '奈及利亞', 'Nigeria', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(155, '纽埃', 'Niue', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(156, '朝鲜', 'North Korea', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(157, '北馬其頓', 'North Macedonia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(158, '北塞浦路斯', 'Northern Cyprus', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(159, '北馬里亞納群島', 'Northern Mariana Islands', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(160, '挪威', 'Norway', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(161, '阿曼', 'Oman', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(162, '巴基斯坦', 'Pakistan', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(163, '帛琉', 'Palau', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(164, '巴勒斯坦', 'Palestine', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(165, '巴拿马', 'Panama', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(166, '巴布亚新几内亚', 'Papua New Guinea', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(167, '巴拉圭', 'Paraguay', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(168, '秘魯', 'Peru', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(169, '菲律賓', 'Philippines', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(170, '皮特凯恩群岛', 'Pitcairn Islands', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(171, '波蘭', 'Poland', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(172, '葡萄牙', 'Portugal', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(173, '德涅斯特河沿岸', 'Pridnestrovie', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(174, '波多黎各', 'Puerto Rico', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(175, '卡塔尔', 'Qatar', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(176, '羅馬尼亞', 'Romania', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(177, '俄羅斯', 'Russia', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(178, '卢旺达', 'Rwanda', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(179, '圣巴泰勒米', 'Saint Barthelemy', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(180, '圣基茨和尼维斯', 'Saint Christopher and Nevis', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(181, '圣赫勒拿、阿森松和特里斯坦-达库尼亚', 'Saint Helena, Ascension and Tristan da Cunha', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(182, '圣卢西亚', 'Saint Lucia', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(183, '圣皮埃尔和密克隆', 'Saint Pierre and Miquelon', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(184, '圣文森特和格林纳丁斯', 'Saint Vincent and the Grenadines', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(185, '萨摩亚', 'Samoa', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(186, '圣马力诺', 'San Marino', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(187, '聖多美和普林西比', 'São Tomé and Príncipe\r\n', '2020-02-28 02:51:52', '2020-02-28 03:39:40'),
(188, '沙烏地阿拉伯', 'Saudi Arabia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(189, '塞内加尔', 'Senegal', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(190, '塞爾維亞', 'Serbia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(191, '塞舌尔', 'Seychelles', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(192, '塞拉利昂', 'Sierra Leone', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(193, '新加坡', 'Singapore', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(194, '荷屬聖馬丁', 'Sint Maarten', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(195, '斯洛伐克', 'Slovakia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(196, '斯洛維尼亞', 'Slovenia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(197, '所罗门群岛', 'Solomon Islands', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(198, '索馬利亞', 'Somalia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(199, '索馬利蘭', 'Somaliland', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(200, '南非', 'South Africa', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(201, '韩国', 'South Korea', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(202, '南奥塞梯', 'South Ossetia', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(203, '南蘇丹', 'South Sudan', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(204, '西班牙', 'Spain', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(205, '斯里蘭卡', 'Sri Lanka', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(206, '苏丹', 'Sudan', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(207, '苏里南', 'Suriname', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(208, '斯瓦尔巴', 'Svalbard', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(209, '瑞典', 'Sweden', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(210, '瑞士', 'Switzerland', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(211, '叙利亚', 'Syria', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(212, '中華民國', 'Taiwan', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(213, '塔吉克斯坦', 'Tajikistan', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(214, '坦桑尼亚', 'Tanzania', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(215, '泰國', 'Thailand', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(216, '梵蒂冈', 'The Holy See（Vatican City）', '2020-02-28 02:51:55', '2020-02-28 03:40:13'),
(217, '东帝汶', 'Timor-Leste', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(218, '多哥', 'Togo', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(219, '托克勞', 'Tokelau', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(220, '汤加', 'Tonga', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(221, '千里達及托巴哥', 'Trinidad and Tobago', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(222, '突尼西亞', 'Tunisia', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(223, '土耳其', 'Turkey', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(224, '土库曼斯坦', 'Turkmenistan', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(225, '特克斯和凯科斯群岛', 'Turks and Caicos Islands', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(226, '图瓦卢', 'Tuvalu', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(227, '乌干达', 'Uganda', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(228, '烏克蘭', 'Ukraine', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(229, '阿联酋', 'United Arab Emirates', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(230, '英國', 'United Kingdom', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(231, '美國', 'United States', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(232, '乌拉圭', 'Uruguay', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(233, '乌兹别克斯坦', 'Uzbekistan', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(234, '瓦努阿圖', 'Vanuatu', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(235, '委內瑞拉', 'Venezuela', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(236, '越南', 'Vietnam', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(237, '英屬維爾京群島', 'Virgin Islands, British', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(238, '瓦利斯和富圖納', 'Wallis and Futuna', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(239, '西撒哈拉', 'Western Sahara', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(240, '葉門', 'Yemen', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(241, '尚比亞', 'Zambia', '2020-02-28 02:51:58', '2020-02-28 02:51:58'),
(242, '辛巴威', 'Zimbabwe', '2020-02-28 02:51:58', '2020-02-28 02:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `email_verified` bigint(20) DEFAULT 1,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `password_two` char(64) DEFAULT NULL,
  `salt_two` char(64) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `ekyc_update` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `favorite_project` varchar(255) DEFAULT NULL,
  `petri_rating` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `email_verified`, `password`, `salt`, `password_two`, `salt_two`, `phone_no`, `full_name`, `ekyc_update`, `nationality`, `favorite_project`, `petri_rating`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9a04ea5286dc214d621622e14a858dc4', 'admin', 'admin@gmail.com', 1, '2bd3fc76b60d220e67a40adb4c4a9be1d2cdde2afb4a406b6ff1c2daad40ea59', 'a221197fcfb97258210864e1175f32f221cdc16f', NULL, NULL, '6012-3456789', '', NULL, NULL, NULL, NULL, 1, 1, '2020-03-18 06:28:23', '2020-03-30 06:43:42'),
(2, '1ae56476f927e4c262036260d408c522', 'user1', 'user1@gg.cc', 1, '666f06bd48307f6c2880c72aacc14d4d8d358529aa72bad36d6bbe40f0b2a8d7', '9f572918c6bf2e9df9705e4118e2e9a9df4dc4ab', NULL, NULL, '6011-2233445', NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-03-30 04:30:51', '2020-03-30 06:43:39'),
(3, '1f348034d3ced3d2c12e75cf47e24ea7', 'user2', 'user2@gg.cc', 1, '8b9f5b310e81093571ccf1539990780332108cceebc3de6b87eb2f2f341300c3', 'f7af25b4ea4d206d26f1cdfb7082a38e459728d4', NULL, NULL, '987987', NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-03-30 08:11:12', '2020-03-30 08:11:12'),
(4, 'd8082496b846959914a7e2809de6ff33', 'user3', 'user3@gg.cc', 1, 'c17dd9d8c709e55a64a236a215e0eb642ab029b3187934f33802b1f20bd0d8fa', 'e92a9d79c3ab8ec3d2feec9128d08822c5fe13cf', NULL, NULL, '456654', NULL, NULL, NULL, NULL, NULL, 1, 1, '2020-03-30 08:11:35', '2020-03-30 08:11:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles_one`
--
ALTER TABLE `articles_one`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles_one`
--
ALTER TABLE `articles_one`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
