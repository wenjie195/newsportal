-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2020 at 08:31 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_news`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles_one`
--

CREATE TABLE `articles_one` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `article_link` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `title_cover` varchar(255) DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `paragraph_four` text DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `paragraph_five` text DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `paragraph_six` text DEFAULT NULL,
  `paragraph_seven` text DEFAULT NULL,
  `paragraph_eight` text DEFAULT NULL,
  `paragraph_nine` text DEFAULT NULL,
  `paragraph_ten` text DEFAULT NULL,
  `image_six` varchar(255) DEFAULT NULL,
  `image_seven` varchar(255) DEFAULT NULL,
  `image_eight` varchar(255) DEFAULT NULL,
  `image_nine` varchar(255) DEFAULT NULL,
  `image_ten` varchar(255) DEFAULT NULL,
  `img_one_source` text DEFAULT NULL,
  `img_two_source` text DEFAULT NULL,
  `img_three_source` text DEFAULT NULL,
  `img_four_source` text DEFAULT NULL,
  `img_five_source` text DEFAULT NULL,
  `img_six_source` text DEFAULT NULL,
  `img_seven_source` text DEFAULT NULL,
  `img_eight_source` text DEFAULT NULL,
  `img_nine_source` text DEFAULT NULL,
  `img_ten_source` text DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles_one`
--

INSERT INTO `articles_one` (`id`, `uid`, `author_uid`, `author_name`, `title`, `seo_title`, `article_link`, `keyword_one`, `keyword_two`, `title_cover`, `paragraph_one`, `image_one`, `paragraph_two`, `image_two`, `paragraph_three`, `image_three`, `paragraph_four`, `image_four`, `paragraph_five`, `image_five`, `paragraph_six`, `paragraph_seven`, `paragraph_eight`, `paragraph_nine`, `paragraph_ten`, `image_six`, `image_seven`, `image_eight`, `image_nine`, `image_ten`, `img_one_source`, `img_two_source`, `img_three_source`, `img_four_source`, `img_five_source`, `img_six_source`, `img_seven_source`, `img_eight_source`, `img_nine_source`, `img_ten_source`, `author`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, 'd1a5b19a9029bf49f47df7b336cd12b7', '9a04ea5286dc214d621622e14a858dc4', 'admin', 'Top 5 Apps that will enhance your Fashion or Beauty Instagram postings', 'Top5AppsthatwillenhanceyourFashionorBeautyInstagrampostings-beautifyphotoapps', 'Top5AppsthatwillenhanceyourFashionorBeautyInstagrampostings-beautifyphotoapps', 'beautifyphotoapps', 'Instagram, Social, Beauty, Apps, Photo, Picture, Fashion, Trend, Photo editor, Canva, Unfold, Snow, Highlight Cover Make, Type Artr,', 'neonbrand-nZJBt4gQlKI-unsplash.jpg', 'This app allows you to add in any template of your choice to your photo such as characters, stickers, background, frame, text and so on,   It allows customizing your photo to make it look more professional and creative even though you are not a professional.', 'unnamed.png', 'If you are a storyteller and wanted to take your social stories to the next level, then you can consider Unfold. This app can help users to create beautiful stories with easy-to-use templates. The templates provided by Unfold are all in a minimalist style.', 'unnamed (1).png', 'Are you looking for a way to replace photo posting on your IG in order to grab attention? Then you should use Snow and it is available on Google Play Store and App Store.   The best thing about Snow is you can add in music into your video, they have a lot of trendy kinds of songs like POP, Chillout, Party, Soft and many more. You will definitely have to try on their “Hot” song. Make sure to check on their other functions like “Story” to create motion stories with unique filters that Instagram did not provide.', 'unnamed (2).png', 'Want to highlight all the important stories you posted? Make your highlights relevant &amp; different from the others! Now is the best time for you to make changes on your Instagram!   Highlight Cover Maker is a story covers editor app for Instagram stories. It provides high-quality Instagram story highlights covers to add to your Instagram profile and attract more likes and followers to you!', 'unnamed (3).png', 'Want to make your Insta stories stand out among others? Less boring and more interesting? Now, you can do it with TypeArt. Not only adding text inside your stories but with special animated text and also music. But it is only available for Apple user at App Store.', 'unnamed (4).png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ONE', 'TWO', 'THREE', 'FOUR', 'FIVE', '6', '7', '8', '9', '10', 'Joe D Flezer, Junn Hann Tan, Raymond Ho, Ewei Goh, Emily Choong', 'Social', 'YES', '2020-04-03 09:17:56', '2020-04-07 06:31:19'),
(3, '023385e29d85c2af15fab73307ef18b7', '9a04ea5286dc214d621622e14a858dc4', 'admin', 'Food Panda Promo Code Up To 60% Discount To All Foodpanda Customers!', 'FoodPandaPromoCodeUpTo60%DiscountToAllFoodpandaCustomers!-Foodpanda-April-Promotion-2020', 'FoodPandaPromoCodeUpTo60%DiscountToAllFoodpandaCustomers!-Foodpanda-April-Promotion-2020', 'Foodpanda-April-Promotion-2020', 'lifestyle,promotion,foodpanda', 'Panda 1.png', 'Food Panda is giving out exclusive promotions with up to 60% discount for breakfast, teatime, dinner and snacks to fix Malaysian cravings during this Movement Control Order (MCO)! Customers can apply different promo codes at different times.', 'Panda 2.png', 'Food Panda is giving out 60% off for the Malaysian and work from home warriors to enjoy. The minimum order is just RM15 and the promo code redemption period is every 9 am to 11 am. Order your All Day Breakfast now!', 'Panda 4.png', 'Your hangry spell during the 2 pm to 4 pm? Apply JOMTEATIME and order Food Panda lah ! Grab some cakes or boba fixes is super duper satisfied. Don’t forget to upload your food to Instagram !', 'Panda 6.png', 'Feeling tired after work, but too lazy to cook? Get your 25% off here when you order with Foodpanda!', 'Panda 8.png', 'Don’t know what to have for your snacks? Foodpanda is having a snacks promotion code for you!', 'Panda 3.jpg', 'Be sure to apply your promo code during checkout, don’t miss the opportunity because it has a limit per day. Order your food by using FoodPanda now !', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Raymond Ho, Junn Hann', 'Social', 'YES', '2020-04-03 14:36:35', '2020-04-07 02:09:40'),
(4, '96c72ea49c443ef1b5e5064d3b0f3238', '9a04ea5286dc214d621622e14a858dc4', 'admin', 'Use Simple Material To Take IG Photo Like a Pro!', 'UsesimplematerialtotakeIGPhotolikeaPro!-Creative-IG-Photo', 'UsesimplematerialtotakeIGPhotolikeaPro!-Creative-IG-Photo', 'Creative-IG-Photo', 'tips,instagram,materials,pro,professional', '8VcmgPfoRdSaKhj77mLQ_5_Easy_Ways_to_Take_Instagram_Photos_Like_a_Pro_-_Editing.jpg', 'What to do at home during lock down period? Want to post on IG but lack of photos? Here, you can learn how to take IG photo like a pro just use some easy tips and materials.', '', 'Plastic Wrap. Yes, don’t be surprised. You can take a gorgeous photo with just a plastic wrap. What you need to do is just open it up and splash some water on it. Now, open the flashlight and close the lamp in your room. Adjust your angle, start taking photos.', '1.jpg', 'Any Chips Bag.After finishing the chips, don’t throw away the bag. Because it will be a good material to help you take wonderful photos. Cut off the chips bag, clean it and dry it. Ok done!', '3.jpg', 'Glass. Don’t underestimate what a glass can do. What you need to do is just turn on the flashlight to illuminate the light to the glass.', '4.jpg', 'With just easy found material and simple steps, you now can start to take your IG photos like a Pro. What you are waiting for, start to “boom” photos into your Instagram!', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Beauty', 'HIDE', '2020-04-03 14:53:25', '2020-04-07 01:59:06'),
(5, '1844773179d0a88515ad5488f519aee2', '9a04ea5286dc214d621622e14a858dc4', 'admin', '就让我们一起来用Highlight Cover Maker把我们的Insta Story美起来！', 'HighlightCoverMaker让您的InstaStory美起来！-Highlight-Cover-Maker-教学', 'HighlightCoverMaker让您的InstaStory美起来！-Highlight-Cover-Maker-教学', 'Highlight-Cover-Maker-教学', 'IG，教学，应用程序', 'Highlight 1.png', '踏入2020年第4个月，我们不难发现电子商业的重要性。而Instagram正是最好的媒介让我们把各种产品图片上载以便读者可以随时观看。然而， 一个吸引读者们的Instagram账号不可或缺的是产品图片之外，其中还包括账号本身的外观， 今天就让小编介绍美眉们一款简易好用的APP， 让美眉们的Instagram外观靓到爆吧。', '', '每当我们点入Instagram， 第一个看到的就是我们的Insta Story， 如果没有一个漂亮图片作为覆盖，美眉的账号分数将会被大打折扣哦！ 以下便让我们一起来看看它的用法吧。步骤1：点开APP， 美眉们将看到各式各样的徽标。如果美眉想根据自己的喜欢设计一个，只需点击下方“+”。', 'Highlight 2.jpg', '步骤2： 然后美眉们可以从背景，相框，徽标以及文字中选择美眉喜欢的。', 'Highlight 3.jpg', '步骤3: 完成以后，只学点击下载，美眉们就可以上载至美眉的Insta Story当覆盖啦。', 'Highlight 4.jpg', '是不是很简单呢？真的是轻易的动动手指就能让我们Instagram外观美美哒。  想了解更多更方便更建议的APP？记得关注我们以及订阅哟。', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ewei Goh', 'Social', 'YES', '2020-04-03 15:05:07', '2020-04-07 02:09:49'),
(6, 'f9436005836498464508cc0e44e60cdd', '9a04ea5286dc214d621622e14a858dc4', 'admin', '什么！？即使在室内也会有被晒伤的危机？', '什么！？即使在室内也会有被晒伤的危机？-防晒小贴士-室内', '什么！？即使在室内也会有被晒伤的危机？-防晒小贴士-室内', '防晒小贴士-室内', '太阳,阳光,室内,防晒,晒伤,危机', 'What 1.jpg', '很多女性都清楚知道出门需要搽防晒，不只是在脸上同时手脚也很需要，不然长时间暴晒在太阳底下以及紫外线的攻击，我们的肌肤将会损伤，同时也会面临患癌的危机。', 'What 2.jpg', '那么，您知道室内阳光也会有一定的危害吗？', 'What 3.jpeg', '在室内虽然我们没有直接接触太阳的照射，可是依然会带有一些些的紫外线。尤其是长期在窗户边工作或休息的您，这一些些的紫外线也会对您的肌肤带来伤害哟。', 'What 4.jpg', '紫外线有分三种，短波紫外线，中波紫外线，长波紫外线。短波紫外线其实对人们没有多大损伤，因为它在穿过大气层的时候就已经被过滤了，能到达地面的不到2%。中波紫外线能被玻璃阻挡大约80%的伤害，所以只要不打开窗户或玻璃门来个直接阳光接触，对我们的皮肤伤害其实也不严重。然而长波紫外线则是室内阳光的头号威胁。长波紫外线由于穿透性比较强，它能被隔在玻璃外的伤害不到10%，所以它的伤害可谓是直透我们的皮肤深层，所以带给我们皮肤的损伤是最大的。为了避免皮肤损伤，室内防晒还是必要的。', 'What 5.png', '为了让我们的皮肤保持健康，小编建议在室内用的防晒可以选用较低的SPF值，例如SPF35的防晒产品。除此之外，小编也建议长期在窗户边工作的美眉们，如果不是非常必要，也许可以更改一下工作位置，减少损伤以及患癌几率。', '', '最后小编在这里提醒大家，即使是在室内，一层薄薄的防晒也是要做足，不可以偷懒哟。', '想了解更多关于护肤美肌？记得关注我们吖，欢迎订阅哟。', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ewei Goh', 'Beauty', 'YES', '2020-04-06 12:43:50', '2020-04-07 02:13:06'),
(7, 'ba00cfec9875404ceb3fa039a3357859', '9a04ea5286dc214d621622e14a858dc4', 'admin', '只需9天就可炼成！5个步骤让马甲线粘着您 #马甲线 #减肥打卡', '只需9天就可炼成！5个步骤让马甲线粘着您#马甲线#减肥打卡-只需9天就可炼成!5个步骤让马甲线粘着您-马甲线-减肥打卡-9天炼成马甲线-减肥-教学', '只需9天就可炼成!5个步骤让马甲线粘着您-马甲线-减肥打卡-9天炼成马甲线-减肥-教学', '9天炼成马甲线-减肥-教学', '9天马甲线,人鱼线,瘦身,减肥', 'Zebra 1.1.jpg', '封城时期，每天在家的循环日常就是吃玩睡，苗条身材就快变没了。小编在封城的第一个星期胖了2公斤，我的天。为了让大家不被封城而影响，小编找了这个教学，在家也能练成马甲线，只要5个步骤，坚持9天，马甲线就基本成型啦。', 'Zebra 2.jpg', '步骤1： 腹部激活 20次 X 3组', 'Zebra 3.jpg', '步骤2： 摸膝 20次 X 3组', 'Zebra 4.jpg', '步骤3： 仰卧蹬车 20次 X 3组', 'Zebra 5.jpg', '步骤4： 俄罗斯转体 20次 X 3组', 'Zebra 6.jpg', '步骤5： 拉伸1分钟 X 2组', '是不是很简单呢？虽然步骤都简单，可是也不要小看它的功力哟，小编就是靠着这组教学成功减掉那可恶的2公斤，而且小编还是没有控制饮食的那种哦。如果想要观看完整影片，可以点击这链接（shorturl.at/jozL5）。以下就让大家看看小编的成果吧。', '虽然有点小遗憾小编的马甲线没有很成功（虽然有些线条，可是拍照却看不见），可是可以看得出来小编的腰型出来咯，而且能那么轻易就把2公斤给减掉了，其实小编已经很满足啦。正在看这篇文章的您，要不要来挑战一下9天练成马甲线呢？', '喜欢我的文章吗？喜欢的话记得关注以及订阅哦。', NULL, 'Zebra 7.jpg', 'Zebra 8.jpg', 'Zebra 9.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ewei Goh', 'Fashion', 'YES', '2020-04-06 13:06:41', '2020-04-07 04:05:11'),
(8, '6af5a9a99aea29dad1c1f11d99c78344', '9a04ea5286dc214d621622e14a858dc4', 'admin', '5 Must-Eat Food After The MCO', '5Must-EatFoodAfterTheMCO-Food-After-MCO', '5Must-EatFoodAfterTheMCO-Food-After-MCO', 'Food-After-MCO', 'Food-MCO-Countdown', 'Food MCO 1.png', 'Malaysian are restricted to have their meal outside their house during the MCO. Yes, you might order the food delivery service but there are some of the food that is not available for delivery or you must enjoy having it outside with your friends or family. Here they are.', 'Food MCO 2.jpeg', 'Roti Canai is one of the traditional food in Malaysia, however, due to the MCO Malaysians are not allowed to have their meal outside and are not supported by food delivery services. Yes, you may order take away but having roti canai and Teh Tarik together with your friends and family in the restaurant itself is still the best thing.', 'Food MCO 3.jpg', 'I believe most of the Malaysian are craving buffet and BBQ where you can serve yourself and this is the food that you are unable to have during this MCO. Not available for delivery as well.', 'Food MCO 4.jpg', 'Ramly burger is the most famous food for supper in Malaysia and the stalls are only available at night. However, Malaysian are encouraged to stay at home every 8 pm. “Burger ayam special satu tambah cheese bang”', 'Food MCO 5.jpg', 'You can make your own steamboat at home during the MCO but having it outside is much better especially at Hai Di Lao. You are being served as a King or Queen by their professional waiter and waitress and just sit back and enjoy the meal.', 'Food MCO 6.jpg', 'Delivery for lok lok? Impossible lahh, Lok Lok must eat at the stalls only nice weh. Eat all you can! Penang lang, miss Pulau Tikus Lok Lok? I am craving for that.', 'Stay safe Malaysian, we are going to enjoy all this food after the MCO and COVID-19 outbreak. We can do this!', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Raymond Ho', 'Social', 'YES', '2020-04-06 13:23:28', '2020-04-07 02:13:29'),
(9, 'c1737545977f9def69c4bbd236bc94dd', '9a04ea5286dc214d621622e14a858dc4', 'admin', '把家里变成摄影场，这里居然能拍出杂志封面，天啊！也太美了吧！', '把家里变成摄影场，这里居然能拍出杂志封面，天啊！也太美了吧！-把家里变成摄影场，这里居然能拍出杂志封面，天啊！也太美了吧！', '把家里变成摄影场，这里居然能拍出杂志封面，天啊！也太美了吧！-把家里变成摄影场，这里居然能拍出杂志封面，天啊！也太美了吧！', '把家里变成摄影场，这里居然能拍出杂志封面，天啊！也太美了吧！', '摄影，杂志封面，拍照，IG,', '摄影1.jpg', '行动限制令下，不能出门，不能出门，不能出门。那待在家要做什么啊？想拍美照？想要上传IG照？不怕，直接把家里变成摄影场，5种简单易学的网红照，你准备好了吗？', '摄影2.jpg', '洗衣机： 没有错！“洗衣机照”现在在网上可是掀起了一阵狂潮，许多人开始争相模仿。谁又能想到在洗衣机内拍照，竟然能拍出简单又不失高贵的照片。你只需要打开洗衣机盖子，把手机开到倒数计时然后放进去，然后快速摆好pose，咔嚓完成。PS：记得打开闪光灯模式。', '摄影3.jpg', '保鲜膜： 是不是很意外，保鲜膜除了是厨房的必备品之外，你还可以拿来拍网红照。撕一片保鲜膜，把它撑开，再在外层上撒一点水，打开手机的闪光灯然后关上房间的灯，调好角度靠在墙上就可以开始啦。PS: 保鲜膜会反光，大家小心照片曝光哦。', '摄影4.jpg', '薯片袋： 没有想到吧，吃完薯片了原来袋子还有这种用处。赶快学起来，下次吃完就来拍。把薯片袋（越大越好）头尾都减掉，把里面的碎片清洗干净，就可以开始拍啦。摆出你的pose，打开闪光灯，卡擦卡擦~', '摄影5.jpg', '玻璃杯： 不要小看一个玻璃杯的威力，你只要用过它来拍照就会知道我为什么这么说了。你只需要打手机里的手电筒模式打开，然后放入一个干净的玻璃杯里就可以开始摆pose了。PS: 不一样的玻璃杯有不一样的纹路，折射出来的水波纹也会不一样哦。', '摄影6.jpeg', '水雾： 每次冲完热水澡后，镜子上总会起一层水雾，你不知道原来这个时候，正是拍出美照的时机。你可以适当的抹掉少许的水雾，让你的脸孔若影若现，起到修饰脸型的效果。准备手机相机模式，开拍！', '是不是完全没有发现，原来这些地方/东西都可以拍出这么酷炫的照片。除了可以在这段期间打发时间，还能“轰炸”IG朋友圈。赶快把这些都学起来吧~', NULL, NULL, NULL, '摄影7.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Emily', 'Fashion', 'YES', '2020-04-06 13:36:36', '2020-04-07 02:12:36'),
(10, '034ecc35c9a3423d091116696ebc2443', '9a04ea5286dc214d621622e14a858dc4', 'admin', 'Getting bored during MCO? Look at these trends happening right now!', 'GettingboredduringMCO?Lookatthesetrendshappeningrightnow!-GettingboredduringMCO?Lookatthesetrendshappeningrightnow!-Social-Media-Trends-During-LockDown', 'GettingboredduringMCO?Lookatthesetrendshappeningrightnow!-Social-Media-Trends-During-LockDown', 'Social-Media-Trends-During-LockDown', 'Social Media, Trends, LockDown', 'SM Trends 1.jpg', 'Everyone has to stay at home due to coronavirus lockdown (COVID-19) and turning themselves to social media to vent. These are the trending events happening in social media:', 'SM Trends 2.jpg', 'Dancing Pallbearers/ Dancing Coffin (Facebook): Recently, Facebook is going viral about videos showing funny fail videos combined with Ghanian pallbearers dancing while they are carrying a coffin, paired with the EDM track ‘Astronomia” by Tony Igy.', 'SM Trends 3.png', 'Sharing Food Recipe (Facebook): Since everyone is forced to stay at home, many people decide to share their ‘secret recipe’ foods on Facebook. They included the steps and ingredients in the video so that others can learn to cook as well.', 'SM Trends 4.jpg', 'Instagram sticker: Instagram has a ‘stay home’ sticker to add in their stories. People that use the sticker will be featured under the “stay home” highlight. It can be a form of entertainment as other users can check on this highlight and see what others are doing during the lockdown period.', 'SM Trends 5.jpg', 'Until Tomorrow (Instagram): You will notice this trend if you see it from someone you follow on Instagram and they have this caption (Until Tomorrow)  park under their photo. This challenge is to post embarrassing photos or photos of that person for 24 hours, you will most likely be challenged by them if you like their post.', 'SM Trends 6.JPG', 'Home workout (Facebook &amp; Instagram): Since everyone could not go to the gym, but they still want to exercise to bring their sweats out. The majority of them will record themselves or start a live session on Facebook/ Instagram to show internet users what exercise they can do while staying at home.', 'Be sure to try out these trends, stay at home and stay safe everyone!', 'Written by: Junn Hann', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Junn Hann Tan', 'Social', 'YES', '2020-04-06 13:36:52', '2020-04-07 03:19:08'),
(11, '390cb0d32bfb4e77e8371b8240ed1d8f', '9a04ea5286dc214d621622e14a858dc4', 'admin', 'Top 5 things to do to overcome mental fatigue during COVID-19', 'Top5thingstodotoovercomementalfatigueduringCOVID-19-Overcome-Mental-Fatigue-COVID-19', 'Top5thingstodotoovercomementalfatigueduringCOVID-19-Overcome-Mental-Fatigue-COVID-19', 'Overcome-Mental-Fatigue-COVID-19', 'Mental Fatigue, COVID-19', 'MF 1.jpg', 'Mental fatigue is also known as mental exhaustion, happens when a person has too many decisions to make and over the capacity of handling the stress especially during this Movement Control Order. Over interruption, demand, multi-tasking are few of the factor that contributes to mental fatigue and it might happen when you are working from home as well. The more mental fatigue the less focus it’s and leads to underperforming.', '', 'Below are the five major things to do to overcome this issue.', 'MF 2.jpg', 'Make fewer decisions: The more decisions to be made, the more mental energy required to think about the choice and considering it. Make fewer decisions throughout the day, avoid unnecessary dilemmas on non-important decisions such as deciding what to eat, where to eat instead plan for it ahead like Monday breakfast is bread, Tuesday is oats and so on, this will reduce the decision require to make. Nonetheless, some working decisions can’t be avoided so it is best to choose which thing to decide first which thing not, by prioritizing urgent decisions to least urgent and decide on that first.', 'MF 3.jpg', 'Power of nature: Based on research, green nature does help in rejuvenating mental energy. For people living in a condominium/ apartment, they can just walk to their balcony and enjoy the scenery while for those who are living in a landed house, they could take a walk in their garden. This could help them to freshen their mind.', 'MF 4.jpg', 'Exercise: By doing exercise this allows our brain to take a rest from all the decisions and pending tasks and give it a quick boost. Indeed, not all people could go exercise and it takes a lot of effort so what I suggest is to take a simple movement rather keep starring at the screen or sitting on your chair. You can start exercising at home by watching videos tutorials on YouTube or if you have any cardio machine at home that would be pretty awesome.', 'MF 5.jpg', 'Get Enough of sleep: Overall sleep is very important. Some people think that sleeping less saves up a lot of time for important tasks but in fact, it is wrong and a common rookie mistake and will cause loss of concentration and as well as productivity. Sleep helps temporarily shut down our brain and also the mind which gives it time to recharge. Getting the right amount of rest is very important to have mental working as normal.', 'Listen to song and watch your favorite movie: Feeling stress during the work from home session because of the MCO? You can always get your 1-hour lunch break to watch your favorite dramas or movies and might as well open up your YouTube and listening to any Jazz or your favorite song. Fully utilize your lunch break to boost yourself to the best situation for the upcoming tasks.', 'It is tough for you during this hard time but always remember life still goes on. It is normal that feeling stress or fatigue during the crisis outbreak. However, we are glad that we still able to work from home for the company and the country. Good luck and stay safe.', NULL, NULL, 'MF 6.png', 'MF 7.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Joe D Flezer', 'Social', 'YES', '2020-04-06 13:45:11', '2020-04-07 02:11:46'),
(12, '03b0fc8897d45e0baee564b42ffc6a0f', '9a04ea5286dc214d621622e14a858dc4', 'admin', '5种天然护法材料，你不知道的小秘密，让你在家也能轻松护发', '5种天然护法材料，你不知道的小秘密，让你在家也能轻松护发-天然材料-轻松护发', '5种天然护法材料，你不知道的小秘密，让你在家也能轻松护发-天然材料-轻松护发', '天然材料-轻松护发', '天然，材料，护发，在家，轻松', '护发1.jpeg', '头发对于大部分女生来说是非常的重要，它可以让你的形象加分也会扣分。所以今天小编找了两种天然材料让你在家也能随时随地的护理头发。', '护发2.jpeg', '椰子油：说到天然护发材料一定少不了它--椰子油。它含有丰富的植物性油脂，可以深入的从内到外更有效地滋润头发，尤其更适合头发干燥的女生。你只需要在洗头发之前将椰子油从发根开始按摩至发尾，再等5分钟就可以冲洗了。', '护发3.jpeg', '绿茶： 绿茶除了能帮助美容，减肥，提神之外，它还是天然的护发神器。它本身含有的抗氧化物质能刺激你的头发生长以及拥有头皮治疗的功效，从而让你有拥有自然光滑亮丽的头发。你只需将2包绿茶浸泡在3杯热水中，然后把它放凉，在洗好头发之后再用绿茶做最后冲洗就行啦。', '护发4.jpeg', '鳄梨果： 鳄梨果，有些人会叫牛油果，里面含有丰富的维生素，脂肪酸和矿物质，可以帮助修复受损的头发以及恢复头发光泽。在这里，你除了需要一个去核的熟鳄梨之外，还需要一颗鸡蛋。将这两种材料加在一起捣碎，然后把它涂抹在湿发上，等待至少20分钟，再重复冲洗几次就完成啦。', '护发5.jpg', '芦荟： 相信大家都知道芦荟在保养界可是非常出名的，除了滋润皮肤还可以消炎杀菌。但是，你知道其实芦荟还可以帮助刺激头发生长以及恢复头发光泽吗? 你只需准备将至少4汤匙的芦荟胶和1汤匙的柠檬汁，将它们混合在一起。然后使用在沾上洗发液的头发上，静等5分钟，再用温水冲洗干净就完成了。', '护发6.jpg', '苏打粉： 小苏打对于家庭主妇来说可以说是不可或缺的帮手，除了能帮助清理顽固污渍还能做甜品。那么小苏打能帮助头发达到什么效果呢? 如果你是油性头皮那么小苏打绝对可以帮助你，因为小苏打可以帮你去除头皮多余的油脂还能恢复头发的光泽。 你可以每两个星期使用一次，但是切记，过度使用小苏打会导致头皮过度干燥，所以如果你是干性头皮就要减少使用次数。使用方法很简单，只需将1到2汤匙的小苏打加进少量的水里，搅拌至浓稠糊状就可以了。在使用洗发液前把它按摩在湿发上，等待15分钟，然后把头发冲洗干净再使用洗发液继续梳洗就可以了。', '女生就算待在家也要好好保养自己，头发自然不能少。想要拥有自然有光泽的头发吗? 今天就开始使用这些小妙招护发吧~', '想了解更多生活小贴士，欢迎关注与订阅哦。', NULL, NULL, '护发7.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Emily Choong', 'Beauty', 'YES', '2020-04-06 13:46:54', '2020-04-07 02:11:20'),
(14, '76aafcaeb9a0f949a5d73f5a7e41eab0', '9a04ea5286dc214d621622e14a858dc4', 'admin', 'Techniques To Grow Your Beauty Niche Instagram Page', 'TechniquesToGrowYourBeautyNicheInstagramPage-Techniques-grow-beauty-niche-instagram', 'TechniquesToGrowYourBeautyNicheInstagramPage-Techniques-grow-beauty-niche-instagram', 'Techniques-grow-beauty-niche-instagram', 'techniques,grow,beauty,nichie,instagram', 'Niche IG 1.jpg', 'Follow The Followers First and foremost, look into beauty niche competitors and start to follow the persons that liked your competitor post. Not any competitor post but the post with the least liked.   The persons that liked the worst post usually are super passionate followers and it means those followers could be interested in the contents you posted too.   You need to follow your competitors as well because this will mark your page account as same niche and this will show your page on suggestion when people follow.', 'Niche IG 2.png', 'Super Specific Contents The beauty content post should be as specific as possible. As an example, a beauty page can mean a lot but we can still differentiate it such as Asian Beauty, Western Beauty and to be more specific Asian Face beauty or Asian Body Beauty, Asian Skin Beauty. By being more specific, it creates uniqueness and differentiates from the competitors.', 'Niche IG 3.jpg', 'Interact with The Niche Start interacting with your beauty niche by commenting on competitor posts to increase your visibility to the public audiences. Create good comments that will attract a lot of likes and this will rank your comment higher in the post and people might curious about your page and click on it.', 'Niche IG 4.jpg', 'Consistent Posting on Right Time Yes, create a consistent posting this will put your account as active and Instagram will reward you by reaching more audience. Instagram is about to get people to spend more time and if a theme page could do that, Instagram will reward the page with more visitors. Remember to also test out the best time to post content by looking at which time period has more results.', 'Niche IG 5.png', 'Collaborate with Competitors Repost some of your competitor posts and mentioned them while they did the same and mention your page. The posts need to be interesting and not the full version so audiences will be curious to look into the post. As an example, a video that only plays a 10 second and full video is in the collaborator page. So the audience will curios to check it out.', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Joe D Flezer', 'Fashion', 'YES', '2020-04-06 14:01:09', '2020-04-07 02:11:36');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) NOT NULL,
  `ch_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `ch_name`, `en_name`, `date_created`, `date_updated`) VALUES
(1, '阿布哈茲', 'Abkhazia', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(2, '阿富汗', 'Afghanistan', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(3, '奥兰', 'Åland', '2020-02-28 02:51:32', '2020-02-28 02:53:16'),
(4, '阿尔巴尼亚', 'Albania', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(5, '阿尔及利亚', 'Algeria', '2020-02-28 02:51:32', '2020-02-28 02:51:32'),
(6, '美属萨摩亚', 'American Samoa', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(7, '安道尔', 'Andorra', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(8, '安哥拉', 'Angola', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(9, '安圭拉', 'Anguilla', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(10, '安地卡及巴布達', 'Antigua and Barbuda', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(11, '阿根廷', 'Argentina', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(12, '亞美尼亞', 'Armenia', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(13, '阿尔扎赫', 'Artsakh', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(14, '阿鲁巴', 'Aruba', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(15, '澳大利亚', 'Australia', '2020-02-28 02:51:33', '2020-02-28 02:51:33'),
(16, '奥地利', 'Austria', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(17, '阿塞拜疆', 'Azerbaijan', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(18, '巴哈马', 'Bahamas', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(19, '巴林', 'Bahrain', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(20, '孟加拉国 孟加拉国', 'Bangladesh', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(21, '巴巴多斯', 'Barbados', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(22, '白俄羅斯', 'Belarus', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(23, '比利時', 'Belgium', '2020-02-28 02:51:34', '2020-02-28 02:51:34'),
(24, '伯利兹', 'Belize', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(25, '贝宁', 'Benin', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(26, '百慕大', 'Bermuda', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(27, '不丹', 'Bhutan', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(28, '玻利维亚', 'Bolivia', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(29, '波斯尼亚和黑塞哥维那', 'Bosnia and Herzegovina', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(30, '博茨瓦纳', 'Botswana', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(31, '巴西', 'Brazil', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(32, '文莱', 'Brunei', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(33, '保加利亚', 'Bulgaria', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(34, '布吉納法索', 'Burkina Faso', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(35, '布隆迪', 'Burundi', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(36, '柬埔寨', 'Cambodia', '2020-02-28 02:51:35', '2020-02-28 02:51:35'),
(37, '喀麦隆', 'Cameroon', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(38, '加拿大', 'Canada', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(39, '佛得角', 'Cape Verde', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(40, '开曼群岛', 'Cayman Islands', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(41, '中非共和国', 'Central African Republic', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(42, '乍得', 'Chad', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(43, '智利', 'Chile', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(44, '中国', 'China', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(45, '圣诞岛', 'Christmas Island', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(46, '科科斯（基林）', 'Cocos (Keeling) Islands', '2020-02-28 02:51:36', '2020-02-28 02:51:36'),
(47, '哥伦比亚', 'Colombia', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(48, '科摩罗', 'Comoros', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(49, '刚果（布）', 'Congo (Brazzaville)', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(50, '刚果（金）', 'Congo (Kinshasa)', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(51, '庫克群島', 'Cook Islands', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(52, '哥斯达黎加', 'Costa Rica', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(53, '科特迪瓦', 'Côte d\'Ivoire\r\n', '2020-02-28 02:51:37', '2020-02-28 03:38:22'),
(54, '克罗地亚', 'Croatia', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(55, '古巴', 'Cuba', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(56, '库拉索', 'Curaçao\r\n', '2020-02-28 02:51:37', '2020-02-28 03:37:19'),
(57, '賽普勒斯', 'Cyprus', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(58, '捷克', 'Czech Republic', '2020-02-28 02:51:37', '2020-02-28 02:51:37'),
(59, '丹麥', 'Denmark', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(60, '吉布提', 'Djibouti', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(61, '多米尼克', 'Dominica', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(62, '多米尼加', 'Dominican Republic', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(63, '顿涅茨克', 'Donetsk', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(64, '厄瓜多尔', 'Ecuador', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(65, '埃及', 'Egypt', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(66, '薩爾瓦多', 'El Salvador', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(67, '赤道几内亚', 'Equatorial Guinea', '2020-02-28 02:51:38', '2020-02-28 02:51:38'),
(68, '厄立特里亚', 'Eritrea', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(69, '爱沙尼亚', 'Estonia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(70, '斯威士兰', 'Eswatini', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(71, '衣索比亞', 'Ethiopia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(72, '福克蘭群島', 'Falkland Islands', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(73, '法罗群岛', 'Faroe Islands', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(74, '斐济', 'Fiji', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(75, '芬兰', 'Finland', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(76, '法國', 'France', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(77, '法屬玻里尼西亞', 'French Polynesia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(78, '加彭', 'Gabon', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(79, '冈比亚', 'Gambia', '2020-02-28 02:51:39', '2020-02-28 02:51:39'),
(80, '格鲁吉亚', 'Georgia', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(81, '德國', 'Germany', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(82, '加纳', 'Ghana', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(83, '直布罗陀', 'Gibraltar', '2020-02-28 02:51:40', '2020-02-28 02:51:40'),
(84, '希臘', 'Greece', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(85, '格陵兰', 'Greenland', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(86, '格瑞那達', 'Grenada', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(87, '關島', 'Guam', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(88, '危地马拉', 'Guatemala', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(89, '根西', 'Guernsey', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(90, '几内亚', 'Guinea', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(91, '几内亚比绍', 'Guinea-Bissau', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(92, '圭亚那', 'Guyana', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(93, '海地', 'Haiti', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(94, '洪都拉斯', 'Honduras', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(95, '香港', 'Hong Kong', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(96, '匈牙利', 'Hungary', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(97, '冰島', 'Iceland', '2020-02-28 02:51:41', '2020-02-28 02:51:41'),
(98, '印度', 'India', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(99, '印尼', 'Indonesia', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(100, '伊朗', 'Iran', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(101, '伊拉克', 'Iraq', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(102, '爱尔兰', 'Ireland', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(103, '马恩岛', 'Isle of Man', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(104, '以色列', 'Israel', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(105, '意大利', 'Italy', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(106, '牙买加', 'Jamaica', '2020-02-28 02:51:42', '2020-02-28 02:51:42'),
(107, '日本', 'Japan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(108, '澤西', 'Jersey', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(109, '约旦', 'Jordan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(110, '哈萨克斯坦', 'Kazakhstan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(111, '肯尼亚', 'Kenya', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(112, '基里巴斯', 'Kiribati', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(113, '科索沃', 'Kosovo', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(114, '科威特', 'Kuwait', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(115, '吉尔吉斯斯坦', 'Kyrgyzstan', '2020-02-28 02:51:43', '2020-02-28 02:51:43'),
(116, '老挝', 'Laos', '2020-02-28 02:51:44', '2020-02-28 02:51:44'),
(117, '拉脫維亞', 'Latvia', '2020-02-28 02:51:44', '2020-02-28 02:51:44'),
(118, '黎巴嫩', 'Lebanon', '2020-02-28 02:51:45', '2020-02-28 02:51:45'),
(119, '賴索托', 'Lesotho', '2020-02-28 02:51:45', '2020-02-28 02:51:45'),
(120, '利比里亚', 'Liberia', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(121, '利比亞', 'Libya', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(122, '列支敦斯登', 'Liechtenstein', '2020-02-28 02:51:46', '2020-02-28 02:51:46'),
(123, '立陶宛', 'Lithuania', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(124, '卢甘斯克', 'Luhansk', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(125, '卢森堡', 'Luxembourg', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(126, '澳門', 'Macau', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(127, '马达加斯加', 'Madagascar', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(128, '马拉维', 'Malawi', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(129, '马来西亚', 'Malaysia', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(130, '馬爾地夫', 'Maldives', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(131, '马里', 'Mali', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(132, '馬爾他', 'Malta', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(133, '马绍尔群岛', 'Marshall Islands', '2020-02-28 02:51:47', '2020-02-28 02:51:47'),
(134, '毛里塔尼亚', 'Mauritania', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(135, '模里西斯', 'Mauritius', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(136, '墨西哥', 'Mexico', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(137, '密克羅尼西亞聯邦', 'Micronesia', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(138, '摩尔多瓦', 'Moldova', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(139, '摩納哥', 'Monaco', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(140, '蒙古國', 'Mongolia', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(141, '蒙特內哥羅', 'Montenegro', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(142, '蒙特塞拉特', 'Montserrat', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(143, '摩洛哥', 'Morocco', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(144, '莫桑比克', 'Mozambique', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(145, '緬甸', 'Myanmar', '2020-02-28 02:51:48', '2020-02-28 02:51:48'),
(146, '纳米比亚', 'Namibia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(147, '瑙鲁', 'Nauru', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(148, '尼泊尔', 'Nepal', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(149, '荷蘭', 'Netherlands', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(150, '新喀里多尼亞', 'New Caledonia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(151, '新西蘭', 'New Zealand', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(152, '尼加拉瓜', 'Nicaragua', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(153, '尼日尔', 'Niger', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(154, '奈及利亞', 'Nigeria', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(155, '纽埃', 'Niue', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(156, '朝鲜', 'North Korea', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(157, '北馬其頓', 'North Macedonia', '2020-02-28 02:51:49', '2020-02-28 02:51:49'),
(158, '北塞浦路斯', 'Northern Cyprus', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(159, '北馬里亞納群島', 'Northern Mariana Islands', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(160, '挪威', 'Norway', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(161, '阿曼', 'Oman', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(162, '巴基斯坦', 'Pakistan', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(163, '帛琉', 'Palau', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(164, '巴勒斯坦', 'Palestine', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(165, '巴拿马', 'Panama', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(166, '巴布亚新几内亚', 'Papua New Guinea', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(167, '巴拉圭', 'Paraguay', '2020-02-28 02:51:50', '2020-02-28 02:51:50'),
(168, '秘魯', 'Peru', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(169, '菲律賓', 'Philippines', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(170, '皮特凯恩群岛', 'Pitcairn Islands', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(171, '波蘭', 'Poland', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(172, '葡萄牙', 'Portugal', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(173, '德涅斯特河沿岸', 'Pridnestrovie', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(174, '波多黎各', 'Puerto Rico', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(175, '卡塔尔', 'Qatar', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(176, '羅馬尼亞', 'Romania', '2020-02-28 02:51:51', '2020-02-28 02:51:51'),
(177, '俄羅斯', 'Russia', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(178, '卢旺达', 'Rwanda', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(179, '圣巴泰勒米', 'Saint Barthelemy', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(180, '圣基茨和尼维斯', 'Saint Christopher and Nevis', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(181, '圣赫勒拿、阿森松和特里斯坦-达库尼亚', 'Saint Helena, Ascension and Tristan da Cunha', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(182, '圣卢西亚', 'Saint Lucia', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(183, '圣皮埃尔和密克隆', 'Saint Pierre and Miquelon', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(184, '圣文森特和格林纳丁斯', 'Saint Vincent and the Grenadines', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(185, '萨摩亚', 'Samoa', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(186, '圣马力诺', 'San Marino', '2020-02-28 02:51:52', '2020-02-28 02:51:52'),
(187, '聖多美和普林西比', 'São Tomé and Príncipe\r\n', '2020-02-28 02:51:52', '2020-02-28 03:39:40'),
(188, '沙烏地阿拉伯', 'Saudi Arabia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(189, '塞内加尔', 'Senegal', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(190, '塞爾維亞', 'Serbia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(191, '塞舌尔', 'Seychelles', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(192, '塞拉利昂', 'Sierra Leone', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(193, '新加坡', 'Singapore', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(194, '荷屬聖馬丁', 'Sint Maarten', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(195, '斯洛伐克', 'Slovakia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(196, '斯洛維尼亞', 'Slovenia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(197, '所罗门群岛', 'Solomon Islands', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(198, '索馬利亞', 'Somalia', '2020-02-28 02:51:53', '2020-02-28 02:51:53'),
(199, '索馬利蘭', 'Somaliland', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(200, '南非', 'South Africa', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(201, '韩国', 'South Korea', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(202, '南奥塞梯', 'South Ossetia', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(203, '南蘇丹', 'South Sudan', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(204, '西班牙', 'Spain', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(205, '斯里蘭卡', 'Sri Lanka', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(206, '苏丹', 'Sudan', '2020-02-28 02:51:54', '2020-02-28 02:51:54'),
(207, '苏里南', 'Suriname', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(208, '斯瓦尔巴', 'Svalbard', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(209, '瑞典', 'Sweden', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(210, '瑞士', 'Switzerland', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(211, '叙利亚', 'Syria', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(212, '中華民國', 'Taiwan', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(213, '塔吉克斯坦', 'Tajikistan', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(214, '坦桑尼亚', 'Tanzania', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(215, '泰國', 'Thailand', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(216, '梵蒂冈', 'The Holy See（Vatican City）', '2020-02-28 02:51:55', '2020-02-28 03:40:13'),
(217, '东帝汶', 'Timor-Leste', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(218, '多哥', 'Togo', '2020-02-28 02:51:55', '2020-02-28 02:51:55'),
(219, '托克勞', 'Tokelau', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(220, '汤加', 'Tonga', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(221, '千里達及托巴哥', 'Trinidad and Tobago', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(222, '突尼西亞', 'Tunisia', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(223, '土耳其', 'Turkey', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(224, '土库曼斯坦', 'Turkmenistan', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(225, '特克斯和凯科斯群岛', 'Turks and Caicos Islands', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(226, '图瓦卢', 'Tuvalu', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(227, '乌干达', 'Uganda', '2020-02-28 02:51:56', '2020-02-28 02:51:56'),
(228, '烏克蘭', 'Ukraine', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(229, '阿联酋', 'United Arab Emirates', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(230, '英國', 'United Kingdom', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(231, '美國', 'United States', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(232, '乌拉圭', 'Uruguay', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(233, '乌兹别克斯坦', 'Uzbekistan', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(234, '瓦努阿圖', 'Vanuatu', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(235, '委內瑞拉', 'Venezuela', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(236, '越南', 'Vietnam', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(237, '英屬維爾京群島', 'Virgin Islands, British', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(238, '瓦利斯和富圖納', 'Wallis and Futuna', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(239, '西撒哈拉', 'Western Sahara', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(240, '葉門', 'Yemen', '2020-02-28 02:51:57', '2020-02-28 02:51:57'),
(241, '尚比亞', 'Zambia', '2020-02-28 02:51:58', '2020-02-28 02:51:58'),
(242, '辛巴威', 'Zimbabwe', '2020-02-28 02:51:58', '2020-02-28 02:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `email_verified` bigint(20) DEFAULT 1,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `password_two` char(64) DEFAULT NULL,
  `salt_two` char(64) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `ekyc_update` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `favorite_project` varchar(255) DEFAULT NULL,
  `petri_rating` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `email_verified`, `password`, `salt`, `password_two`, `salt_two`, `phone_no`, `full_name`, `ekyc_update`, `nationality`, `favorite_project`, `petri_rating`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9a04ea5286dc214d621622e14a858dc4', 'admin', 'admin@gmail.com', 1, '2bd3fc76b60d220e67a40adb4c4a9be1d2cdde2afb4a406b6ff1c2daad40ea59', 'a221197fcfb97258210864e1175f32f221cdc16f', NULL, NULL, '6012-3456789', '', NULL, NULL, NULL, NULL, 1, 1, '2020-03-18 06:28:23', '2020-03-30 06:43:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles_one`
--
ALTER TABLE `articles_one`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles_one`
--
ALTER TABLE `articles_one`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
