<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ArticleOne.php';
require_once dirname(__FILE__) . '/../classes/User.php';

// require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["article_uid"]);
    $display_Type = "HIDE";

    //   FOR DEBUGGING
    echo "<br>";
    echo $uid."<br>";
    echo $display_Type."<br>";

    if(isset($_POST['article_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($display_Type)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display_Type);
            $stringType .=  "s";
        }    
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $loginTypeUpdated = updateDynamicData($conn,"articles_one"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        
        if($loginTypeUpdated)
        {
            // echo "success";
            header('Location: ../viewArticles.php?type=1');
        }
        else
        {
            // echo "fail";
            header('Location: ../viewArticles.php?type=2');
        }
    }
    else
    {
        //echo "error";
        header('Location: ../viewArticles.php?type=3');
    }

}
else
{
     header('Location: ../index.php');
}
?>
