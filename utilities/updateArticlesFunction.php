<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ArticleOne.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = $userUid;

     $articleUid = rewrite($_POST['article_uid']);

     $title = rewrite($_POST['update_title']);

     $articleLink = rewrite($_POST['update_article_link']);
     $keywordTwo = rewrite($_POST['update_keyword_two']);
     $author = rewrite($_POST['update_author']);

     $str = str_replace( array(' ',','), '', $title);
     $strkeywordOne = str_replace( array(','), '', $articleLink);
     $AddToEnd = "-";
     $seoTitle = $str.$AddToEnd.$strkeywordOne;
     
     $paragraphOne = rewrite($_POST['update_paragraph_one']);


     $paragraphTwo = rewrite($_POST['update_paragraph_two']);
     $paragraphThree = rewrite($_POST['update_paragraph_three']);

     $delParagraphTwo = rewrite($_POST['delete_paragraph_two']);
     if($delParagraphTwo == "Delete")
     {
          $paragraphTwo = "Delete";
     }
     else
     {
          $paragraphTwo = rewrite($_POST['update_paragraph_two']);
     }

     $delParagraphThree = rewrite($_POST['delete_paragraph_three']);
     if($delParagraphThree == "Delete")
     {
          $paragraphThree = "Delete";
     }
     else
     {
          $paragraphThree = rewrite($_POST['update_paragraph_three']);
     }


     $paragraphFour = rewrite($_POST['update_paragraph_four']);
     $paragraphFive = rewrite($_POST['update_paragraph_five']);

     $paragraphSix = rewrite($_POST['update_paragraph_six']);
     $paragraphSeven = rewrite($_POST['update_paragraph_seven']);
     $paragraphEight = rewrite($_POST['update_paragraph_eight']);
     $paragraphNine = rewrite($_POST['update_paragraph_nine']);
     $paragraphTen = rewrite($_POST['update_paragraph_ten']);

     $imgCoverSrc = rewrite($_POST['update_cover_photo_source']);
     $imgOneSrc = rewrite($_POST['update_image_one_source']);


     $delImgTwoSrc = rewrite($_POST['delete_img_src_two']);
     if($delImgTwoSrc == "Delete")
     {
          $imgTwoSrc = "Delete";
     }
     else
     {
          $imgTwoSrc = rewrite($_POST['update_image_two_source']);
     }

     $delImgThreeSrc = rewrite($_POST['delete_img_src_three']);
     if($delImgThreeSrc == "Delete")
     {
          $imgThreeSrc = "Delete";
     }
     else
     {
          $imgThreeSrc = rewrite($_POST['update_image_three_source']);
     }


     $imgFourSrc = rewrite($_POST['update_image_four_source']);
     $imgFiveSrc = rewrite($_POST['update_image_five_source']);
     $imgSixSrc = rewrite($_POST['update_image_six_source']);
     $imgSevenSrc = rewrite($_POST['update_image_seven_source']);
     $imgEightSrc = rewrite($_POST['update_image_eight_source']);
     $imgNineSrc = rewrite($_POST['update_image_nine_source']);
     $imgTenSrc = rewrite($_POST['update_image_ten_source']);

     $type = rewrite($_POST['update_type']);

     $coverPhoto = $_FILES['update_cover_photo']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["update_cover_photo"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['update_cover_photo']['tmp_name'],$target_dir.$coverPhoto);
     }

     $imageOne = $_FILES['update_image_one']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["update_image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['update_image_one']['tmp_name'],$target_dir.$imageOne);
     }

     $delImgTwo = rewrite($_POST['delete_img_two']);

     if($delImgTwo == "Delete")
     {
          $imageTwo = "Delete";
     }
     else
     {

          $imageTwo = $_FILES['update_image_two']['name'];
          $target_dir = "../uploads/";
          $target_file = $target_dir . basename($_FILES["update_image_two"]["name"]);
          // Select file type
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Valid file extensions
          $extensions_arr = array("jpg","jpeg","png","gif");
          if( in_array($imageFileType,$extensions_arr) )
          {
               move_uploaded_file($_FILES['update_image_two']['tmp_name'],$target_dir.$imageTwo);
          }

     }

     $delImgThree = rewrite($_POST['delete_img_three']);

     if($delImgThree == "Delete")
     {
          $imageThree = "Delete";
     }
     else
     {

          $imageThree = $_FILES['update_image_three']['name'];
          $target_dir = "../uploads/";
          $target_file = $target_dir . basename($_FILES["update_image_three"]["name"]);
          // Select file type
          $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
          // Valid file extensions
          $extensions_arr = array("jpg","jpeg","png","gif");
          if( in_array($imageFileType,$extensions_arr) )
          {
               move_uploaded_file($_FILES['update_image_three']['tmp_name'],$target_dir.$imageThree);
          }

     }

     $imageFour = $_FILES['update_image_four']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["update_image_four"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['update_image_four']['tmp_name'],$target_dir.$imageFour);
     }

     $imageFive = $_FILES['update_image_five']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["update_image_five"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['update_image_five']['tmp_name'],$target_dir.$imageFive);
     }

     $imageSix = $_FILES['update_image_six']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["update_image_six"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['update_image_six']['tmp_name'],$target_dir.$imageSix);
     }

     $imageSeven = $_FILES['update_image_seven']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["update_image_seven"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['update_image_seven']['tmp_name'],$target_dir.$imageSeven);
     }

     $imageEight = $_FILES['update_image_eight']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["update_image_eight"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['update_image_eight']['tmp_name'],$target_dir.$imageEight);
     }

     $imageNine = $_FILES['update_image_nine']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["update_image_nine"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['update_image_nine']['tmp_name'],$target_dir.$imageNine);
     }

     $imageTen = $_FILES['update_image_ten']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["update_image_ten"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['update_image_ten']['tmp_name'],$target_dir.$imageTen);
     }
     
     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $country."<br>";
     // echo $firstname."<br>";
     // echo $lastname."<br>";
     // echo $prooftype."<br>";

     if(isset($_POST['submit']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          // //echo "save to database";
          if($title)
          {
               array_push($tableName,"title");
               array_push($tableValue,$title);
               $stringType .=  "s";
          }

          if($articleLink)
          {
               array_push($tableName,"article_link");
               array_push($tableValue,$articleLink);
               $stringType .=  "s";
          }
          if($keywordTwo)
          {
               array_push($tableName,"keyword_two");
               array_push($tableValue,$keywordTwo);
               $stringType .=  "s";
          }
          if($author)
          {
               array_push($tableName,"author");
               array_push($tableValue,$author);
               $stringType .=  "s";
          }
          if($seoTitle)
          {
               array_push($tableName,"seo_title");
               array_push($tableValue,$seoTitle);
               $stringType .=  "s";
          }

          if($paragraphOne)
          {
               array_push($tableName,"paragraph_one");
               array_push($tableValue,$paragraphOne);
               $stringType .=  "s";
          }
          if($paragraphTwo)
          {
               array_push($tableName,"paragraph_two");
               array_push($tableValue,$paragraphTwo);
               $stringType .=  "s";
          }
          if($paragraphThree)
          {
               array_push($tableName,"paragraph_three");
               array_push($tableValue,$paragraphThree);
               $stringType .=  "s";
          }
          if($paragraphFour)
          {
               array_push($tableName,"paragraph_four");
               array_push($tableValue,$paragraphFour);
               $stringType .=  "s";
          }
          if($paragraphFive)
          {
               array_push($tableName,"paragraph_five");
               array_push($tableValue,$paragraphFive);
               $stringType .=  "s";
          }
          if($type)
          {
               array_push($tableName,"type");
               array_push($tableValue,$type);
               $stringType .=  "s";
          }
          if($coverPhoto)
          {
               array_push($tableName,"title_cover");
               array_push($tableValue,$coverPhoto);
               $stringType .=  "s";
          }
          if($imageOne)
          {
               array_push($tableName,"image_one");
               array_push($tableValue,$imageOne);
               $stringType .=  "s";
          }
          if($imageTwo)
          {
               array_push($tableName,"image_two");
               array_push($tableValue,$imageTwo);
               $stringType .=  "s";
          }
          if($imageThree)
          {
               array_push($tableName,"image_three");
               array_push($tableValue,$imageThree);
               $stringType .=  "s";
          }
          if($imageFour)
          {
               array_push($tableName,"image_four");
               array_push($tableValue,$imageFour);
               $stringType .=  "s";
          }
          if($imageFive)
          {
               array_push($tableName,"image_five");
               array_push($tableValue,$imageFive);
               $stringType .=  "s";
          }
          
          if($paragraphSix)
          {
               array_push($tableName,"paragraph_six");
               array_push($tableValue,$paragraphSix);
               $stringType .=  "s";
          }
          if($paragraphSeven)
          {
               array_push($tableName,"paragraph_seven");
               array_push($tableValue,$paragraphSeven);
               $stringType .=  "s";
          }
          if($paragraphEight)
          {
               array_push($tableName,"paragraph_eight");
               array_push($tableValue,$paragraphEight);
               $stringType .=  "s";
          }
          if($paragraphNine)
          {
               array_push($tableName,"paragraph_nine");
               array_push($tableValue,$paragraphNine);
               $stringType .=  "s";
          }
          if($paragraphTen)
          {
               array_push($tableName,"paragraph_ten");
               array_push($tableValue,$paragraphTen);
               $stringType .=  "s";
          }
          if($imageSix)
          {
               array_push($tableName,"image_six");
               array_push($tableValue,$imageSix);
               $stringType .=  "s";
          }
          if($imageSeven)
          {
               array_push($tableName,"image_seven");
               array_push($tableValue,$imageSeven);
               $stringType .=  "s";
          }
          if($imageEight)
          {
               array_push($tableName,"image_eight");
               array_push($tableValue,$imageEight);
               $stringType .=  "s";
          }
          if($imageNine)
          {
               array_push($tableName,"image_nine");
               array_push($tableValue,$imageNine);
               $stringType .=  "s";
          }
          if($imageTen)
          {
               array_push($tableName,"image_ten");
               array_push($tableValue,$imageTen);
               $stringType .=  "s";
          }

          if($imgCoverSrc)
          {
               array_push($tableName,"img_cover_source");
               array_push($tableValue,$imgCoverSrc);
               $stringType .=  "s";
          }
          if($imgOneSrc)
          {
               array_push($tableName,"img_one_source");
               array_push($tableValue,$imgOneSrc);
               $stringType .=  "s";
          }
          if($imgTwoSrc)
          {
               array_push($tableName,"img_two_source");
               array_push($tableValue,$imgTwoSrc);
               $stringType .=  "s";
          }
          if($imgThreeSrc)
          {
               array_push($tableName,"img_three_source");
               array_push($tableValue,$imgThreeSrc);
               $stringType .=  "s";
          }
          if($imgFourSrc)
          {
               array_push($tableName,"img_four_source");
               array_push($tableValue,$imgFourSrc);
               $stringType .=  "s";
          }if($imgFiveSrc)
          {
               array_push($tableName,"img_five_source");
               array_push($tableValue,$imgFiveSrc);
               $stringType .=  "s";
          }
          if($imgSixSrc)
          {
               array_push($tableName,"img_six_source");
               array_push($tableValue,$imgSixSrc);
               $stringType .=  "s";
          }
          if($imgSevenSrc)
          {
               array_push($tableName,"img_seven_source");
               array_push($tableValue,$imgSevenSrc);
               $stringType .=  "s";
          }
          if($imgEightSrc)
          {
               array_push($tableName,"img_eight_source");
               array_push($tableValue,$imgEightSrc);
               $stringType .=  "s";
          }
          if($imgNineSrc)
          {
               array_push($tableName,"img_nine_source");
               array_push($tableValue,$imgNineSrc);
               $stringType .=  "s";
          }
          if($imgTenSrc)
          {
               array_push($tableName,"img_ten_source");
               array_push($tableValue,$imgTenSrc);
               $stringType .=  "s";
          }

          array_push($tableValue,$articleUid);
          $stringType .=  "s";
          $updateArticles = updateDynamicData($conn,"articles_one"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateArticles)
          {
               // echo "<script>alert('Data Updated !');window.location='../member.php'</script>"; 
               // echo "data updated !!";
               // header('Location: ../viewArticles.php?type=1');

               if($type == 'Beauty')
               {
                    header('Location: ../beautyCare.php');
               }
               elseif($type == 'Fashion')
               {
                    header('Location: ../trendyFashion.php');
               }
               elseif($type == 'Social')
               {
                    header('Location: ../socialNews.php');
               }
               else
               {
                    echo "error";
               }

          }
          else
          {    
               // $_SESSION['messageType'] = 1;
               // header('Location: ../kycVerification.php?type=2');
               // echo "<script>alert('unable to update data !');window.location='../userEditEkycDetails.php'</script>";   
               // echo "fail to update data !!";   
               header('Location: ../viewArticles.php?type=2');
          }
     }
     else
     {
          // $_SESSION['messageType'] = 1;
          // header('Location: ../kycVerification.php?type=3');
          // echo "<script>alert('error');window.location='../userEditEkycDetails.php'</script>";  
          // echo "server error";   
          header('Location: ../viewArticles.php?type=3');
     }
  
}
else 
{
     header('Location: ../index.php');
}
?>