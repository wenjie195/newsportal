<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/ArticleOne.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

// $timestamp = time();

function uploadNewArticles($conn,$newsUid,$authorUid,$authorName,$title,$seoTitle,$articleLink,$titleCover,$keywordOne,$keywordTwo,$paragraphOne,$imageOne,$paragraphTwo,$imageTwo,
                              $paragraphThree,$imageThree,$paragraphFour,$imageFour,$paragraphFive,$imageFive,$paragraphSix,$paragraphSeven,$paragraphEight,$paragraphNine,
                                   $paragraphTen,$imageSix,$imageSeven,$imageEight,$imageNine,$imageTen,$imgCoverSrc,$imgOneSrc,$imgTwoSrc,$imgThreeSrc,$imgFourSrc,$imgFiveSrc,
                                        $imgSixSrc,$imgSevenSrc,$imgEightSrc,$imgNineSrc,$imgTenSrc,$headerTwo,$headerThree,$videoTwo,$videoThree,$vdSrcTwo,$vdSrcThree,$author,$type)
{
     if(insertDynamicData($conn,"articles_one",array("uid","author_uid","author_name","title","seo_title","article_link","title_cover","keyword_one","keyword_two","paragraph_one","image_one",
                              "paragraph_two","image_two","paragraph_three","image_three","paragraph_four","image_four","paragraph_five","image_five","paragraph_six","paragraph_seven",
                                   "paragraph_eight","paragraph_nine","paragraph_ten","image_six","image_seven","image_eight","image_nine","image_ten","img_cover_source","img_one_source","img_two_source",
                                        "img_three_source","img_four_source","img_five_source","img_six_source","img_seven_source","img_eight_source","img_nine_source","img_ten_source",
                                             "header_two","header_three","video_two","video_three","vd_src_two","vd_src_three","author","type"),
          array($newsUid,$authorUid,$authorName,$title,$seoTitle,$articleLink,$titleCover,$keywordOne,$keywordTwo,$paragraphOne,$imageOne,$paragraphTwo,$imageTwo,
          $paragraphThree,$imageThree,$paragraphFour,$imageFour,$paragraphFive,$imageFive,$paragraphSix,$paragraphSeven,$paragraphEight,$paragraphNine,
               $paragraphTen,$imageSix,$imageSeven,$imageEight,$imageNine,$imageTen,$imgCoverSrc,$imgOneSrc,$imgTwoSrc,$imgThreeSrc,$imgFourSrc,$imgFiveSrc,
                    $imgSixSrc,$imgSevenSrc,$imgEightSrc,$imgNineSrc,$imgTenSrc,$headerTwo,$headerThree,$videoTwo,$videoThree,$vdSrcTwo,$vdSrcThree,$author,$type),
                    "ssssssssssssssssssssssssssssssssssssssssssssssss") === null)
     {
          header('Location: ../userUploadArticles.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $newsUid = md5(uniqid());

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $authorUid = $userDetails[0]->getUid();
     $authorName = $userDetails[0]->getUsername();

     // $authorUid = "123456789";
     // $authorName = "Oliver Queen";

     $title = rewrite($_POST['title']);
     // $seo = rewrite($_POST['keyword']);

     // $keywordOne = rewrite($_POST['keyword_one']);
     $keywordTwo = rewrite($_POST['keyword_two']);

     $articleLink = rewrite($_POST['article_link']);
     $author = rewrite($_POST['author_name']);

     // $seoTitle = rewrite($_POST['keyword']);
     // $str = str_replace( array(' ',','), '', $title);
     // $AddToEnd = "-";
     // $seoTitle = $str.$AddToEnd.$keywordOne.$AddToEnd.$keywordTwo;

     $str = str_replace( array(' ',','), '', $title);
     $strkeywordOne = str_replace( array(','), '', $articleLink);
     $AddToEnd = "-";
     $seoTitle = $str.$AddToEnd.$strkeywordOne;
     // $seoTitle = $str.$AddToEnd.$strkeywordOne.$AddToEnd.$strkeywordTwo;

     // $AddToEnd = "-";
     // $seoTitle = $strkeywordOne.$AddToEnd.$strkeywordTwo;

     $paragraphOne = rewrite($_POST['paragraph_one']);
     $paragraphTwo = rewrite($_POST['paragraph_two']);

     $paragraphThree = rewrite($_POST['paragraph_three']);
     $paragraphFour = rewrite($_POST['paragraph_four']);
     $paragraphFive = rewrite($_POST['paragraph_five']);

     $paragraphSix = rewrite($_POST['paragraph_six']);
     $paragraphSeven = rewrite($_POST['paragraph_seven']);
     $paragraphEight = rewrite($_POST['paragraph_eight']);
     $paragraphNine = rewrite($_POST['paragraph_nine']);
     $paragraphTen = rewrite($_POST['paragraph_ten']);

     $imgCoverSrc = rewrite($_POST['cover_photo_source']);
     $imgOneSrc = rewrite($_POST['image_one_source']);
     $imgTwoSrc = rewrite($_POST['image_two_source']);
     $imgThreeSrc = rewrite($_POST['image_three_source']);
     $imgFourSrc = rewrite($_POST['image_four_source']);
     $imgFiveSrc = rewrite($_POST['image_five_source']);
     $imgSixSrc = rewrite($_POST['image_six_source']);
     $imgSevenSrc = rewrite($_POST['image_seven_source']);
     $imgEightSrc = rewrite($_POST['image_eight_source']);
     $imgNineSrc = rewrite($_POST['image_nine_source']);
     $imgTenSrc = rewrite($_POST['image_ten_source']);

     $type = rewrite($_POST['type']);
     
     // $imageOne = rewrite($_POST['register_email_user']);

     $titleCover = $_FILES['cover_photo']['name'];
     // $titleCover = $timestamp.$_FILES['cover_photo']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["cover_photo"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['cover_photo']['tmp_name'],$target_dir.$titleCover);
     }

     $imageOne = $_FILES['image_one']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     $imageTwo = $_FILES['image_two']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_two"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_two']['tmp_name'],$target_dir.$imageTwo);
     }

     $headerTwo = rewrite($_POST['header_two']);
     $vdSrcTwo = rewrite($_POST['vd_src_two']);

     $videoTwo = $_FILES['video_two']['name'];
     $target_dir = "../videos/";
     $target_file = $target_dir . basename($_FILES["video_two"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("mp4","avi","3gp","mov","mpeg");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['video_two']['tmp_name'],$target_dir.$videoTwo);
     }

     $imageThree = $_FILES['image_three']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_three"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_three']['tmp_name'],$target_dir.$imageThree);
     }

     $headerThree = rewrite($_POST['header_three']);
     $vdSrcThree = rewrite($_POST['vd_src_three']);

     $videoThree = $_FILES['video_three']['name'];
     $target_dir = "../videos/";
     $target_file = $target_dir . basename($_FILES["video_three"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("mp4","avi","3gp","mov","mpeg");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['video_three']['tmp_name'],$target_dir.$videoThree);
     }

     $imageFour = $_FILES['image_four']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_four"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_four']['tmp_name'],$target_dir.$imageFour);
     }

     $imageFive = $_FILES['image_five']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_five"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_five']['tmp_name'],$target_dir.$imageFive);
     }

     $imageSix = $_FILES['image_six']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_six"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_six']['tmp_name'],$target_dir.$imageSix);
     }

     $imageSeven = $_FILES['image_seven']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_seven"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_seven']['tmp_name'],$target_dir.$imageSeven);
     }

     $imageEight = $_FILES['image_eight']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_eight"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_eight']['tmp_name'],$target_dir.$imageEight);
     }

     $imageNine = $_FILES['image_nine']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_nine"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_nine']['tmp_name'],$target_dir.$imageNine);
     }

     $imageTen = $_FILES['image_ten']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_ten"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_ten']['tmp_name'],$target_dir.$imageTen);
     }

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $newsUid."<br>";
     // echo $authorUid."<br>";
     // echo $authorName."<br>";
     // echo $title."<br>";
     // echo $seoTitle."<br>";
     // echo $paragraphOne."<br>";
     // echo $paragraphTwo."<br>";
     // echo $type."<br>";

     if(uploadNewArticles($conn,$newsUid,$authorUid,$authorName,$title,$seoTitle,$articleLink,$titleCover,$keywordOne,$keywordTwo,$paragraphOne,$imageOne,$paragraphTwo,$imageTwo,
     $paragraphThree,$imageThree,$paragraphFour,$imageFour,$paragraphFive,$imageFive,$paragraphSix,$paragraphSeven,$paragraphEight,$paragraphNine,
          $paragraphTen,$imageSix,$imageSeven,$imageEight,$imageNine,$imageTen,$imgCoverSrc,$imgOneSrc,$imgTwoSrc,$imgThreeSrc,$imgFourSrc,$imgFiveSrc,
               $imgSixSrc,$imgSevenSrc,$imgEightSrc,$imgNineSrc,$imgTenSrc,$headerTwo,$headerThree,$videoTwo,$videoThree,$vdSrcTwo,$vdSrcThree,$author,$type))
     {
          // echo "articles upload successfully";
          // echo "<script>alert('Register Success !');window.location='../addReferee.php'</script>";    

          if($type == 'Beauty')
          {
               header('Location: ../beautyCare.php');
          }
          elseif($type == 'Fashion')
          {
               header('Location: ../trendyFashion.php');
          }
          elseif($type == 'Social')
          {
               header('Location: ../socialNews.php');
          }
          else
          {
               echo "error";
          }

     }
     else
     {
          echo "fail to upload article";
          // echo "<script>alert('error registering new account.The account already exist');window.location='../addReferee.php'</script>";
     }
  
}
else 
{
     header('Location: ../index.php');
}

?>