<?php
if (session_id() == "")
{
     session_start();
}

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phone)
{
     if(insertDynamicData($conn,"user",array("uid","username","email","password","salt","phone_no"),
          array($uid,$username,$email,$finalPassword,$salt,$phone),"ssssss") === null)
     {
          header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['register_username']);
     $phone = rewrite($_POST['register_phone']);
     $email = rewrite($_POST['register_email']);

     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);

     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING 
     echo "<br>";
     echo $uid."<br>";
     echo $username."<br>";
     echo $email."<br>";
     echo $phone."<br>";
     echo $register_password."<br>";
     echo $register_retype_password."<br>";
     echo $password."<br>";
     echo $salt."<br>";
     echo $finalPassword."<br>";

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {
               
               $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($_POST['register_username']),"s");
               $usernameDetails = $usernameRows[0];

               $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email']),"s");
               $userEmailDetails = $userEmailRows[0];

               $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
               $userPhoneDetails = $userPhoneRows[0];

               if (!$usernameDetails && !$userEmailDetails && !$userPhoneDetails)
               {

                    if(registerNewUser($conn,$uid,$username,$email,$finalPassword,$salt,$phone))
                    {
                         echo "register successfully";
                    }
                    else
                    { 
                         // echo "fail to register via upline";
                    }
               }
               else
               {
                    // $_SESSION['messageType'] = 2;
                    // header('Location: ../index.php?type=4');
                    echo "input data for register already taken by other user";
                    //echo "<script>alert('input data for register already taken by other user');window.location='../index.php'</script>";    
               }
          }
          else 
          {
               // $_SESSION['messageType'] = 2;
               // header('Location: ../index.php?type=5');
               echo "password length must be more than 6";
               // echo "<script>alert('password length must be more than 6');window.location='../index.php'</script>";    
          }
     }
     else 
     {
          // $_SESSION['messageType'] = 2;
          // header('Location: ../index.php?type=6');
          echo "password and retype password not the same";
          //echo "<script>alert('password and retype password not the same');window.location='../index.php'</script>";    
     }      
}
else 
{
     header('Location: ../index.php');
}
?>