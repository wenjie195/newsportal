<?php
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ArticleOne.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $articles = getArticlesOne($conn,"WHERE seo_title = ? AND display = 'YES' ",array("seo_title"),array($_GET['id']), "s");
$articles = getArticlesOne($conn,"WHERE article_link = ? AND display = 'YES' ",array("article_link"),array($_GET['id']), "s");
$articlesTitle = $articles[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!-- <meta property="og:image" content="https:/tevy.asia/img/fb-meta.jpg" /> -->
<meta property="og:image" content="https://tevy.asia/uploads/<?php echo $articlesTitle->getTitleCover();?>" />
<meta property="og:title" content="<?php echo $articlesTitle->getTitle();?> | Tevy" />
<meta property="og:description" content="<?php echo $articlesTitle->getParagraphOne();?>" />
<meta name="description" content="<?php echo $articlesTitle->getParagraphOne();?>" />
<meta name="keywords" content="<?php echo $articlesTitle->getKeywordTwo();?>">
<!--<link rel="canonical" href="https://chillitbuddy.com/" />-->
<link rel="canonical" href="https://tevy.asia/article.php?id=<?php echo $articlesTitle->getArticleLink();?>" />
<title><?php echo $articlesTitle->getTitle();?> | Tevy</title>

<?php include 'css.php'; ?>
</head>

<?php include 'header-after-login.php'; ?>

<?php 
        // Program to display URL of current page. 
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
        $link = "https"; 
        else
        $link = "http"; 

        // Here append the common URL characters. 
        $link .= "://"; 

        // Append the host(domain name, ip) to the URL. 
        $link .= $_SERVER['HTTP_HOST']; 

        // Append the requested resource location to the URL 
        $link .= $_SERVER['REQUEST_URI']; 

        // Print the link 
        // echo $link; 
        ?> 

        <?php
        $str1 = $link;
        // $referrerUidLink = str_replace( 'http://localhost/newsportal/article.php?id=', '', $str1);
        $referrerUidLink = str_replace( 'http://tevy.asia/article.php?id=', '', $str1);
        //seo title
        // echo $referrerUidLink;
?>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
// $articlesDetails = getArticlesOne($conn,"WHERE seo_title = ? ", array("seo_title") ,array($_GET['id']),"s");
$articlesDetails = getArticlesOne($conn,"WHERE article_link = ? ", array("article_link") ,array($_GET['id']),"s");
?>

  <?php
  if($articlesDetails)
  {
  for($cnt = 0;$cnt < count($articlesDetails) ;$cnt++)
  {
  ?>

<div class="background-div">
    <div class="cover-gap content min-height2 blog-content-div1">

  <div class="print-area" id="printarea">







    <div class="clear"></div>

    <h1 class="red-h1 slab darkpink-text"><?php echo $articlesTitle->getTitle();?></h1>

    <div class="left-contributor-div">
      <!--<p class="contributor-p">by <span class="pink-span"><?php echo $articlesDetails[$cnt]->getAuthorName();?></span>, Contributor</p>-->
      <!-- craft the category link here -->
      <p class="contributor-p"><a href="#" class="orange-p"><?php echo $articlesDetails[$cnt]->getType();?></a></p>
      <p class="blue-date"><?php echo $articlesDetails[$cnt]->getDateCreated();?></p>
    </div>
	<img src="uploads/<?php echo $articlesDetails[$cnt]->getTitleCover();;?>" class="width100 cover-photo" alt="<?php echo $articlesTitle->getTitle();?>" title="<?php echo $articlesTitle->getTitle();?>">
    <!-- <p class="source-p"><?php //echo _ARTICLE_SOURCE ?>: <?php //echo $articlesDetails[$cnt]->getImgCoverSrc();?></p> -->
    <?php $imageCoverSrc = $articlesDetails[$cnt]->getImgCoverSrc();
    if($imageCoverSrc !=  '')
    {
    ?>
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getImgCoverSrc();?></p>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    

  <div class="test">
    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphOne();?></p>
    <?php $image1 = $articlesDetails[$cnt]->getImageOne();
    if($image1 !=  '')
    {
    ?>
        <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageOne();?>" class="width100 article-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>">
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>
    <!-- <p class="source-p"><?php //echo _ARTICLE_SOURCE ?>: <?php //echo $articlesDetails[$cnt]->getImgOneSrc();?></p> -->
    <?php $image1Src = $articlesDetails[$cnt]->getImgOneSrc();
    if($image1Src !=  '')
    {
    ?>
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getImgOneSrc();?></p>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>
	<p class="header-p"><?php echo $articlesDetails[$cnt]->getHeaderTwo();?></p>
    <?php $paragraph2 = $articlesDetails[$cnt]->getParagraphTwo();
    if($paragraph2 !=  'Delete')
    {
    ?>
        <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphTwo();?></p>
    <?php
    }
    elseif($paragraph2 ==  'Delete')
    {
    ?>
    <?php
    }
    ?>

    <?php $image2 = $articlesDetails[$cnt]->getImageTwo();
    if($image2 !=  'Delete')
    {
    ?>
        <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageTwo();?>" class="width100 article-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>">
    <?php
    }
    elseif($image2 ==  'Delete')
    {
    ?>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    <?php $image2Src = $articlesDetails[$cnt]->getImgTwoSrc();
    if($image2Src !=  'Delete')
    {
    ?>
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getImgTwoSrc();?></p>
    <?php
    }
    elseif($image2Src ==  'Delete')
    {
    ?>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    <?php $vd2Src = $articlesDetails[$cnt]->getVdSrcTwo();
    if($vd2Src !=  'Delete')
    {
    ?>
       
        <p class="video-p">
            <video controls class="video-css">
                <source src="videos/<?php echo $articlesDetails[$cnt]->getVideoTwo();?>" type="video/mp4">
                Your browser does not support HTML5 video.
            </video>
        </p> 
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getVdSrcTwo();?></p>
    <?php
    }
    elseif($vd2Src ==  'Delete')
    {
    ?>
    <?php
    }
    // else
    elseif($vd2Src ==  '')
    {
    ?>
    <?php
    }
    ?>
    <p class="header-p"><?php echo $articlesDetails[$cnt]->getHeaderThree();?></p>
    <?php $paragraph3 = $articlesDetails[$cnt]->getParagraphThree();
    if($paragraph3 !=  'Delete')
    {
    ?>
        <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphThree();?></p>
    <?php
    }
    elseif($paragraph3 ==  'Delete')
    {
    ?>
    <?php
    }
    ?>

    <?php $image3 = $articlesDetails[$cnt]->getImageThree();
    if($image3 !=  'Delete')
    {
    ?>
        <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageThree();?>" class="width100 article-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>">
    <?php
    }
    elseif($image3 ==  'Delete')
    {
    ?>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    <?php $image3Src = $articlesDetails[$cnt]->getImgThreeSrc();
    if($image3Src !=  'Delete')
    {
    ?>
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getImgThreeSrc();?></p>
    <?php
    }
    elseif($image3Src ==  'Delete')
    {
    ?>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    <?php $vd3Src = $articlesDetails[$cnt]->getVdSrcThree();
    if($vd3Src !=  'Delete')
    {
    ?>
 
        <p class="video-p">
            <video controls class="video-css">
                <source src="videos/<?php echo $articlesDetails[$cnt]->getVideoThree();?>" type="video/mp4">
                Your browser does not support HTML5 video.
            </video>
        </p> 
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getVdSrcThree();?></p>
    <?php
    }
    elseif($vd3Src ==  'Delete')
    {
    ?>
    <?php
    }
    // else
    elseif($vd3Src ==  '')
    {
    ?>
    <?php
    }
    ?>

    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphFour();?></p>
    <?php $image4 = $articlesDetails[$cnt]->getImageFour();
    if($image4 !=  '')
    {
    ?>
        <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageFour();?>" class="width100 article-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>">
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>
    <!-- <p class="source-p"><?php// echo _ARTICLE_SOURCE ?>: <?php //echo $articlesDetails[$cnt]->getImgFourSrc();?></p> -->
    <?php $image4Src = $articlesDetails[$cnt]->getImgFourSrc();
    if($image4Src !=  '')
    {
    ?>
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getImgFourSrc();?></p>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphFive();?></p>
    <?php $image5 = $articlesDetails[$cnt]->getImageFive();
    if($image5 !=  '')
    {
    ?>
        <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageFive();?>" class="width100 article-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>">
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>
    <!-- <p class="source-p"><?php //echo _ARTICLE_SOURCE ?>: <?php //echo $articlesDetails[$cnt]->getImgFiveSrc();?></p> -->
    <?php $image5Src = $articlesDetails[$cnt]->getImgFiveSrc();
    if($image5Src !=  '')
    {
    ?>
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getImgFiveSrc();?></p>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphSix();?></p>
    <?php $image6 = $articlesDetails[$cnt]->getImageSix();
    if($image6 !=  '')
    {
    ?>
        <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageSix();?>" class="width100 article-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>">
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>
    <!-- <p class="source-p"><?php //echo _ARTICLE_SOURCE ?>: <?php //echo $articlesDetails[$cnt]->getImgSixSrc();?></p> -->
    <?php $image6Src = $articlesDetails[$cnt]->getImgSixSrc();
    if($image6Src !=  '')
    {
    ?>
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getImgSixSrc();?></p>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphSeven();?></p>
    <?php $image7 = $articlesDetails[$cnt]->getImageSeven();
    if($image7 !=  '')
    {
    ?>
        <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageSeven();?>" class="width100 article-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>">
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>
    <!-- <p class="source-p"><?php //echo _ARTICLE_SOURCE ?>: <?php //echo $articlesDetails[$cnt]->getImgSevenSrc();?></p> -->
    <?php $image7Src = $articlesDetails[$cnt]->getImgSevenSrc();
    if($image7Src !=  '')
    {
    ?>
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getImgSevenSrc();?></p>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphEight();?></p>
    <?php $image8 = $articlesDetails[$cnt]->getImageEight();
    if($image8 !=  '')
    {
    ?>
        <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageEight();?>" class="width100 article-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>">
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>
    <!-- <p class="source-p"><?php //echo _ARTICLE_SOURCE ?>: <?php //echo $articlesDetails[$cnt]->getImgEightSrc();?></p> -->
    <?php $image8Src = $articlesDetails[$cnt]->getImgEightSrc();
    if($image8Src !=  '')
    {
    ?>
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getImgEightSrc();?></p>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphNine();?></p>
    <?php $image9 = $articlesDetails[$cnt]->getImageNine();
    if($image9 !=  '')
    {
    ?>
        <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageNine();?>" class="width100 article-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>">
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>
    <!-- <p class="source-p"><?php //echo _ARTICLE_SOURCE ?>: <?php //echo $articlesDetails[$cnt]->getImgNineSrc();?></p> -->
    <?php $image9Src = $articlesDetails[$cnt]->getImgNineSrc();
    if($image9Src !=  '')
    {
    ?>
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getImgNineSrc();?></p>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    <p class="article-p"><?php echo $articlesDetails[$cnt]->getParagraphTen();?></p>
    <?php $image10 = $articlesDetails[$cnt]->getImageTen();
    if($image10 !=  '')
    {
    ?>
        <img src="uploads/<?php echo $articlesDetails[$cnt]->getImageTen();?>" class="width100 article-photo" alt="<?php echo $articlesDetails[$cnt]->getTitle();?>" title="<?php echo $articlesDetails[$cnt]->getTitle();?>">
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>
    <!-- <p class="source-p"><?php //echo _ARTICLE_SOURCE ?>: <?php //echo $articlesDetails[$cnt]->getImgTenSrc();?></p> -->
    <?php $image10Src = $articlesDetails[$cnt]->getImgTenSrc();
    if($image10Src !=  '')
    {
    ?>
        <p class="source-p"><?php echo _ARTICLE_SOURCE ?>: <?php echo $articlesDetails[$cnt]->getImgTenSrc();?></p>
    <?php
    }
    else
    {
    ?>
    <?php
    }
    ?>

    <p class="article-p"><b>Author : <?php echo $articlesDetails[$cnt]->getAuthor();?></b> <img src="img/feather.png" class="feather-png" alt="Author" title="Author"></p>

  </div>

  </div>
  <div class="hide-print">
      <script async src="https://static.addtoany.com/menu/page.js"></script>
    <!-- AddToAny END -->
    <div class="clear"></div>
  <div class="width100 overflow same-padding">
              <!-- AddToAny BEGIN -->
        <div class="border-div"></div>
        <h3 class="darkpink-text share-h3"><?php echo _ARTICLE_SHARE ?>:</h3>
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
        <a class="a2a_button_copy_link"></a>
        <a class="a2a_button_facebook"></a>
        <a class="a2a_button_twitter"></a>
        <a class="a2a_button_linkedin"></a>
        <a class="a2a_button_blogger"></a>
        <a class="a2a_button_facebook_messenger"></a>
        <a class="a2a_button_whatsapp"></a>
        <a class="a2a_button_wechat"></a>
        <a class="a2a_button_line"></a>
        <a class="a2a_button_telegram"></a>
        <!--<a class="a2a_button_print"></a>-->
        </div> 
  </div>
  <div class="width100 overflow same-padding">
  	<div class="border-div"></div>

<div id="disqus_thread"></div>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://tevy.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>    
<script id="dsq-count-scr" src="https://tevy.disqus.com/count.js" async></script>    
                        
                                
                                
                                
 <a href="https://tevy.asia/article.php?id=<?php echo $articlesDetails[$cnt]->getArticleLink();;?>#disqus_thread" class="live-hide"></a>
    
    </div>
    <div class="width100 overflow same-padding">
        <div class="border-div"></div>
        <h3 class="darkpink-text share-h3"><?php echo _ARTICLE_RECOMMENDED ?>:</h3>


        <?php
        $currentUid = $articles[$cnt]->getUid();
        $currentType = $articles[$cnt]->getType();
        $allArticles = getArticlesOne($conn," WHERE uid != '$currentUid' AND type = '$currentType' AND display = 'YES' ORDER BY date_created DESC LIMIT 3");

        if($allArticles)
        {   
            for($cntAA = 0;$cntAA < count($allArticles) ;$cntAA++)
            {
            ?>
            
            <a href='article.php?id=<?php echo $allArticles[$cntAA]->getArticleLink();?>'>
                    <div class="article-card article-card-overwrite recommended">

                        <div class="article-bg-img-box">
                            <img src="uploads/<?php echo $allArticles[$cntAA]->getTitleCover();?>" class="article-img1" alt="<?php echo $allArticles[$cntAA]->getTitle();?>" title="<?php echo $allArticles[$cntAA]->getTitle();?>">
                        </div>

                        <div class="box-caption box2">

                            <div class="wrap-a wrap100">

                                
                                    <span class="peach-text"><?php echo $allArticles[$cntAA]->getType();?></span> <span class="grey-text">• <span class="grey-text"><?php echo $allArticles[$cntAA]->getDateCreated();?></span>
                                
                                
                            </div>

                           
                                <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a">
                                    <?php echo $allArticles[$cntAA]->getTitle();?>
                                </div>
                                <div class="text-content-div">
                                    <?php echo $allArticles[$cntAA]->getParagraphOne();?>
                                </div>
                        
                            
                        </div>
                        
                    </div>
                </a>   

            <?php
            }
            ?>
        <?php
        }
        ?>


    </div>
    
    
    
    
    
    
    </div>
    
    </div>  
    </div>

  <?php
  }
  ?>
  <?php
  }
  ?>

  <div class="clear"></div>

  <?php
  }
  ?>



<?php include 'footer.php'; ?>

</body>
</html>