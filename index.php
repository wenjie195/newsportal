<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/ArticleOne.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $articles = getArticlesOne($conn, " WHERE date_created DESC ");

$articles = getArticlesOne($conn, " WHERE display = 'YES' ORDER BY date_created DESC ");

// $articles = getArticlesOne($conn);
// $articlesBeauty = getArticlesOne($conn, " WHERE type = 'Beauty' ");
// $articlesFashion = getArticlesOne($conn, " WHERE type = 'Fashion' ");
// $articlesSocial = getArticlesOne($conn, " WHERE type = 'Social' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:image" content="https://tevy.asia/img/fb-meta.jpg" />
<meta property="og:title" content="Tevy" />
<meta property="og:description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="description" content="Tevy is one of the most popular news platform among the ladies. Tevy always updated with the trendy news regarding make up, beauty, skin care, fashion, etc." />
<meta name="keywords" content="Tevy, girls, female, lady, ladies, news, beauty care, beauty, skin care, fashion, social, etc">
<link rel="canonical" href="https://tevy.asia/" />
<title>Tevy</title>
<?php include 'css.php'; ?>



</head>
<body>
<?php include 'header-after-login.php'; ?>

<div class="background-div">
    <div class="cover-gap content min-height2">

        <div class="test ">
            <!-- <h1 class="landing-h1">Latest Articles</h1> -->
            <h1 class="landing-h1"><?php echo _MAINJS_INDEX_LATEST_ART ?></h1>
            <div class="big-white-div">

            <?php
            $conn = connDB();
            if($articles)
            {
                for($cnt = 0;$cnt < count($articles) ;$cnt++)
                {
                ?>


                    <!-- <a href='article001.php?id=<?php //echo $articles[$cnt]->getSeoTitle();?>'> -->
                    <!-- <a href='article.php?id=<?php //echo $articles[$cnt]->getSeoTitle();?>'> -->
                    <a href='article.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'>

                        <div class="article-card article-card-overwrite">

                            <div class="article-bg-img-box">
                                <img src="uploads/<?php echo $articles[$cnt]->getTitleCover();?>" class="article-img1" alt="<?php echo $articles[$cnt]->getTitle();?>" title="<?php echo $articles[$cnt]->getTitle();?>">
                            </div>

                            <div class="box-caption box2">

                                <!-- <div class="wrap-a wrap100">
                                    <a href='article001.php?id=<?php //echo $articles[$cnt]->getSeoTitle();?>' class="peach-hover cate-a transition">
                                    <?php //echo $articles[$cnt]->getType();?> <span class="grey-text">• <?php echo $articles[$cnt]->getDateCreated();?></span>
                                    </a>
                                </div> -->

                                <div class="wrap-a wrap100">
                                    <?php $articleType = $articles[$cnt]->getType();
                                    if($articleType ==  'Beauty')
                                    {
                                    ?>
                                        <a href="beautyCare.php" class="peach-hover cate-a transition">
                                            <?php echo $articles[$cnt]->getType();?></a> <span class="grey-text">• <a href='article.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'  class="grey-text"><?php echo $articles[$cnt]->getDateCreated();?></span>
                                        </a>
                                    <?php
                                    }
                                    elseif($articleType ==  'Fashion')
                                    {
                                    ?>
                                        <a href="trendyFashion.php" class="peach-hover cate-a transition">
                                            <?php echo $articles[$cnt]->getType();?></a> <span class="grey-text">• <a href='article.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'  class="grey-text"><?php echo $articles[$cnt]->getDateCreated();?></span>
                                        </a>
                                    <?php
                                    }
                                    elseif($articleType ==  'Social')
                                    {
                                    ?>
                                        <a href="socialNews.php" class="peach-hover cate-a transition">
                                            <?php echo $articles[$cnt]->getType();?></a> <span class="grey-text">• <a href='article.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'  class="grey-text"><?php echo $articles[$cnt]->getDateCreated();?></span>
                                        </a>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <!-- <a href='article001.php?id=<?php //echo $articles[$cnt]->getSeoTitle();?>'> -->
                                <a href='article.php?id=<?php echo $articles[$cnt]->getArticleLink();?>'>
                                <div class="wrap-a wrap100 wrapm darkpink-hover article-title-a">
                                    <?php echo $articles[$cnt]->getTitle();?>
                                </div>

                                <div class="text-content-div">
                                    <?php echo $articles[$cnt]->getParagraphOne();?>
                                </div>
								</a>
                            </div>
                            
                        </div>
                    </a>
                    
                <?php
                }
                ?>
            <?php
            }
            $conn->close();
            ?>

        </div>

        <div class="clear"></div>
        

    </div>
</div>
<?php include 'footer.php'; ?>

</body>
</html>